﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RPM_Tool
{
    public partial class DataFeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "8")
                    {
                        idVal = "8";
                    }
                }

                if (idVal == "8")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }
        [WebMethod]
        public static string bindHistory()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMbindHistory";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string[] GetEmail(string prefix)
        {
            List<string> customers = new List<string>();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMGetEmail   '" + prefix + "'";
            dt = cls.GetData(qry);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                customers.Add(string.Format("{0}-{1}", dt.Rows[i][1], dt.Rows[i][0]));
            }
            return customers.ToArray();
        }
        [WebMethod]
        public static string ddlValues()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataSet dtSet = new DataSet();
            string qry = "Select * from tbl_RPM_ListTitle where id = 24";
            string qry1 = "Select CreatedBy from tbl_RPM_DataFeedCreated";

            string qryStoreProc = "EXEC [spRPMddlValues]";
            dtSet = cls.GetDataSet(qryStoreProc);
            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[6];

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1 } });
        }

        [WebMethod]
        public static string InsertData(ArrayList GetTitle, ArrayList GetLAlertMassage, ArrayList GetCondition)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string exm = GetTitle[1].ToString();
            DateTime dtFrom = DateTime.Parse(GetTitle[4].ToString());
            DateTime dtTo = DateTime.Parse(GetTitle[5].ToString());

            string insertCreated = "";
            //            string insertCreated = @"INSERT INTO tbl_RPM_DataFeedCreated (Name,CreatedBy,CreatedDate) values('
            //             GetTitle[0].ToString()" + @"','" + HttpContext.Current.Session["ntid"] + "','" + DateTime.Now.ToString("hh:mm:ss") + @"')";


            //            //  cls.ExecuteQuery(insertCreated);


            //            string insertDateFeed = @"INSERT INTO tbl_RPM_DataFeed (id,Title,Workflow,AtLocation,OnDays,TimeFrom,TimeTo,TimeZone) values(
            //            (Select MAX(id) from tbl_RPM_DataFeedCreated)" + ",'"
            //                                                           + GetTitle[0].ToString() + "','" + GetTitle[1].ToString() + "','" + GetTitle[2].ToString() + @"','" +
            //                                                           GetTitle[3].ToString() +
            //            @"','" + dtFrom.TimeOfDay.ToString() + "','" + dtTo.TimeOfDay.ToString() + "','" + GetTitle[4].ToString() + "')";
            //            //cls.ExecuteQuery(insertDateFeed);


            //            string insertAlertSuccess = "INSERT INTO tbl_RPM_DataFeed_Alert (id,Subject,Message,isSuccess) VALUES((Select MAX(id) from tbl_RPM_DataFeedCreated)" +
            //                                           @",'" + GetLAlertMassage[0].ToString() + "','" + GetLAlertMassage[1].ToString() + @"','Success')";
            //            //  cls.ExecuteQuery(insertAlertSuccess);

            //            string insertAlertFail = @"INSERT INTO tbl_RPM_DataFeed_Alert (id,Subject,Message,isSuccess) VALUES( 
            //                        (Select MAX(id) from tbl_RPM_DataFeedCreated)" + ",'" + GetLAlertMassage[2].ToString() + "','" + GetLAlertMassage[3].ToString() + @"','Fail')";
            //            //   cls.ExecuteQuery(insertAlertFail);

            //            string insertAlertCondition1 = "";

            insertCreated = "EXEC spRPMInsertData 0,'" +
                GetTitle[0].ToString() + "','" +
                HttpContext.Current.Session["ntid"] + "','" +
                GetTitle[0].ToString() + "','" +
                GetTitle[1].ToString() + "','" +
                GetTitle[2].ToString() + "','" +
                GetTitle[3].ToString() + "','" +
                dtFrom.TimeOfDay.ToString() + "','" +
                dtTo.TimeOfDay.ToString() + "','" +
                GetTitle[4].ToString() + "','" +
                GetLAlertMassage[0].ToString() + "','" +
                GetLAlertMassage[1].ToString() + "','" +
                GetLAlertMassage[2].ToString() + "','" +
                GetLAlertMassage[3].ToString() + "','','','','','','','',''";
            cls.ExecuteQuery(insertCreated);
             
            insertCreated = "";
            if (GetCondition.Count != 0)
            {
                int countarray = GetCondition.Count / 6;
                int GetCon = 0;

                //insertAlertCondition1 = "INSERT INTO tbl_RPM_DataFeedAlertConditions (id,isActive,isRecieve,AfterNo,AfterSet,isSuccess,Email_To,EveryNo,EverySet) VALUES";
                for (int i = 0; i < countarray; i++)
                {
                    if (i != 0)
                    {
                        GetCon = GetCon + 6;
                    }

                    string[] splitValAfter = GetCondition[GetCon + 2].ToString().Split(' ');
                    string[] splitValEvery = GetCondition[GetCon + 5].ToString().Split(' ');

                    //insertAlertCondition1 += "((Select MAX(id) from tbl_RPM_DataFeedCreated)" + ",'" + GetCondition[GetCon].ToString() + "','" +
                    //    GetCondition[GetCon + 1].ToString() + "','" + splitValAfter[0] + "','" + splitValAfter[1] + "','" + GetCondition[GetCon + 3].ToString() + "','" +
                    //    GetCondition[GetCon + 4].ToString() + "','" + splitValEvery[0] + "','" +
                    //    splitValEvery[1] + "'" + "),";

                    insertCreated += " EXEC spRPMInsertData 1,'','','','','','','','','','','','','','" +
                        GetCondition[GetCon].ToString() + "','" +
                        GetCondition[GetCon + 1].ToString() + "','" +
                        splitValAfter[0] + "','" +
                        splitValAfter[1] + "','" +
                        GetCondition[GetCon + 3].ToString() + "','" +
                        GetCondition[GetCon + 4].ToString() + "','" +
                        splitValEvery[0] + "','" +
                        splitValEvery[1] + "'; ";

                }

                //insertAlertCondition1 = insertAlertCondition1.Remove(insertAlertCondition1.Length - 1, 1);
                //cls.ExecuteQuery(insertAlertCondition1);
                cls.ExecuteQuery(insertCreated);
            }
            return "TRUE";
        }

        [WebMethod]
        public static string DeleteDataFeed(string id)
        {
            clsConnection cls = new clsConnection();
            string qry = @"Delete tbl_RPM_DataFeedCreated where id =" + id + " ";
            qry += @"Delete tbl_RPM_DataFeed where id =" + id + " ";
            qry += @"Delete tbl_RPM_DataFeedAlertConditions where id =" + id + " ";
            qry += @"Delete tbl_RPM_DataFeed_Alert where id =" + id + " ";
            cls.ExecuteQuery(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success" });
        }

        [WebMethod]
        public static string EditHistory(string id)
        {
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();

            string qry = @"Select * from tbl_RPM_DataFeedCreated where id =" + id + " ";
            qry += @"Select * from tbl_RPM_DataFeed where id =" + id + " ";
            qry += @"Select * from tbl_RPM_DataFeedAlertConditions where id =" + id + " ";
            qry += @"Select * from tbl_RPM_DataFeed_Alert where id =" + id + " ";

            dtSet = cls.GetDataSet(qry);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dtSet.Tables[0],
                    asd1 = dtSet.Tables[1],
                    asd2 = dtSet.Tables[2],
                    asd3 = dtSet.Tables[3]
                }
            });
        }

        [WebMethod]
        public static string DataLoad()
        {
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();

            string qryData = @"Select top 1 * from tbl_RPM_DataFeedCreated ";
            qryData += @"Select top 1 * from tbl_RPM_DataFeed ";
            qryData += @"Select * from tbl_RPM_DataFeed_Alert ";
            qryData += @"Select * from tbl_RPM_DataFeedAlertConditions ";

            dtSet = cls.GetDataSet(qryData);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    dataFeedCreated = dtSet.Tables[0],
                    dataFeed = dtSet.Tables[1],
                    dataFeedAlert = dtSet.Tables[2],
                    dataFeedAlertConditions = dtSet.Tables[3]
                }
            });
        }

        [WebMethod]
        public static string UpdateWorkflow(string data, string location, ArrayList alerts)
        {
            JObject jData = JObject.Parse(data);

            string title = (string)jData["Title"];                                      //Title/Filename of the workflow
            string atLocation = location;                                               //Location Path
            string onDays = (string)jData["OnDays"];                                    //Days toggled
            string timeFrom = (string)jData["TimeFrom"];                                //Time From
            string timeTo = (string)jData["TimeTo"];                                    //Time To
            string timeZone = (string)jData["TimeZone"];                                //Time From from 24HR to 12HR

            string subjectSuccess = (string)jData["SubjectSuccess"];                    //Success alert message subject
            string messageSuccess = (string)jData["MessageSuccess"];                    //Success alert message
            string subjectFail = (string)jData["SubjectFail"];                          //Fail alert message subject
            string messageFail = (string)jData["MessageFail"];                          //Fail alert message

            clsConnection cls = new clsConnection();                                    //CONNECT
            string query = "";                                                          //Query

            query += @"update tbl_RPM_DataFeedCreated set [Name] ='" + title + "', CreatedBy = '" + HttpContext.Current.Session["ntid"] +
                    "', CreatedDate ='" + DateTime.Today + "' where id = 58" + System.Environment.NewLine;                                                                                     //UPDATE THE CREATED TABLE
            query += @"update tbl_RPM_DataFeed set[Title] ='" + title + "', AtLocation ='" + atLocation + "', OnDays ='" + onDays + "', TimeFrom = '" + timeFrom +                                 // SAVE DATA FEED DETAILS
                    "',TimeTo = '" + timeTo + "', TimeZone = '" + timeZone + "' where id = 58" + System.Environment.NewLine;
            query += @"delete from tbl_RPM_DataFeed_Alert where id = 58" + System.Environment.NewLine;                                                                                      //DELETE ALERT MESSAGES
            query += @"insert into tbl_RPM_DataFeed_Alert (id, Subject, Message, isSuccess) values (58, '" + subjectSuccess + "', '" + messageSuccess + "', 'Success')" + System.Environment.NewLine;      //SUCCESS MESSAGE
            query += @"insert into tbl_RPM_DataFeed_Alert (id, Subject, Message, isSuccess) values (58, '" + subjectFail + "', '" + messageFail + "', 'Fail')" + System.Environment.NewLine;            //FAIL MESSAGE   
            query += @"delete from tbl_RPM_DataFeedAlertConditions where id=58" + System.Environment.NewLine;                                                                               //DELETE ALERTS   

            if (alerts.Count != 0)
            {
                int alertCount = alerts.Count / 8;   //Count how many alerts are entered
                int getAlert = 0;

                for (int i = 0; i < alertCount; i++)
                {
                    if (i != 0)
                    {
                        getAlert = getAlert + 8;
                    }
                    string isActive = alerts[getAlert].ToString();          //IS ACTIVE
                    string isReceived = alerts[getAlert + 1].ToString();    //IF FILE IS RECIEVED OR NOT
                    string afterNo = alerts[getAlert + 2].ToString();       //AFTER NUMBER
                    string afterSet = alerts[getAlert + 3].ToString();      //AFTER LABEL
                    string isSuccess = alerts[getAlert + 4].ToString();     //THEN SEND SUCCESS OR FAILURE ALERT
                    string emailTo = alerts[getAlert + 5].ToString();       //EMAILS
                    string everyNo = alerts[getAlert + 6].ToString();       //EVERY NUMBER
                    string everySet = alerts[getAlert + 7].ToString();      //EVERY LABEL

                    int izActive = 0;
                    if (isActive == "on") { izActive = 1; } else { izActive = 0; }

                    query += @"insert into tbl_RPM_DataFeedAlertConditions (id, isActive, isRecieve, AfterNo, AfterSet, isSuccess, Email_To, EveryNo, EverySet)" +
                        	"values(58, " + izActive + ", '" + isReceived + "', '" + afterNo + "', '" + afterSet + "', '" + isSuccess +
                            "', '" + emailTo + "', '" + everyNo + "', '" + everySet + "')" + System.Environment.NewLine; ;
                }
            }

            cls.ExecuteQuery(query);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success"
            });
        }
    }
}


