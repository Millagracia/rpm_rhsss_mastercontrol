﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
	public class PrioClass
	{
		public List<PrioMain> tblPrioMain { get; set; }
		public List<PrioList> tblPrio { get; set; }
	}

	public class PrioList
	{
		public string id { get; set; }
		public string isActive { get; set; }
		public string sortBy { get; set; }
		public string orderBy { get; set; }
		public string appliedTo { get; set; }
	}

    public class PrioMain
    {
        public string id { get; set; }
        public string level { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string location { get; set; }
        public string bunit { get; set; }
        public string bsegment { get; set; }
        public string actOn { get; set; }
        public string actUntil { get; set; }
        public string keepAct { get; set; }
        public string users { get; set; }
        public string do_not_end { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        
    }

    public class PrioMainList
    {
        public List<PrioMain> tblPrioMain { get; set; }
    }
    public class UserSettings
    {
    }
    public class AccessRole
    {
        public string id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
    }
    public class AccessFunc
    {
        public string id { get; set; }
        public string name { get; set; }
        public string isOn { get; set; }
    }

    public class UserDetails
    {
        public string id { get; set; }
        public string empname { get; set; }
        public string emplevel { get; set; }
        public string role_id { get; set; }
        public string skill_id { get; set; }
        public string immsupp { get; set; }
        public string ntid { get; set; }
    }

    public class UserList
    {
        public List<UserDetails> tblUserList { get; set; }
    }

    public class RoleSkill
    {
        public string id { get; set; }
        public string rsname { get; set; }
        public string rstype { get; set; }
    }
    public class AuditClass
    {
        public string id { get; set; }
        public string page { get; set; }
        public string sub_page { get; set; }
        public string sub_sub_page { get; set; }
        public string action { get; set; }
        public string action_by { get; set; }
        public string action_date { get; set; }
        public string action_time { get; set; }

    }

    public class AuditList
    {
        public List<AuditClass> tblAuditList { get; set; }
    }

    public class AuditParam
    {
        public string timeFrame { get; set; }
        public string by { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}