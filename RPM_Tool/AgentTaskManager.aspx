﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AgentTaskManager.aspx.cs" Inherits="RPM_Tool.AgentTaskManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .drag {
            width: 100%;
            height: 40px;
            padding: 10px;
            border: 1px solid #aaaaaa;
        }

        .panelBox {
            width: 100%;
            height: 40px;
            padding: 10px;
            border: 1px solid #aaaaaa;
        }

        .drag:hover, .pointerCursor:hover {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Master Control</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;Agent Task Manager</span>
                </li>
            </ol>
        </section>
        <section class="content">
             <div class="row">
                <div class="col-lg-3">
                    <div class="btn-group">
                        <button type="button" id="btnAssign" style="width: 118px" class="btn btn-primary ">Assign</button>
                        <button type="button" id="btnReassign" style="width: 118px" class="btn btn-default ">Reassign</button>
                    </div>
                </div>
            </div>
            <div class="row" id="dvReassign" style="display:none">
            </div>
            <div class="row" id="dvAssign">
                <div class="col-lg-6">
                    <div class="panel-heading">
                        <b>Select Employee</b>
                    </div>
                    <div class="panel panel-body" style="height: 610px; overflow: scroll">
                        <div class="row">
                            <div class="col-lg-6">
                                <b>Search:</b>
                            </div>
                            <div class="col-lg-6">
                                <b>Filter:</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                Employee Name
                            </div>
                            <div class="col-lg-6">
                                Immediate Supervisor
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <select id="slcEmpName" class="form-control input-sm tb1">
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select id="slcMGR" class="form-control input-sm tb1">
                                </select>
                            </div>
                        </div>
                        <div class="row topStyle">
                            <div class="col-lg-10">
                            </div>
                            <div class="col-lg-2">
                                <button type="button" id="btnApply" class="btn btn-primary form-control btn-radius-custom">Apply</button>
                            </div>
                        </div>
                        <div class="row topStyle">
                            <div class="col-lg-12">
                                <%--<table class="table table-hover">
                                    <thead style="background-color: #142541; color: #fff">
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" /></th>
                                            <th>Employee Name</th>
                                            <th>Immediate Supervisor</th>
                                            <th>Tasks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>--%>
                                <table id="tblEmployee" class="table table-bordered table-hover" data-single-select="true">
                                    <thead style="background-color: #142541; color: #fff">
                                        <tr>
                                            <th data-field="NTID" data-formatter="idFormatter1">
                                                <input type='checkbox' id='chkRowAll' class='chkRow1' />
                                            </th>
                                            <th data-sortable="true" data-field="EmpName">Employee Name</th>
                                            <th data-sortable="true" data-field="MngrName">Immediate Supervisor</th>
                                            <th data-sortable="true" data-formatter="idFormatter2" data-field="NoOfTask">Task</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel-heading ">
                        <b>Set Workflow priority</b>
                    </div>
                    <div class="panel panel-body" style="height: 610px">
                        <div class="row text-right">
                            <div class="col-lg-12">
                                <div class="btn btn-link" id="btnRefresh"><i class="fas fa-sync fa-2x"></i></div>

                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-5 text-center">
                                Workflows
                            </div>
                            <div class="col-lg-1 text-center">
                                
                            </div>
                            <div class="col-lg-6 text-center">
                                Assignment & Prioritization
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 dvPrio ">
                            </div>
                            <div class="col-lg-1 dvPrioNum ">
                            </div>
                            <div class="col-lg-6 dvPrioDropZone">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-5 text-center">
                            </div>
                            <div class="col-lg-2 text-center">
                                <button type="button" id="btnSaveWorkflow" class="btn btn-primary form-control btn-radius-custom">Save</button>
                            </div>
                            <div class="col-lg-5 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #142541; color: #fff">
                    <button type="button" class="close text-red" style="color: #ff0000" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">
                        <label id="lblSubTask" style="font-style: normal !important; font-size: 16px"></label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5 dvSubPrio">
                        </div>
                        <div class="col-lg-1 dvSubPrioNum">
                        </div>
                        <div class="col-lg-6 dvSubPrioDropZone">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" id="btnSaveSubTaskPrio" class="btn btn-primary">Save</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="mdTask" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #142541; color: #fff">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Task</h4>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <div class="col-lg-6">
                            <label>Assigned Workflows</label>
                        </div>
                        <div class="col-lg-6">
                            <label>Sub-Tasks</label>
                        </div>
                    </div>

                    <div class="dvTask">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="JS/AgentTaskManager.js"></script>
</asp:Content>
