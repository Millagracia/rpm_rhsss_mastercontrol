﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;

namespace RPM_Tool
{
    public partial class SupervisorDashBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "7")
                    {
                        idVal = "7";
                    }
                }

                if (idVal == "7")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtCount = new DataTable();
            DataTable dtCheck = new DataTable();
            DataTable dtTotalLeft = new DataTable();
            DataTable dtTotalInflow = new DataTable();
            string Workflow = "";
            string ActionReq = "";
            string disposition = "";
            string NoOfAgents = "";
            string TotalLeft = "";
            string[] workflowCount = { };
            var timeToConvert = DateTime.Now;
            var est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var targetTime = TimeZoneInfo.ConvertTime(timeToConvert, est);
            string GetDate = targetTime.ToString("MM/dd/yyyy");
            string GetDateNow = DateTime.Now.ToString("MM/dd/yyyy");
            string qry = @"SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', Task_Type AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                                     '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                            FROM            tbl_RPM_Workable_Buildings
                            WHERE        CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"' or not id in 
                                    (Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = 'Buildings' and workflow_id is not null and not disposition IN ('In progress','Escalated')) 
                            GROUP BY Workflow, Task_Type
                            UNION ALL
                            SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', Task_Type AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                                     '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                            FROM            tbl_RPM_Workable_Leases
                            WHERE        CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"' or not id in (Select workflow_id from tbl_RPM_Agents_Workload 
                            where workflow_table = 'Leases' and workflow_id is not null and not disposition IN ('In progress','Escalated')) 
                            GROUP BY Workflow, Task_Type
                            UNION ALL
                            SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', Task_Type AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                                     '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                            FROM            tbl_RPM_Workable_RRI
                            WHERE        CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"' or not id in (Select workflow_id from tbl_RPM_Agents_Workload 
                                        where workflow_table = 'RRI' and workflow_id is not null and not disposition IN ('In progress','Escalated'))
                            GROUP BY Workflow, Task_Type";
            dt = cls.GetData(qry);
            cls.ExecuteQuery("Delete tbl_RPM_ExampleDashBoard_Workflow");
            cls.SQLBulkCopy("tbl_RPM_ExampleDashBoard_Workflow", dt);

            string qryInsert = "Insert Into tbl_RPM_ExampleDashBoard_Workflow (WorkflowType, ActionRequired,TotalLeft,Completed) values ";
            string qryCount = @"SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated', b.disposition, CONVERT(varchar(10),  COUNT(Workflow) )+ ',' + b.disposition as 'Workflow' 
                                FROM tbl_RPM_Workable_Buildings a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id  and b.ntid = a.lockto
                                Where b.disposition is not null and CONVERT(VARCHAR(10), Inflow_Date , 101)  = '" + GetDate + @"' or disposition IN ('In progress','Escalated')
                                GROUP BY Workflow, Task_Type, b.disposition
                                Union All
                                SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated', b.disposition, CONVERT(varchar(10),  COUNT(Workflow) ) + ',' + b.disposition as 'Workflow' 
                                FROM tbl_RPM_Workable_Leases a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id  and b.ntid = a.lockto
                                Where b.disposition is not null and CONVERT(VARCHAR(10), Inflow_Date, 101)  = '" + GetDate + @"' or disposition IN ('In progress','Escalated')
                                GROUP BY Workflow, Task_Type, b.disposition
                                Union All
                                SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated', b.disposition, CONVERT(varchar(10),  COUNT(Workflow) )+ ',' + b.disposition as 'Workflow'
                                FROM tbl_RPM_Workable_RRI a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id   and b.ntid = a.lockto
                                Where b.disposition is not null and CONVERT(VARCHAR(10), Inflow_Date , 101)  = '" + GetDate + @"' or disposition IN ('In progress','Escalated')
                                GROUP BY Workflow, Task_Type, b.disposition";

            string qryTotalLeft = @"SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired','' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' 
                                AS 'Escalated',Count(Task_Type) - Count(b.disposition) AS 'Workflow'
                                FROM tbl_RPM_Workable_Buildings a left join tbl_RPM_Agents_Workload b on b.workflow_id = a.id  and b.ntid = a.lockto
                                GROUP BY Workflow, Task_Type
                                Union All
                                SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' 
                                AS 'Escalated',Count(Task_Type) - Count(b.disposition) AS 'Workflow'
                                FROM tbl_RPM_Workable_Leases a left join tbl_RPM_Agents_Workload b on b.workflow_id = a.id  and b.ntid = a.lockto
                                GROUP BY Workflow, Task_Type
                                Union All
                                SELECT Workflow AS 'WorkflowType', Task_Type AS 'ActionRequired', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                Count(Distinct b.ntid) AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' 
                                AS 'Escalated',Count(Task_Type) -  Count(b.disposition) AS 'Workflow'
                                FROM tbl_RPM_Workable_RRI a left join tbl_RPM_Agents_Workload b on b.workflow_id = a.id  and b.ntid = a.lockto
                                GROUP BY Workflow, Task_Type";

            string qryTotalInflow = @"SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', '' AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                                FROM            tbl_RPM_Workable_Buildings
                                where CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"'  or  not id in 
                                    (Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = 'Buildings' and workflow_id is not null and not disposition IN ('In progress','Escalated')) 
                                GROUP BY Workflow
                                UNION ALL
                                SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', '' AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                                FROM            tbl_RPM_Workable_Leases
                                where CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"'  or  not id in 
                                    (Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = 'Leases' and workflow_id is not null and not disposition IN ('In progress','Escalated')) 
                                GROUP BY Workflow
                                UNION ALL
                                SELECT        Workflow AS 'WorkflowType', Count(Workflow) AS 'TotalInflow', '' AS 'ActionRequired', Count(*) AS 'Counts', '' AS 'TotalLeft', '' AS 'TimeRequired', 
                                '' AS 'NoOfAgents', '' AS 'Completed', '' AS 'InProgress', '' AS 'Escalated'
                                FROM            tbl_RPM_Workable_RRI
                                where CONVERT(VARCHAR(10), Inflow_Date , 101) = '" + GetDate + @"' or  not id in 
                                    (Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = 'RRI' and workflow_id is not null and not disposition IN ('In progress','Escalated')) 
                                GROUP BY Workflow";

            dtCount = cls.GetData(qryCount);
            dtTotalLeft = cls.GetData(qryTotalLeft);
            dtTotalInflow = cls.GetData(qryTotalInflow);
            string qryUpdate = "";
            for (int ii = 0; ii < dtCount.Rows.Count; ii++)
            {
                Workflow = dtCount.Rows[ii][0].ToString();
                ActionReq = dtCount.Rows[ii][1].ToString();
                NoOfAgents = dtCount.Rows[ii][4].ToString();
                disposition = dtCount.Rows[ii][8].ToString();
                workflowCount = dtCount.Rows[ii][9].ToString().Split(',');

                for (int iii = 0; iii < dtTotalLeft.Rows.Count; iii++)
                {
                    if (dtTotalLeft.Rows[iii][0].ToString() == Workflow && dtTotalLeft.Rows[iii][1].ToString() == ActionReq)
                    {
                        TotalLeft = dtTotalLeft.Rows[iii][8].ToString();
                        break;
                    }
                    else
                    {

                    }
                }
                if (ii == 0)
                {
                    for (int iii = 0; iii < dtTotalLeft.Rows.Count; iii++)
                    {
                        qryUpdate += "Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + dtTotalLeft.Rows[iii][8].ToString() + @"' 
                                     where WorkflowType = '" + dtTotalLeft.Rows[iii][0].ToString() + @"'
                                     and ActionRequired = '" + dtTotalLeft.Rows[iii][1].ToString() + "' ";
                    }

                    for (int iii = 0; iii < dtTotalInflow.Rows.Count; iii++)
                    {
                        qryUpdate += "Update tbl_RPM_ExampleDashBoard_Workflow Set TotalInflow = '" + dtTotalInflow.Rows[iii][3].ToString() + @"' 
                                     where WorkflowType = '" + dtTotalInflow.Rows[iii][0].ToString() + @"'";
                    }
                }

                string qryCheck = "Select * from tbl_RPM_ExampleDashBoard_Workflow where WorkflowType = '" + Workflow + @"'
                                   and ActionRequired = '" + ActionReq + "'";
                dtCheck = cls.GetData(qryCheck);

                if (dtCheck.Rows.Count != 0)
                {

                    if (workflowCount[1] == "Completed")
                    {
                        qryUpdate += "Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + "', Completed = '" + workflowCount[0] +
                                    "', NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                                     and ActionRequired = '" + ActionReq + "' ";

                        //                        qryUpdate += " Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + @"', 
                        //                                    NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                        //                                     and ActionRequired = '" + ActionReq + "' ";
                    }
                    else if (workflowCount[1] == "In progress")
                    {
                        qryUpdate += "Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + "', InProgress = '" + workflowCount[0] +
                                    "', NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                                     and ActionRequired = '" + ActionReq + "' ";

                        //                        qryUpdate += " Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + @"',
                        //                                    NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                        //                                                             and ActionRequired = '" + ActionReq + "' ";
                    }
                    else if (workflowCount[1] == "Escalated")
                    {
                        qryUpdate += "Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + "', Escalated = '" + workflowCount[0] +
                                    "', NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                                     and ActionRequired = '" + ActionReq + "' ";
                        //                        qryUpdate += " Update tbl_RPM_ExampleDashBoard_Workflow Set TotalLeft = '" + TotalLeft + @"', 
                        //                                    NoOfAgents = '" + NoOfAgents + "' where WorkflowType = '" + Workflow + @"'
                        //                                     and ActionRequired = '" + ActionReq + "' ";
                    }
                    else
                    {
                    }

                }
            }
            qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
            cls.ExecuteQuery(qryInsert);
            cls.ExecuteQuery(qryUpdate);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int ii = 0; ii < dtCount.Rows.Count; ii++)
                {
                    Workflow = dtCount.Rows[ii][0].ToString();
                    ActionReq = dtCount.Rows[ii][2].ToString();
                }
            }


        }

        [WebMethod]
        public static string displayWorkflow()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();


            string qrySelect = @"Select * from tbl_RPM_ExampleDashBoard_Workflow";
            dt = cls.GetData(qrySelect);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string displayAgents()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qrySelect = "Select * from tbl_RPM_ExampleDashBoard_Agents";
            dt = cls.GetData(qrySelect);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string displayCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();
            DateTime dateForButton = DateTime.Now.AddDays(-1);
            var timeToConvert = DateTime.Now;
            var est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var targetTime = TimeZoneInfo.ConvertTime(timeToConvert, est);
            string GetDate = targetTime.ToString("MM/dd/yyyy");

            string qry = @" Declare @log int;
                            Select @log= SUM(Total) from vw_RPM_TotalLeft
                            Select  SUM(Counts) - @log as 'Number', '1' as 'Left' from vw_RPM_Backlog 
                            Union all 
                            Select SUM(Counts) as 'Number', '2' as 'Left' from vw_RPM_Backlog
                            ";
            dtCount = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dtCount } });
        }
    }
}