﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;

namespace RPM_Tool
{
    public partial class ListMaker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "5")
                    {
                        idVal = "5";
                    }
                }

                if (idVal == "5")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }
        [WebMethod]
        public static string SaveList(string ListTitle)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            //string qryInsert = "Insert Into tbl_RPM_ListTitle (List_Title) values('" + ListTitle + "')";
            string qryUpdate = "Update tbl_RPM_ListTitle set isActive =  1 where List_Title IN ('" + ListTitle + "')";
            string qrySelect = "Select * from tbl_RPM_ListTitle";

            string qryStroreProc = "EXEC spRPMSaveList '" + ListTitle + "'";


            cls.ExecuteQuery(qryStroreProc);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetListTitle()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qrySelect = "EXEC spRPMGetListTitle";
            dt = cls.GetData(qrySelect);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string DeleteList(string delete_id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            //cls.ExecuteQuery("Delete tbl_RPM_ListTitle where id = '" + delete_id + "'");

            string qryStoreProc = "EXEC spRPMDeleteList " + delete_id;


            //cls.ExecuteQuery("Update tbl_RPM_ListTitle set isActive =  0 where id IN ('" + delete_id + "')");
            //dt = cls.GetData("Select * from tbl_RPM_ListTitle where isActive =  0");

            dt = cls.GetData(qryStoreProc);


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string SaveListOption(string ListOption, string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qryUpdate = "UPdate tbl_RPM_ListTitle Set List_Option = '" + ListOption + "' where id =" + id;
            string qrySelect = "Select * from tbl_RPM_ListTitle";

            string qryStoreProc = "EXEC spRPMSaveListOption '" + ListOption + "'," + id;

            cls.ExecuteQuery(qryStoreProc);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }


        [WebMethod]
        public static string GetListOption(string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qrySelect = "Select * from tbl_RPM_ListTitle where id =" + id;
            string qryStoreProc = "EXEC spRPMGetListOption " + id;
            dt = cls.GetData(qryStoreProc);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string ddlValues()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataSet dtSet = new DataSet();
            string qry = "EXEC [spRPMddlValues] ";


            dtSet = cls.GetDataSet(qry);
            dt = dtSet.Tables[7];
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetHeaders(string filename)
        {

            clsConnection cls = new clsConnection();
            string FilePath = @"C:\Users\millagra\Documents\Visual Studio 2013\Projects\RPM_Tool\RPM_Tool\fileExcel\" + filename + "";
            string ext = ".xlsx";
            string isHader = "";
            DataTable dataTable = new DataTable();
            //cls.ExecuteQuery("Delete tbl_RPM_ListMaker_Headers where isSave = 0");
            //cls.ExecuteQuery("Delete tbl_RPM_ListMaker_Headers_Created where isSave = 0");
            string qryInsert = "Insert Into tbl_RPM_ListMaker_Headers (id,Headers,Assign_To_Workflow) values";



            string qryStoreProc = "";
            string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0",
                HttpContext.Current.Server.MapPath("~/fileExcel/" + filename + ""));
            // Create Connection to Excel Workbook 
            using (OleDbConnection connection =
                         new OleDbConnection(excelConnectionString))
            {
                OleDbCommand command = new OleDbCommand
                ("Select * FROM [Sheet1$]", connection);
                connection.Open();
                // Create DbDataReader to Data Worksheet 
                using (DbDataReader dr = command.ExecuteReader())
                {
                    // SQL Server Connection String 
                    string sqlConnectionString = @"Data Source=piv8dbmisnp01;Initial Catalog=MIS_ALTI;Integrated Security=True";
                    dataTable.Load(dr);
                }
            }
            List<string> headers = new List<string>();
            foreach (DataColumn col in dataTable.Columns)
            {
                headers.Add(col.ColumnName);
            }
            qryStoreProc = "EXEC spRPMGetHeadersDeleteInsert 1,'" + HttpContext.Current.Session["ntid"] + "','','','','" + filename + "'";
            //            cls.ExecuteQuery(@"Insert Into tbl_RPM_ListMaker_Headers_Created (Workflow,Last_Modified,Last_Modified_DateAndTime,FileName) values (
            //                                '','" + HttpContext.Current.Session["ntid"] + "',GetDate(), '" + filename + "' )");
            cls.ExecuteQuery(qryStoreProc);
            qryStoreProc = "";
            for (int i = 0; i < headers.Count(); i++)
            {
                //qryInsert += "((Select Max(id) From tbl_RPM_ListMaker_Headers_Created),'" + headers[i] + "','0'),";
                qryStoreProc += "EXEC spRPMGetHeadersDeleteInsert 0,'','','" + headers[i] + "','',''";
            }

            cls.ExecuteQuery(qryStoreProc);

            //qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
            //cls.ExecuteQuery(qryInsert);
            return filename;
        }





        [WebMethod]
        public static string GetDisplayHeaders()
        {

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = @"EXEC spRPMGetDisplayHeaders";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string GetDisplayHeadersEdit(int id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataSet dtSet = new DataSet();


            //string qry = @"Select Headers from tbl_RPM_ListMaker_Headers where id  = " + id;
            //string qry1 = @"Select Headers from tbl_RPM_ListMaker_Headers where id  = " + id + " and Assign_To_Workflow = 1";
            //string qry2 = @"Select Workflow,FileName from tbl_RPM_ListMaker_Headers_Created where id  = " + id;

            string qryStoreProc = "EXEC spRPMGetDisplayHeadersEdit " + id;

            dtSet = cls.GetDataSet(qryStoreProc);

            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dt2 } });
        }
        [WebMethod]
        public static string GetDisplayHeadersView(int id)
        {

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataSet dtSet = new DataSet();
            //string qry = @"Select Headers from tbl_RPM_ListMaker_Headers where id  =" + id + " and Assign_To_Workflow = 1";
            string qryStoreProc = "EXEC spRPMGetDisplayHeadersEdit " + id;

            dtSet = cls.GetDataSet(qryStoreProc);
            dt = dtSet.Tables[1];
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string ddlValues1()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataSet dtSet = new DataSet();

            //string qry = "Select * from tbl_RPM_ListTitle where id = 24";
            //string qry1 = "Select NTID,EmpName from tbl_RPM_HRMS_EmployeeMaster where EmpLevel = 'MGR' order by EmpName";
            //string qry2 = "Select NTID,EmpName from tbl_RPM_HRMS_EmployeeMaster where EmpLevel = 'TL' order by EmpName";
            //string qry3 = "Select distinct Created_By from [tbl_RPM_Workflow] order by Created_By";
            //string qry4 = "Select * from tbl_RPM_ListTitle where id = 25";
            
            string qryStoreProc = "EXEC spRPMddlValuesListMaker";
            dtSet = cls.GetDataSet(qryStoreProc);
            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            dt3 = dtSet.Tables[3];
            dt4 = dtSet.Tables[4];
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dt2, asd3 = dt3, asd4 = dt4 } });
        }


        [WebMethod]
        public static string InsertDataHeaders(ArrayList myDataBuildings, ArrayList myDataLeases, ArrayList myDataRRI, string Workflow)
        {
            clsConnection cls = new clsConnection();

            string qryUpdate = "";
            string Headers = "";
            string qryStoreProc = "";
            DataTable dtBuildings = new DataTable();
            DataTable dtLeases = new DataTable();
            DataTable dtRRI = new DataTable();
            DataTable dtCheck = new DataTable();
            string headers = "";

            string qryCheck = "Select * from tbl_RPM_ListMaker_Headers_Created Where Workflow IN ('" + Workflow +"')";
            dtCheck = cls.GetData(qryCheck);

            if(dtCheck.Rows.Count == 0)
            {
                string qryBuildings = "Select * from tbl_RPM_Workable_Buildings";
                string qryLeases = "Select * from tbl_RPM_Workable_Leases";
                string qryRRI = "Select * from tbl_RPM_Workable_RRI";
                string qryInsert = "Insert Into [tbl_RPM_ListMaker_Headers] (id, Headers,Pick_Table,Assign_To_Workflow, isSave) values ";
                string qryInsertCreated = @"Insert Into [tbl_RPM_ListMaker_Headers_Created] (Workflow,Last_Modified,Last_Modified_DateAndTime,isSave) 
                               values ('" + Workflow + "','" + HttpContext.Current.Session["ntid"] + "',GetDate(),1)";
                dtBuildings = cls.GetData(qryBuildings);
                dtLeases = cls.GetData(qryLeases);
                dtRRI = cls.GetData(qryRRI);

                cls.ExecuteQuery(qryInsertCreated);
                foreach (DataColumn col in dtBuildings.Columns)
                {
                    headers += headers == "" ? col.ColumnName : "," + col.ColumnName;

                    if (myDataBuildings.Contains(col.ColumnName))
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','Buildings',1,1),";
                    }
                    else
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','Buildings',0,1),";
                    }
                }

                foreach (DataColumn col in dtLeases.Columns)
                {
                    headers += headers == "" ? col.ColumnName : "," + col.ColumnName;

                    if (myDataLeases.Contains(col.ColumnName))
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','Leases',1,1),";
                    }
                    else
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','Leases',0,1),";
                    }
                }

                foreach (DataColumn col in dtRRI.Columns)
                {
                    headers += headers == "" ? col.ColumnName : "," + col.ColumnName;

                    if (myDataRRI.Contains(col.ColumnName))
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','RRI',1,1),";
                    }
                    else
                    {
                        qryInsert += "((Select Max(id) From [tbl_RPM_ListMaker_Headers_Created]), '" + col.ColumnName + "','RRI',0,1),";
                    }
                }


                qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
                cls.ExecuteQuery(qryInsert);

                //            qryUpdate = @"Update [tbl_RPM_ListMaker_Headers] set Assign_To_Workflow = 1 
                //                                    where id = (Select Max (id) from tbl_RPM_ListMaker_Headers_Created) and Headers in (" + Headers + ")";
                //            cls.ExecuteQuery(qryUpdate);
                //            cls.ExecuteQuery("Update tbl_RPM_ListMaker_Headers_Created Set Workflow = '" + Workflow + "' where id = (Select Max (id) from tbl_RPM_ListMaker_Headers_Created)");
                //            cls.ExecuteQuery(@"Update [tbl_RPM_ListMaker_Headers] set  isSave = 1
                //                                    where id = (Select Max (id) from tbl_RPM_ListMaker_Headers_Created); 
                //                                Update [tbl_RPM_ListMaker_Headers_Created] set  isSave = 1
                //                                    where id = (Select Max (id) from tbl_RPM_ListMaker_Headers_Created);");

                return JsonConvert.SerializeObject(new { Success = true, Message = "Success" });
            }
            else
            {
                return JsonConvert.SerializeObject(new { Success = true, Message = "Not Save" });
            }

           
        }

        [WebMethod]
        public static string displayRepository()
        {


            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMdisplayRepository";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UpdateHeaders(ArrayList myDataBuilding, ArrayList myDataLeases, ArrayList myDataRRI, string Workflow, int id)
        {
            clsConnection cls = new clsConnection();
            string qryUpdate = "";
            string Headers = "";
            string qryStoreProc = "EXEC spRPMUpdateHeaders 0," + id + ",'','',''";
            //cls.ExecuteQuery("Update tbl_RPM_ListMaker_Headers set Assign_To_Workflow = 0 where id = " + id);
            cls.ExecuteQuery(qryStoreProc);

            qryStoreProc = "";
            for (int i = 0; i < myDataBuilding.Count; i++)
            {
                Headers += Headers == "" ? "'" + myDataBuilding[i] + "'" : ",'" + myDataBuilding[i] + "'";
                qryStoreProc += " EXEC spRPMUpdateHeaders 1," + id + ",'" + myDataBuilding[i] + "','','Buildings'; ";
            }
            for (int i = 0; i < myDataLeases.Count; i++)
            {
                Headers += Headers == "" ? "'" + myDataLeases[i] + "'" : ",'" + myDataLeases[i] + "'";
                qryStoreProc += " EXEC spRPMUpdateHeaders 1," + id + ",'" + myDataLeases[i] + "','','Leases'; ";
            }
            for (int i = 0; i < myDataRRI.Count; i++)
            {
                Headers += Headers == "" ? "'" + myDataRRI[i] + "'" : ",'" + myDataRRI[i] + "'";
                qryStoreProc += " EXEC spRPMUpdateHeaders 1," + id + ",'" + myDataRRI[i] + "','','RRI'; ";
            }
            
            cls.ExecuteQuery(qryStoreProc);
            //            qryUpdate = @"Update [tbl_RPM_ListMaker_Headers] set Assign_To_Workflow = 1 
            //                                    where id = " + id + " and Headers in (" + Headers + ")";
            //            cls.ExecuteQuery(qryUpdate);

            cls.ExecuteQuery("EXEC spRPMUpdateHeaders 2," + id + ",'','" + Workflow + "',''");
            //cls.ExecuteQuery("Update tbl_RPM_ListMaker_Headers_Created Set Workflow = '" + Workflow + "' where id = " + id + "");

            return "TRUE";
        }

        [WebMethod]
        public static string GetTableHeader(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string headers = "";
            string qry = "Select * from tbl_RPM_Workable";
            dt = cls.GetData(qry);

            foreach (DataColumn col in dt.Columns)
            {
                headers += headers == "" ? col.ColumnName : "," + col.ColumnName;

            }


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = headers } });
        }

        [WebMethod]
        public static string SaveWorkflow(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qryInsert = "EXEC [spRPMSaveWorkflow] '" + workflow + "',24";
            cls.ExecuteQuery(qryInsert);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string GetWorkflowVal()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = "Select List_Option from tbl_RPM_ListTitle where id = 24";
            qry = "Select List_Option from tbl_RPM_ListTitle_Option where id = 24";
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string GetSubWorkflowVal(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = @"Select Sublist_Option from tbl_RPM_ListTitle_SubOption where id = (
						Select Workflow_ID from tbl_RPM_ListTitle_Option where List_Option IN ('" + workflow + "'))";
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string SaveSubWorkflow(string workflow, string subWorkflowAdd)
        {
            clsConnection cls = new clsConnection();
            string qry = @"Insert into tbl_RPM_ListTitle_SubOption (id,SubList_Option) values 
                        ((Select Workflow_ID from tbl_RPM_ListTitle_Option  where List_Option IN ('" + workflow + "')),'" + subWorkflowAdd + "') ";
            cls.ExecuteQuery(qry);

            return "True";
        }
        [WebMethod]
        public static string DeleteWorkflow(string deleteVal)
        {

            clsConnection cls = new clsConnection();
            string qry = @"Delete tbl_RPM_ListTitle_Option where List_Option = '" + deleteVal + "'";
            cls.ExecuteQuery(qry);
            return "True";
        }

        [WebMethod]
        public static string DeleteSubWorkflow(string deleteVal)
        {
            string[] splitVal = deleteVal.Split(',');

            clsConnection cls = new clsConnection();
            string qry = @"Delete tbl_RPM_ListTitle_SubOption where id = 
                    (Select Workflow_ID from tbl_RPM_ListTitle_Option where List_Option = '" + splitVal[0] + "') and Sublist_Option = '" + splitVal[1] + "'";
            cls.ExecuteQuery(qry);
            return "True";
        }
        [WebMethod]
        public static string GetHeaders()
        {
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();
            DataTable dtBuildings = new DataTable();
            DataTable dtLeases = new DataTable();
            DataTable dtRRI = new DataTable();

            List<string> arrBuilding = new List<string> { };
            List<string> arrLeases = new List<string> { };
            List<string> arrRRI = new List<string> { };

            string qry = @"Select * from tbl_RPM_Workable_Buildings ";
            string qry1 = @"Select * from tbl_RPM_Workable_Leases ";
            string qry2 = @"Select * from tbl_RPM_Workable_RRI ";

            dtBuildings = cls.GetData(qry);
            dtLeases = cls.GetData(qry1);
            dtRRI = cls.GetData(qry2);


            foreach (DataColumn column in dtBuildings.Columns)
            {
                arrBuilding.Add(column.ColumnName);
            }

            foreach (DataColumn column in dtLeases.Columns)
            {
                arrLeases.Add(column.ColumnName);
            }

            foreach (DataColumn column in dtRRI.Columns)
            {
                arrRRI.Add(column.ColumnName);
            }


            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = arrBuilding,
                    asd1 = arrLeases,
                    asd2 = arrRRI
                }
            });
        }


    
    }
}