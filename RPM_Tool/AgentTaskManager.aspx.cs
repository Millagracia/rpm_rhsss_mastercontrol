﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RPM_Tool
{
    public partial class AgentTaskManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "3")
                    {
                        idVal = "3";
                    }
                }

                if (idVal == "3")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }
        
        [WebMethod]
        public static string ddlValues()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();

            DataSet dtSet = new DataSet();
            dtSet = cls.GetDataSet("EXEC spRPMddlValues");
            string qry = @"Select Sublist_Option from tbl_RPM_ListTitle_SubOption where id = 
                            (Select Workflow_ID From tbl_RPM_ListTitle_Option Where List_Option = 'Lease Amendment')";
           // dt = cls.GetData(qry);
            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            dt3 = dtSet.Tables[3];
            dt4 = dtSet.Tables[4];
            dt5 = dtSet.Tables[5];

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    asd1 = dt1,
                    asd2 = dt2,
                    asd3 = dt3,
                    asd4 = dt4,
                    asd5 = dt5
                }
            });
        }
        [WebMethod]
        public static string ddlValuesSubWorkflow(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();

            DataSet dtSet = new DataSet();
            dtSet = cls.GetDataSet("EXEC spRPMddlValues");
            string qry = @"Select Sublist_Option from tbl_RPM_ListTitle_SubOption where id = 
                            (Select Workflow_ID From tbl_RPM_ListTitle_Option Where List_Option = '" + workflow + "')";
           // dt = cls.GetData(qry);
            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            dt3 = dtSet.Tables[3];
            dt4 = cls.GetData(qry);
            dt5 = dtSet.Tables[5];

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    asd1 = dt1,
                    asd2 = dt2,
                    asd3 = dt3,
                    asd4 = dt4,
                    asd5 = dt5
                }
            });
        }

        [WebMethod]
        public static string displayEmployee()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"Select a.NTID,a.EmpName,a.MngrName,b.NoOfTask from tbl_RPM_HRMS_EmployeeMaster a FULL OUTER JOIN tbl_RPM_AgentTask_Manager b on b.ntid = a.NTID 
                            where not a.NTID is null and a.NTID in (select ntid from tbl_RPM_Employee_List where department = 'RHSS')  order by a.EmpName";

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string displayEmployeeSearch(string EmpName, string MGR)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMdisplayEmployeeSearch '" + EmpName + "','" + MGR + "'";



            //            if (EmpName != "0" || MGR != "0")
            //            {
            //                if (EmpName == "0")
            //                {
            //                    qry = @"Select a.NTID,a.EmpName,a.MngrName,b.NoOfTask from tbl_RPM_HRMS_EmployeeMaster a FULL OUTER JOIN tbl_RPM_AgentTask_Manager b on b.ntid = a.NTID
            //                            where not a.NTID is null and a.MngrName = '" + MGR + "'  order by a.EmpName";
            //                }
            //                else
            //                {
            //                    qry = @"Select a.NTID,a.EmpName,a.MngrName,b.NoOfTask from tbl_RPM_HRMS_EmployeeMaster a FULL OUTER JOIN tbl_RPM_AgentTask_Manager b on b.ntid = a.NTID
            //                            where not a.NTID is null and a.NTID = '" + EmpName + "'  order by a.EmpName";
            //                }
            //            }
            //            else if (EmpName == "0" && MGR == "0")
            //            {
            //                qry = @"Select a.NTID,a.EmpName,a.MngrName,b.NoOfTask from tbl_RPM_HRMS_EmployeeMaster a FULL OUTER JOIN tbl_RPM_AgentTask_Manager b on b.ntid = a.NTID 
            //                            where not a.NTID is null order by a.EmpName";
            //            }
            //            else
            //            {
            //                qry = @"Select a.NTID,a.EmpName,a.MngrName,b.NoOfTask from tbl_RPM_HRMS_EmployeeMaster a FULL OUTER JOIN tbl_RPM_AgentTask_Manager b on b.ntid = a.NTID
            //                        where not a.NTID is null and a.NTID = '" + EmpName + "' and a.MngrName = '" + MGR + "'  order by a.EmpName";
            //            }

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static void applyAction(string valWorkflow, string emp_id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string[] emp_arr = emp_id.Split(',');
            string[] workflow_arr = valWorkflow.Split(',');
            string ntid = "";
            string qryInsert = "INSERT INTO tbl_RPM_AgentTask_Manager (ntid,Workflow,NoOfTask,Created_By) VALUES ";
            string qryUpdate = "";
            for (int i = 0; i < emp_arr.Length; i++)
            {
                ntid += "'" + emp_arr[i] + "',";
            }
            ntid += "''";
            string qry = "SELECT * FROM tbl_RPM_AgentTask_Manager where ntid in (" + ntid + ")";

            string qryStoreProc = "";
            dt = cls.GetData(qry);

            if (dt.Rows.Count == 0)
            {
                for (int i = 0; i < emp_arr.Length; i++)
                {

                    qryStoreProc += "  EXEC spRPMapplyAction '" + emp_arr[i] + "','" + valWorkflow + "','" + workflow_arr.Length + "','" + HttpContext.Current.Session["ntid"] + "','0'; ";

                    //qryInsert += "('" + emp_arr[i] + "','" + valWorkflow + "','" + workflow_arr.Length + "','" + HttpContext.Current.Session["ntid"] + "'),";
                }
                //qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
                cls.ExecuteQuery(qryStoreProc);
            }
            else
            {
                string[] dt_arr = new string[dt.Rows.Count];
                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {

                    dt_arr[ii] = dt.Rows[ii][1].ToString();
                    int index = Array.IndexOf(emp_arr, dt.Rows[ii][1].ToString());
                    if (index == -1)
                    {
                        //qryInsert += "('" + dt.Rows[ii][1].ToString() + "','" + valWorkflow + "','','" + valINT + "'),";
                    }
                    else
                    {
                        //qryUpdate += "Update tbl_RPM_AgentTask_Manager set Workflow = '"
                        //    + valWorkflow + "',NoOfTask = '" + workflow_arr.Length + "' where ntid = '" + dt.Rows[ii][1].ToString() + "'; ";

                        qryStoreProc += "  EXEC spRPMapplyAction '0','" + valWorkflow + "','" + workflow_arr.Length +
                                        "','" + dt.Rows[ii][1].ToString() + "',2; ";
                    }
                }
                for (int i = 0; i < emp_arr.Length; i++)
                {
                    int index = Array.IndexOf(dt_arr, emp_arr[i]);
                    if (index == -1)
                    {
                        //qryInsert += "('" + emp_arr[i] + "','" + valWorkflow + "','" + workflow_arr.Length + "','" + HttpContext.Current.Session["ntid"] + "'),";
                        qryStoreProc += "  EXEC spRPMapplyAction '" + emp_arr[i] + "','" + valWorkflow +
                                "','" + workflow_arr.Length + "','" + HttpContext.Current.Session["ntid"] + "',1; ";
                    }
                }
                //qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
                cls.ExecuteQuery(qryStoreProc);
                //cls.ExecuteQuery(qryUpdate);
            }
        }
        [WebMethod]
        public static void SaveSubTask(string emp_id, string AssignVal, string SubTaskval)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string ntid = "";
            string[] emp_arr = emp_id.Split(',');
            string qryInsert = "Insert Into tbl_RPM_SubTask (ntid,Workflow_Set,SubTask,Created_By) values ";
            string qryUpdate = "";
            string qryStoreProc = "";
            for (int i = 0; i < emp_arr.Length; i++)
            {
                ntid += "'" + emp_arr[i] + "',";
            }
            ntid = ntid.Remove(ntid.Length - 1, 1);
            string qry = "Select * from tbl_RPM_SubTask where ntid in (" + ntid + ") and Workflow_Set = '" + AssignVal + "'";

            dt = cls.GetData(qry);

            if (dt.Rows.Count == 0)
            {
                for (int i = 0; i < emp_arr.Length; i++)
                {
                    //qryInsert += "('" + emp_arr[i] + "','" + AssignVal + "','" + SubTaskval + "','" + HttpContext.Current.Session["ntid"] + "'),";

                    qryStoreProc += " EXEC spRPMSaveSubTask '" + emp_arr[i] + "','" + AssignVal + "','" + SubTaskval + "','" + HttpContext.Current.Session["ntid"] + "',0";

                }
                //qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
                cls.ExecuteQuery(qryStoreProc);
            }
            else
            {
                string[] dt_arr = new string[dt.Rows.Count];

                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {

                    dt_arr[ii] = dt.Rows[ii][0].ToString();
                    int index = Array.IndexOf(emp_arr, dt.Rows[ii][0].ToString());
                    if (index == -1)
                    {

                    }
                    else
                    {
                        //qryUpdate += "Update tbl_RPM_SubTask set SubTask = '"
                        //    + SubTaskval + "' where ntid = '" + dt.Rows[ii][0].ToString() + "' and Workflow_Set = '" + AssignVal + "'; ";
                        qryStoreProc += " EXEC spRPMSaveSubTask '" + dt.Rows[ii][0].ToString() + "','" + AssignVal + "','" + SubTaskval + "','" + HttpContext.Current.Session["ntid"] + "',2";
                    }
                }
                for (int i = 0; i < emp_arr.Length; i++)
                {
                    int index = Array.IndexOf(dt_arr, emp_arr[i]);
                    if (index == -1)
                    {
                        //qryInsert += "('" + emp_arr[i] + "','" + AssignVal + "','" + SubTaskval + "','" + HttpContext.Current.Session["ntid"] + "'),";
                        qryStoreProc += " EXEC spRPMSaveSubTask '" + emp_arr[i] + "','" + AssignVal + 
                                        "','" + SubTaskval + "','" + HttpContext.Current.Session["ntid"] + "',1";
                    }
                }


                //qryInsert = qryInsert.Remove(qryInsert.Length - 1, 1);
                //cls.ExecuteQuery(qryInsert);
                cls.ExecuteQuery(qryStoreProc);

            }
             
        }

        [WebMethod]
        public static string GetTask(string ntid)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMGetTask '" + ntid + "'";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                }
            });
        }
    }
}