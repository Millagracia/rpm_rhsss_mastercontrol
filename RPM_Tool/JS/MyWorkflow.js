﻿$(document).ready(function () {
    //var start = new Date().getTime(),
    //elapsed = '0.0';
    //window.setInterval(function () {
    //    var time = new Date().getTime() - start;

    //    elapsed = Math.floor(time / 100) / 10;
    //    if (Math.round(elapsed) == elapsed) { elapsed += '.0'; }

    //    document.title = elapsed;
    //}, 100);
});
var isPaused = false;
var initialTime;
var preStatus = "";
var isNotComplete = "";
var WorkingTime = 10000;
var Workflow_id = 0;
function checkTime() {
    if (!isPaused) {
        var timeDifference = Date.now() - initialTime;
        var formatted = convertTime(timeDifference);
        document.getElementById('lblTimers').innerHTML = '' + formatted;
        var about = document.getElementById("lblTimers").innerHTML;

        
        var WorkingTimeVar = about.replace(':', '');
        WorkingTimeVar = WorkingTimeVar.replace(':', '');
        if (WorkingTime < WorkingTimeVar) {
            $('#lblTimers').css({ 'color': 'rgb(203, 4, 4);' });
        }
        else {
            $('#lblTimers').css({ 'color': '#333' });
        }
    }
    else {
    }
}
function convertTime(miliseconds) {
    var d = new Date();
    var n = d.getMilliseconds();
    var totalSeconds = Math.floor(miliseconds / 1000);
    var minutes = Math.floor(totalSeconds / 60);
    var seconds = totalSeconds - minutes * 60;
    var centiseconds = padZero((miliseconds / 10 | 0) % 100);
    seconds = parseInt(seconds) <= 9 ? ('0' + seconds) : seconds;
    return minutes + ':' + seconds + ':' + centiseconds;
}
$('#slcStatus').change(function () {
    console.log($('#slcStatus').val());
    if ($('#slcStatus').val() == 'Ready') {
        $('.dvNotReadySlc,.dvNotReady').hide();
        $('.dvWorkingslc,.dvWorking').show();
        fnGetWorkload();
        $(".dvWorking").removeClass("watermark");
      
        fnWorkStatus('Working');
        preStatus = 'Working...';

        if ($(".dvNotReadySlc,.dvNotReady").css('display') == "none")
        {
            initialTime = Date.now();
            window.setInterval(checkTime, 100);
            console.log("hide");
            $('#slcStatus').val('Not Ready');
        }
        else {
        }
    }
    else if ($('#slcStatus').val() == 'Break 1') {
        fnWorkStatus('Break 1', document.getElementById("lblTimers").innerHTML, 'Working');
    }
    else if ($('#slcStatus').val() == 'Lunch') {
        fnWorkStatus('Lunch', document.getElementById("lblTimers").innerHTML, 'Break 1');
    }
    else if ($('#slcStatus').val() == 'Break 2') {
        fnWorkStatus('Break 2', document.getElementById("lblTimers").innerHTML, 'Lunch');
    }
    else if ($('#slcStatus').val() == 'BioBreak') {
        fnWorkStatus('BioBreak', document.getElementById("lblTimers").innerHTML, 'Break 2');
    }
    else if ($('#slcStatus').val() == 'Meeting') {
        fnWorkStatus('Meeting', document.getElementById("lblTimers").innerHTML, 'BioBreak');
    }
    else if ($('#slcStatus').val() == 'Training') {
        fnWorkStatus('Training', document.getElementById("lblTimers").innerHTML, 'Meeting');
    }
    else if ($('#slcStatus').val() == 'System lssue') {
        fnWorkStatus('System lssue', document.getElementById("lblTimers").innerHTML, 'Training');
    }
    else {
    }
});

function fnWorkStatus(val, duration, valpreStatus) {
    $.ajax({
        type: 'POST',
        url: 'MyWorkflow.aspx/SaveWorkStatus',
        data: '{"workStatus" : "' + val + '","duration" : "' + duration + '","valpreStatus" : "' + valpreStatus + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#slcStatusWorking').change(function () {
    console.log($('#slcStatusWorking').val());
    if ($('#slcStatusWorking').val() == 'Working') {
        initialTime = Date.now();
        isPaused = false;
        $(".dvWorking").removeClass("watermark");
        fnWorkStatus('Working...', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Working...';
    }
    else if ($('#slcStatusWorking').val() == 'Break 1') {
        initialTime = Date.now();
        fnWorkStatus('Break 1', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Break 1';
        $(".dvWorking").addClass("watermark");
    }
    else if ($('#slcStatusWorking').val() == 'Lunch') {
        initialTime = Date.now();
        $(".dvWorking").addClass("watermark");
        fnWorkStatus('Lunch', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Lunch';
    }
    else if ($('#slcStatusWorking').val() == 'Break 2') {
        initialTime = Date.now();
        $(".dvWorking").addClass("watermark");
        fnWorkStatus('Break 2', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Break 2';
    }
    else if ($('#slcStatusWorking').val() == 'BioBreak') {
        initialTime = Date.now();
        $(".dvWorking").addClass("watermark");
        fnWorkStatus('BioBreak', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'BioBreak';
    }
    else if ($('#slcStatusWorking').val() == 'Meeting') {
        initialTime = Date.now();
        $(".dvWorking").addClass("watermark");
        fnWorkStatus('Meeting', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Meeting';
    }
    else if ($('#slcStatusWorking').val() == 'Training') {
        initialTime = Date.now();
        $(".dvWorking").addClass("watermark");
        fnWorkStatus('Training', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'Training';
    }
    else if ($('#slcStatusWorking').val() == 'System lssue') {
        initialTime = Date.now();
        fnWorkStatus('System Issue', document.getElementById("lblTimers").innerHTML, preStatus);
        preStatus = 'System Issue';
        $(".dvWorking").addClass("watermark");
    }
    else if ($('#slcStatusWorking').val() == 'Logout') {
        $('.dvNotReadySlc,.dvNotReady').show();
        $('.dvWorkingslc,.dvWorking').hide();
        $('#slcStatus').val('Not Ready');
        isPaused = true;
        $(".dvWorking").addClass("watermark");
        document.getElementById("lblTimers").innerHTML = '0:00:00'
        fnWorkStatus('Logout', document.getElementById("lblTimers").innerHTML, preStatus);
    }
});
function padZero(miliseconds) {
    return miliseconds < 10 ? '0' + miliseconds : '' + miliseconds;
}
function fnGetWorkload() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'MyWorkflow.aspx/GetWorkloadHeaders',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            
            $('.dvRemoveWorkable').remove();
            if (d.Message == 'Success')
            {
                var records = d.data.asd;
                var records1 = d.data.asd1;
                var records2 = d.data.asd2;
                var appendHeaders = "";
                var strWorkflow = "";
                var arryValue = records1.split('+');
                var arryHeader = [];
                var arryHeaderVal = [];
                $.each(records, function (idx, val) {
                    arryHeader.push(val.Headers);
                    //appendHeaders += '<div class="row topStyle dvRemoveWorkable" >';
                    //appendHeaders += '               <div class="col-lg-2 " style="padding-top: 5px">' + val.Headers + '</div>';
                    //appendHeaders += ' <div class="col-lg-2">';
                    //appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[idx] + '"/>';
                    //appendHeaders += '</div>';
                    //appendHeaders += '</div>';
                    strWorkflow = val.Workflow;
                });
                Workflow_id = arryValue[arryValue.length - 1];
                WorkingTime = arryValue[arryValue.length - 2];
                WorkingTime = WorkingTime.replace(':', '');
                WorkingTime = WorkingTime.replace(':', '');
                WorkingTime = WorkingTime.substring(1);
                $('.dvRemoveWorkable').remove();
                for (i = 1 ; i < arryHeader.length; i = i + 2) {
                    var intHeader = i - 1;

                    appendHeaders += '<div class="row topStyle dvRemoveWorkable" >';
                    appendHeaders += '               <div class="col-lg-3 text-right" style="padding-top: 5px">' + arryHeader[intHeader] + '</div>';
                    appendHeaders += ' <div class="col-lg-2">';
                    appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[intHeader] + '" readonly="readonly"/>';
                    appendHeaders += '</div>';
                    appendHeaders += '               <div class="col-lg-2 text-right" style="padding-top: 5px">' + arryHeader[i] + '</div>';
                    appendHeaders += ' <div class="col-lg-2">';
                    appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[i] + '" readonly="readonly"/>';
                    appendHeaders += '</div>';
                    appendHeaders += '</div>';
                }
                isNotComplete = "Success";
                sessionStorage.setItem('strTable', records2);

                $('#txtWorkflow').val(strWorkflow);
                $('.dvAppendWorkload').append(appendHeaders);
                $('#modalLoading').modal('hide');
            }
            else if (d.Message == 'NotCompleted')
            {
                var records = d.data.asd;
                var records1 = d.data.asd1;
                var records2 = d.data.asd2;
                var appendHeaders = "";
                var strWorkflow = "";
                var arryValue = records1.split('+');
                var arryHeader = [];
                var arryHeaderVal = [];
                Workflow_id = arryValue[arryValue.length - 1];
                WorkingTime = arryValue[arryValue.length - 2];
                WorkingTime = WorkingTime.replace(':', '');
                WorkingTime = WorkingTime.replace(':', '');
                WorkingTime = WorkingTime.substring(1);
                $.each(records, function (idx, val) {
                    arryHeader.push(val.Headers);
                    //appendHeaders += '<div class="row topStyle dvRemoveWorkable" >';
                    //appendHeaders += '               <div class="col-lg-2 " style="padding-top: 5px">' + val.Headers + '</div>';
                    //appendHeaders += ' <div class="col-lg-2">';
                    //appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[idx] + '"/>';
                    //appendHeaders += '</div>';
                    //appendHeaders += '</div>';
                    strWorkflow = val.Workflow;

                });
                $('.dvRemoveWorkable').remove();
                for (i = 1 ; i < arryHeader.length; i = i + 2) {
                    var intHeader = i - 1;

                    appendHeaders += '<div class="row topStyle dvRemoveWorkable" >';
                    appendHeaders += '               <div class="col-lg-3 text-right" style="padding-top: 5px">' + arryHeader[intHeader] + '</div>';
                    appendHeaders += ' <div class="col-lg-2">';
                    appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[intHeader] + '" readonly="readonly"/>';
                    appendHeaders += '</div>';
                    appendHeaders += '               <div class="col-lg-2 text-right" style="padding-top: 5px">' + arryHeader[i] + '</div>';
                    appendHeaders += ' <div class="col-lg-2">';
                    appendHeaders += '  <input type="text" class="form-control" value="' + arryValue[i] + '" readonly="readonly"/>';
                    appendHeaders += '</div>';
                    appendHeaders += '</div>';
                }
                isNotComplete = "NotCompleted";
                sessionStorage.setItem('strTable', records2);

                $('#txtWorkflow').val(strWorkflow);
                $('.dvAppendWorkload').append(appendHeaders);
                $('#modalLoading').modal('hide');
               
            }
            else
            {
                initialTime = Date.now();
                isPaused = true;
                $('.dvNotReadySlc,.dvNotReady').show();
                $('.dvWorkingslc,.dvWorking').hide();
                $('#modalLoading').modal('hide');
            }
       
        },
        error: function (response) {
            initialTime = Date.now();
            isPaused = true;
            $('.dvNotReadySlc,.dvNotReady').show();
            $('.dvWorkingslc,.dvWorking').hide();
            $('#modalLoading').modal('hide');
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#btnSaveWorkload').click(function () {

    console.log($('#lblTimers').html());
    console.log($('#slcDisposition').val());
    console.log($('#slcRemark').val());

    var duration = $('#lblTimers').html();
    var workStatus = $('#slcStatusWorking').val();

    if ($('#slcDisposition').val() == 'Completed') {
        var disposition = $('#slcDisposition').val();
        var remark = $('#slcRemark').val() == '0' ? '-' : $('#slcRemark').val();

        $.ajax({
            type: 'POST',
            url: 'MyWorkflow.aspx/Save',
            data: '{"duration" : "' + duration + '","workStatus" : "' + workStatus +
                '","disposition" : "' + disposition + '","remarks" : "' + remark + '","strTable" : "' + sessionStorage.getItem('strTable') +
                '","strInEs":"' + isNotComplete + '","Workflow_id":"' + Workflow_id + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('#slcDisposition').val('0');
                $('#slcRemark').val('0');
                initialTime = Date.now();
                fnGetWorkload();
            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }
    else if (($('#slcDisposition').val() == '0' && $('#slcRemark').val() == '0') ||
         ($('#slcDisposition').val() == 'In progress' && $('#slcRemark').val() == '0') ||
        ($('#slcDisposition').val() == 'Escalated' && $('#slcRemark').val() == '0') || $('#slcDisposition').val() == '0') {
        console.log('Not Saved');
    }
    else {
        var disposition = $('#slcDisposition').val();
        var remark = $('#slcRemark').val() == '0' ? '-' : $('#slcRemark').val();

        $.ajax({
            type: 'POST',
            url: 'MyWorkflow.aspx/Save',
            data: '{"duration" : "' + duration + '","workStatus" : "' + workStatus +
                  '","disposition" : "' + disposition + '","remarks" : "' + remark + '","strTable" : "' + sessionStorage.getItem('strTable') +
                  '","strInEs":"' + isNotComplete + '","Workflow_id":"' + Workflow_id + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                
                $('#slcDisposition').val('0');
                $('#slcRemark').val('0');
                initialTime = Date.now();
                fnGetWorkload();
            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }






});
$('#slcDisposition').change(function () {
    console.log($('#slcDisposition').val());
    if ($('#slcDisposition').val() == 'Completed') {
        $('#slcRemark').prop('disabled', true);
        $('#slcRemark').val('0');
    }
    else {
        $('#slcRemark').prop('disabled', false);
    }

});






