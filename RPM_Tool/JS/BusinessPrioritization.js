﻿var countAppendState = 1;
var countWorkflow = 0;
var countPrioState = 0;
var test = "";
var idUpdate = "";



$(document).ready(function () {
var d = new Date(1458619200000);
var myTimezone = "America/Toronto";
var myDatetimeFormat = "YYYY-MM-DD hh:mm:ss a z";
//var time = myDatetimeString = moment(d).tz(myTimezone).format(myDatetimeFormat);
    fnStateList();
    fnWorkflowVal();
    fnSettingsPrio();
    fnBusinessPrio();
    
    
   
});
$('#btnrefresh').click(function () {
    //var mydata = [strclientname, selector1, selector2, selector3, selector4, selector5, selector6, selector7, selector8, selector9, selector10, selector11, selector12, selector13, selector14, nt, Categoryname, Slctconline2, slccategoryname2, Slctconline3];
    $.ajax({
        type: 'POST',
        url: 'BusinessPrioritization.aspx/gettime',
        //data: "{mydata:' + JSON.stringify(mydata) + '"}",
        //data: '{mydata: ' + JSON.stringify(mydata) + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: "json",

        success: function (data) {
            alertify.alert("New Rules", "Successfully saved.");
         
            $('#myModal').modal('hide');
           
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });


});
$('#btnCurrent').click(function () {
    $('#dvCurrent').show();
    $('#dvFuture').hide();
    $('#dvSetting').hide();
    $("#btnCurrent").removeClass("btn-default").addClass("btn-primary");
    $("#btnFuture").removeClass("btn-primary").addClass("btn-default");
    $("#btnSettings").removeClass("btn-primary").addClass("btn-default");
});
$('#btnFuture').click(function () {
    $('#dvCurrent').hide();
    $('#dvFuture').show();
    $('#dvSetting').hide();
    $("#btnCurrent").removeClass("btn-primary").addClass("btn-default");
    $("#btnFuture").removeClass("btn-default").addClass("btn-primary");
    $("#btnSettings").removeClass("btn-primary").addClass("btn-default");
});
$('#btnSettings').click(function () {
    $('#dvCurrent').hide();
    $('#dvFuture').hide();
    $('#dvSetting').show();
    $("#btnCurrent").removeClass("btn-primary").addClass("btn-default");
    $("#btnFuture").removeClass("btn-primary").addClass("btn-default");
    $("#btnSettings").removeClass("btn-default").addClass("btn-primary");
});

$('#btnSave').click(function () {
    var Priority = "";
    var Workflow = "";
    var State = "";
    var EffectiveFor = "";
    var EffectiveFrom = "";
    var EffectiveTo = "";

    for (i = 1; i <= 3; i++) {
        Priority += (Priority == "" ? $('.dvPrioritize' + i + '').find('.lblPrioritize').text() : "," +
                    $('.dvPrioritize' + i + '').find('.lblPrioritize').text());
    }
    for (i = 0; i < countWorkflow; i++) {
        for (ii = 0; ii < countWorkflow; ii++) {

            if ($('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() == "") {

            }
            else {
                Workflow += (Workflow == "" ? $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() : "," + $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text());
            }
        }
    }

    State = $('#slcState').val()
    for (i = 1; i <= countPrioState; i++) {
        State += "," + $('#slcState' + i + '').val()
    }



    if ($('#chkFor').prop('checked')) {
        EffectiveFor = $('#slcDays').val() + ' ' + $('#slcMHDWM').val() + '(s), ' + $('#slcForHr').val() + ':' + $('#slcForMins').val() + ' ' + $('#slcForAMPM').val();

        EffectiveTo = $('#txtToDate').val() + " " + $('#slcToHr').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcAMPM').val();
    }
    else if ($('#chkFrom').prop('checked')) {
        EffectiveFrom = $('#txtFromDate').val() + " " + $('#slcFromHr').val() + ':' + $('#slcFromMins').val() + ' ' + $('#slcFromAMPM').val()
        EffectiveTo = $('#txtToDate').val() + " " + $('#slcToHr').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcAMPM').val();
    }
    else {

    }

    console.log(Priority);
    console.log(Workflow);
    console.log(State);
    console.log(EffectiveFor);
    console.log(EffectiveFrom);
    console.log(EffectiveTo);

    $.ajax({
        type: 'POST',
        url: 'BusinessPrioritization.aspx/SaveBusinessPrio',
        data: '{"Priority" : "' + Priority + '","Workflow" : "' + Workflow + '","State" : "' + State +
            '","EffectiveFor" : "' + EffectiveFor + '","EffectiveFrom" : "' + EffectiveFrom + '","EffectiveTo" : "' + EffectiveTo + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
            setTimeout(function () {
                $('#SaveAlert').modal('hide');
                fnBusinessPrio();
                fnStateList();
                fnWorkflowVal();
                fnSettingsPrio();

                for (i = 1; i <= countAppendState; i++) {
                    $('.dvState' + i + '').remove();
                }

                $('#slcDays').val('00');
                $('#slcMHDWM').val('');
                $('#slcForHr').val('00');
                $('#slcForMins').val('00');
                $('#slcForAMPM').val('AM/PM');
                $('#txtFromDate').val('');
                $('#slcFromHr').val('00');
                $('#slcFromMins').val('00');
                $('#slcFromAMPM').val('AM/PM');
                $('#txtToDate').val('');
                $('#slcToHr').val('00');
                $('#slcToMins').val('00');
                $('#slcAMPM').val('AM/PM');

                $('#chkFor').prop('checked', false);
                $('#chkFrom').prop('checked', false);

                countAppendState = 1;
                countPrioState = 0;
            }, 2000);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});

function idFormatter1(value) {
    var split = value.split(',');
    var appendLabel = "";

    for (i = 0; i < split.length; i++) {
        var values = i + 1
        appendLabel += values + ")" + split[i] + " </br>"
    }
    return appendLabel;
}
function idFormatter3(value, row) {
    var appendLabel = row.Effective_For + row.Effective_From + '</br>' + row.Effective_To;
    return appendLabel;
}
function dateFormat(value, row, index) {
    return moment(value).format('MM/DD/YYYY HH:mm:ss');
}
function EditDelete(value, row, index) {
    return '<button type="button" value="' + value + '" class="btn btn-link btnEdit"><i class="far fa-edit fa-lg"></i></button><button value="' + value
        + '"  type="button" class="btn btn-link btnDelete"><i class="far fa-trash-alt text-red fa-lg"></i></button>';
}
function fnBusinessPrio() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'BusinessPrioritization.aspx/displayBusinessPrio',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblBusinessPrio').bootstrapTable('destroy');
                $('#tblBusinessPrio').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}




$('#chkFor').click(function () {
    $('#chkFrom').prop('checked', false);
});
$('#chkFrom').click(function () {
    $('#chkFor').prop('checked', false);
});

$('#drag1').click(function () {
});
$(".dvSettingsPrioDrop").on("click", "#dragPrio1", function () {

    if ($('#dragPrio1').find('.lblPrioritize').text() == "SLA") {

    }
    else if ($('#dragPrio1').find('.lblPrioritize').text() == "Workflow") {
        $('.dvWorkflow').show();
        $('.dvStatePrio').hide();
    }
    else {
        $('.dvStatePrio').show();
        $('.dvWorkflow').hide();
    }


    //$('.dvStatePrio').show();
    //$('.dvWorkflow').hide();
});
$(".dvSettingsPrioDrop").on("click", "#dragPrio2", function () {
    console.log($('#dragPrio2').find('.lblPrioritize').text());
    if ($('#dragPrio2').find('.lblPrioritize').text() == "SLA") {

    }
    else if ($('#dragPrio2').find('.lblPrioritize').text() == "Workflow") {
        $('.dvWorkflow').show();
        $('.dvStatePrio').hide();
    }
    else {
        $('.dvStatePrio').show();
        $('.dvWorkflow').hide();
    }

});
$(".dvSettingsPrioDrop").on("click", "#dragPrio3", function () {
    console.log($('#dragPrio3').find('.lblPrioritize').text());
    if ($('#dragPrio3').find('.lblPrioritize').text() == "SLA") {

    }
    else if ($('#dragPrio3').find('.lblPrioritize').text() == "Workflow") {
        $('.dvWorkflow').show();
        $('.dvStatePrio').hide();
    }
    else {
        $('.dvStatePrio').show();
        $('.dvWorkflow').hide();
    }

});
function fnWorkflowVal() {
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';

    var appendDesignSub = '';
    var appendDropZoneSub = '';
    $('.dvRefresh').remove();
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var slcVal;
                var slcValSub;
                var ListOption = "";
                //$.each(records, function (idx, val) {
                //    slcVal = val.List_Option.split(',');
                //    countWorkflow = 0;
                //});
                $.each(records, function (idx, val) {
                    ListOption += ListOption == "" ? val.List_Option : "," + val.List_Option;

                    countWorkflow = 0;
                });
                slcVal = ListOption.split(',');
                for (i = 0; i < slcVal.length; i++) {
                    //ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                    appendDesign += '<div class="row topStyle dvRefresh">';
                    appendDesign += ' <div class="col-lg-12">';
                    appendDesign += '   <div class="panelBox pointerCursor"  ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendDesign += '       <div id="drag' + i + '" draggable="true" ondragstart="drag(event)" >';
                    appendDesign += '           <div class="col-lg-9" style="padding-left: 0px">';
                    appendDesign += '               <label class="pointerCursor" style="font-size:12px"  id="lblWorkflow' + i + '" value="' + slcVal[i] + '">' + slcVal[i] + '</label>';
                    appendDesign += '           </div>';
                    appendDesign += '           <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    appendDesign += '               <label class= "txtValSubWorkFlow' + i + '" style="display:none"></label> <button type="button" class="btn btn-link btn-sm btnRenewal"  value="' +
                                                        slcVal[i] + '" style="padding-top: 0px"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    appendDesign += '           </div>';
                    appendDesign += '       </div>';
                    appendDesign += '   </div>';
                    appendDesign += '</div>';
                    appendDesign += '</div>';


                    appendNum += '<div class="row topStyle dvRefresh">';
                    appendNum += '<div class="col-lg-12">';
                    appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendNum += i + 1;
                    appendNum += '</div></div></div>';


                    appendDropZone += '<div class="row topStyle dvValWorkflowPrio' + i + ' dvRefresh" >';
                    appendDropZone += '<div class="col-lg-12">';
                    appendDropZone += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendDropZone += '</div>';
                    appendDropZone += ' </div>';


                    countWorkflow++;
                }
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);
                $('.dvPrioNum').append(appendNum);


                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function fnStateList() {

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlStateList',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd

                $('#slcState').empty();
                $('#slcState').append("<option value='0'  selected='selected' >Select</option>");
                $.each(records, function (idx, val) {
                    ta1 = "<option value='" + val.State_Desc + "' >" + val.State_Desc + "</option>";
                    $('#slcState').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnAddPrio').click(function () {
    var appendPrio = '';
    var suffixVal = countAppendState + 1;
    appendPrio += '<div class="row dvState' + countAppendState + ' topStyle dvResetDDL">';
    appendPrio += '<div class="col-lg-2" style="padding-right: 0px; padding-top: 4px">' + ordinal_suffix_of(suffixVal) + ' Priority</div>';
    appendPrio += '<div class="col-lg-4">';
    appendPrio += '<select id="slcState' + countAppendState + '" class="form-control input-sm tb1">';
    appendPrio += '</select>';
    appendPrio += '</div>';
    appendPrio += '<div class="col-lg-2 dvRemoveDelete btnDeletePrio" style="padding-left: 0px">';
    appendPrio += '<button type="button" id="btnDeleteAppend" class="btn btn-link fa-lg"><i class="fas fa-trash-alt text-red"></i></button>';
    appendPrio += '</div>';
    appendPrio += '</div>';
    $('.dvRemoveDelete').remove();
    $('.dvAppendPrio').append(appendPrio);

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlStateList',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var countNo = countAppendState - 1;
                $('#slcState' + countNo + '').empty();
                $('#slcState' + countNo + '').append("<option value='0'  selected='selected' >Select</option>");
                $.each(records, function (idx, val) {
                    ta1 = "<option value='" + val.State_Desc + "' >" + val.State_Desc + "</option>";
                    $('#slcState' + countNo + '').append(ta1);
                });
                countPrioState++;
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

    countAppendState++;
});
$(".dvAppendPrio").on("click", "#btnDeleteAppend", function () {
    var dvRemoveVal = countAppendState - 1;
    var dvRemoveButton = dvRemoveVal - 1;
    var appendRemoveBTN = '<div class="col-lg-2 dvRemoveDelete btnDeletePrio" style="padding-left: 0px">';
    appendRemoveBTN += '<button type="button" id="btnDeleteAppend" class="btn btn-link fa-lg"><i class="fas fa-trash-alt text-red"></i></button>';
    appendRemoveBTN += '</div>';
    $('.dvState' + dvRemoveVal + '').remove();
    $('.dvState' + dvRemoveButton + '').append(appendRemoveBTN);
    countPrioState--;
    countAppendState--;
});





function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function fnSettingsPrio() {
    var appendSettings = '';
    var appendSettingsDrop = '';

    $('.dvDestroySettingsPrio').remove();

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/GetSettingsPrio',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var NoVal = 1;
                $.each(records, function (idx, val) {
                    appendSettings += '<div class="row topStyle dvDestroySettingsPrio">';
                    appendSettings += '                  <div class="col-lg-12">';
                    appendSettings += '                      <div class="panelBox pointerCursor">';
                    appendSettings += '                          <div id="dragPrio' + NoVal + '" value="' + val.Prio_Name + '" draggable="true" ondragstart="drag(event)">';
                    appendSettings += '                              <div class="col-lg-9" style="padding-left: 0px">';
                    appendSettings += '                                  <label class="pointerCursor lblPrioritize">' + val.Prio_Name + '</label>';
                    appendSettings += '                              </div>';
                    appendSettings += '                          </div>';
                    appendSettings += '                      </div>';
                    appendSettings += '                  </div>';
                    appendSettings += '              </div>';

                    appendSettingsDrop += '<div class="row topStyle dvPrioritize' + NoVal + ' dvDestroySettingsPrio">';
                    appendSettingsDrop += '                                <div class="col-lg-12">';
                    appendSettingsDrop += '                     <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendSettingsDrop += '                 </div>';
                    appendSettingsDrop += '             </div>';


                    NoVal++;
                });
                $('.dvSettingsPrio').append(appendSettings);
                $('.dvSettingsPrioDrop').append(appendSettingsDrop);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

}


$("#tblBusinessPrio").on("click", ".btnEdit", function () {
    console.log($(this).val());

    idUpdate = $(this).val();

    $('.dvDestroySettingsPrio').remove();
    $('.dvRefresh').remove();
    $('.dvResetDDL').remove();
    $('#dvCurrent').hide();
    $('#dvFuture').hide();
    $('#dvSetting').show();
    $("#btnCurrent").removeClass("btn-primary").addClass("btn-default");
    $("#btnFuture").removeClass("btn-primary").addClass("btn-default");
    $("#btnSettings").removeClass("btn-default").addClass("btn-primary");

    $('#slcDays').val('00');
    $('#slcMHDWM').val('');
    $('#slcForHr').val('00');
    $('#slcForMins').val('00');
    $('#slcForAMPM').val('AM/PM');
    $('#txtFromDate').val('');
    $('#slcFromHr').val('00');
    $('#slcFromMins').val('00');
    $('#slcFromAMPM').val('AM/PM');
    $('#txtToDate').val('');
    $('#slcToHr').val('00');
    $('#slcToMins').val('00');
    $('#slcAMPM').val('AM/PM');

    $('.dvUpdatebtn').show();
    $('.dvSavebtn').hide();

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"id" : "' + $(this).val() + '"}',
        url: 'BusinessPrioritization.aspx/editSettings',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var Priority;
                var Workflow;
                var State;
                var Effective_for;
                var Effective_From;
                var Effective_To;
                var appendSettingsDrop = "";
                var appendSettings = "";
                var appendWorkflowsDrop = "";
                var appendWorkflow = "";
                var appendWorkflowNum = "";
                var appendPrio = '';

                var NoVal = 1
                var NoState = 1;
                $.each(records, function (idx, val) {
                    Priority = val.Priority.split(',');
                    Workflow = val.Workflow.split(',');
                    State = val.State.split(',');
                    Effective_for = val.Effective_for == undefined ? "" : val.Effective_for.split(' ');
                    Effective_From = val.Effective_From == undefined ? "" : val.Effective_From.split(' ');
                    Effective_To = val.Effective_To == undefined ? "" : val.Effective_To.split(' ');
                });

                for (i = 0; i < Priority.length; i++) {
                    appendSettings += '<div class="row topStyle dvDestroySettingsPrio">';
                    appendSettings += '                  <div class="col-lg-12">';
                    appendSettings += '                      <div class="panelBox pointerCursor drag"  ondrop="drop(event)"  ondragover="allowDrop(event)">';
                    appendSettings += '                      </div>';
                    appendSettings += '                  </div>';
                    appendSettings += '              </div>';

                    appendSettingsDrop += '<div class="row topStyle dvPrioritize' + NoVal + ' dvDestroySettingsPrio">';
                    appendSettingsDrop += '                                <div class="col-lg-12">';
                    appendSettingsDrop += '                     <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';

                    appendSettingsDrop += '                          <div id="dragPrio' + NoVal + '" value="' + Priority[i] + '" draggable="true" ondragstart="drag(event)">';
                    appendSettingsDrop += '                              <div class="col-lg-9" style="padding-left: 0px">';
                    appendSettingsDrop += '                                  <label class="pointerCursor lblPrioritize">' + Priority[i] + '</label>';
                    appendSettingsDrop += '                              </div>';
                    appendSettingsDrop += '                          </div>';

                    appendSettingsDrop += '                 </div>';
                    appendSettingsDrop += '                 </div>';
                    appendSettingsDrop += '             </div>';

                    NoVal++;
                }
                for (i = 0; i < Workflow.length; i++) {
                    appendWorkflow += '<div class="row topStyle dvRefresh">';
                    appendWorkflow += ' <div class="col-lg-12">';
                    appendWorkflow += '   <div class="panelBox pointerCursor"  ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';

                    appendWorkflow += '   </div>';
                    appendWorkflow += '</div>';
                    appendWorkflow += '</div>';

                    appendWorkflowNum += '<div class="row topStyle dvRefresh">';
                    appendWorkflowNum += '<div class="col-lg-12">';
                    appendWorkflowNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendWorkflowNum += i + 1;
                    appendWorkflowNum += '</div></div></div>';

                    appendWorkflowsDrop += '<div class="row topStyle dvValWorkflowPrio' + i + ' dvRefresh" >';
                    appendWorkflowsDrop += '<div class="col-lg-12">';
                    appendWorkflowsDrop += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendWorkflowsDrop += '       <div id="drag' + i + '" draggable="true" ondragstart="drag(event)" >';
                    appendWorkflowsDrop += '           <div class="col-lg-9" style="padding-left: 0px">';
                    appendWorkflowsDrop += '               <label class="pointerCursor" style="font-size:12px"  id="lblWorkflow' + i + '" value="' + Workflow[i] + '">' + Workflow[i] + '</label>';
                    appendWorkflowsDrop += '           </div>';
                    appendWorkflowsDrop += '           <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    appendWorkflowsDrop += '               <button type="button" class="btn btn-link btn-sm btnRenewal" value="' + Workflow[i] + '" style="padding-top: 0px"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    appendWorkflowsDrop += '           </div>';
                    appendWorkflowsDrop += '       </div>';
                    appendWorkflowsDrop += '</div>';
                    appendWorkflowsDrop += '</div>';
                    appendWorkflowsDrop += ' </div>';
                    countWorkflow++;
                }

                for (i = 0; i < State.length; i++) {
                    NoState = i + 1;

                    if (i == (State.length - 1)) {
                        appendPrio = '<div class="row dvState' + i + ' topStyle dvResetDDL">';
                        appendPrio += '<div class="col-lg-2" style="padding-right: 0px; padding-top: 4px">' + ordinal_suffix_of(NoState) + ' Priority</div>';
                        appendPrio += '<div class="col-lg-4">';
                        appendPrio += '<select id="slcState' + i + '" class="form-control input-sm tb1">';
                        appendPrio += '</select>';
                        appendPrio += '</div>';
                        appendPrio += '<div class="col-lg-2 dvRemoveDelete btnDeletePrio" style="padding-left: 0px">';
                        appendPrio += '<button type="button" id="btnDeleteAppend" class="btn btn-link fa-lg"><i class="fas fa-trash-alt text-red"></i></button>';
                        appendPrio += '</div>';
                        appendPrio += '</div>';
                 
                        $('.dvAppendPrio').append(appendPrio);
                        DisplaySelectVl(i, State[i]);
                    }
                    else if (i == 0) {
                        $('#slcState').val(State[i])
                    }
                    else {
                        appendPrio = '<div class="row dvState' + i + ' topStyle dvResetDDL">';
                        appendPrio += '<div class="col-lg-2" style="padding-right: 0px; padding-top: 4px">' + ordinal_suffix_of(NoState) + ' Priority</div>';
                        appendPrio += '<div class="col-lg-4">';
                        appendPrio += '<select id="slcState' + i + '" class="form-control input-sm tb1">';
                        appendPrio += '</select>';
                        appendPrio += '</div>';
                        appendPrio += '<div class="col-lg-2 dvRemoveDelete btnDeletePrio" style="padding-left: 0px">';
                        appendPrio += '<button type="button" id="btnDeleteAppend" class="btn btn-link fa-lg"><i class="fas fa-trash-alt text-red"></i></button>';
                        appendPrio += '</div>';
                        appendPrio += '</div>';
                  
                        $('.dvAppendPrio').append(appendPrio);
                        $('.dvRemoveDelete').remove();
                        DisplaySelectVl(i, State[i]);
                    }

                }

                for (i = 0; i < Effective_for.length; i++) {
                    if(i == 0)
                    {
                        $("#chkFor").attr('checked', true);
                        $("#chkFrom").attr('checked', false);
                    }
                    else if(i== 1)
                    {
                        $('#slcDays').val(Effective_for[i]);
                    }
                    else if (i == 2) {
                        $('#slcMHDWM').val(Effective_for[i].replace('(s),', ''));
                    }
                    else if (i == 3 ) {
                        var GetHHMM = Effective_for[3].split(':');

                        $('#slcForHr').val(GetHHMM[0]);
                        $('#slcForMins').val(GetHHMM[1]);
                    }
                    else if (i == 4) {
                        $('#slcForAMPM').val(Effective_for[i]);
                    }
                    else
                    {
                        $('').val();
                    }
                }
            
                if (Effective_From.length == 1)
                {

                }
                else
                {
                    for (i = 0; i < Effective_From.length; i++) {
                        if (i == 0) {
                            $("#chkFor").attr('checked', false);
                            $("#chkFrom").attr('checked', true);
                        }
                        else if (i == 1) {
                            $('#txtFromDate').val(Effective_From[i]);
                        }
                        else if (i == 2) {
                            var GetHHMM = Effective_From[2].split(':');

                            $('#slcFromHr').val(GetHHMM[0]);
                            $('#slcFromMins').val(GetHHMM[1]);
                        }
                        else if (i == 3) {
                            $('#slcFromAMPM').val(Effective_From[i]);
                        }
                        else {

                        }
                    }
                }
                for (i = 0; i < Effective_To.length; i++) {
                    if (i == 0) {
                      
                    }
                    else if (i == 1) {
                        $('#txtToDate').val(Effective_To[i]);
                    }
                    else if (i == 2) {
                        var GetHHMM = Effective_To[2].split(':');

                        $('#slcToHr').val(GetHHMM[0]);
                        $('#slcToMins').val(GetHHMM[1]);
                    }
                    else if (i == 3) {
                        $('#slcToAMPM').val(Effective_To[i]);
                    }
                    else {

                    }
                }

                $('.dvPrioDropZone').append(appendWorkflowsDrop);
                $('.dvPrio').append(appendWorkflow);
                $('.dvPrioNum').append(appendWorkflowNum);

                $('.dvSettingsPrio').append(appendSettings);
                $('.dvSettingsPrioDrop').append(appendSettingsDrop);
                $('#modalLoading').modal('hide');




            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });


});
function DisplaySelectVl(NoState, valState) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlStateList',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var countNo = countAppendState - 1;
                $('#slcState' + NoState + '').empty();
                $('#slcState' + NoState + '').append("<option value='0'  selected='selected' >Select</option>");
                $.each(records, function (idx, val) {
                    ta1 = "<option value='" + val.State_Desc + "' >" + val.State_Desc + "</option>";
                    $('#slcState' + NoState + '').append(ta1);
                });
                $('#slcState' + NoState + '').val(valState)
                countPrioState++;
                countAppendState++;
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$("#tblBusinessPrio").on("click", ".btnDelete", function () {
    console.log($(this).val());

    $.ajax({
        type: 'POST',
        url: 'BusinessPrioritization.aspx/DeleteBusinessPrior',
        data: '{"id" : "' + $(this).val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            fnBusinessPrio();
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});
$('#btnCancel').click(function () {
    fnBusinessPrio();
    fnStateList();
    fnWorkflowVal();
    fnSettingsPrio();

    for (i = 1; i <= countAppendState; i++) {
        $('.dvState' + i + '').remove();
    }

    $('#slcDays').val('00');
    $('#slcMHDWM').val('');
    $('#slcForHr').val('00');
    $('#slcForMins').val('00');
    $('#slcForAMPM').val('AM/PM');
    $('#txtFromDate').val('');
    $('#slcFromHr').val('00');
    $('#slcFromMins').val('00');
    $('#slcFromAMPM').val('AM/PM');
    $('#txtToDate').val('');
    $('#slcToHr').val('00');
    $('#slcToMins').val('00');
    $('#slcAMPM').val('AM/PM');

    $('#chkFor').prop('checked', false);
    $('#chkFrom').prop('checked', false);


     countAppendState = 1;
     countPrioState = 0;

     $('.dvUpdatebtn').hide();
     $('.dvSavebtn').show();



});


$('#btnUpdate').click(function () {
    var Priority = "";
    var Workflow = "";
    var State = "";
    var EffectiveFor = "";
    var EffectiveFrom = "";
    var EffectiveTo = "";

    for (i = 1; i <= 3; i++) {
        Priority += (Priority == "" ? $('.dvPrioritize' + i + '').find('.lblPrioritize').text() : "," +
                    $('.dvPrioritize' + i + '').find('.lblPrioritize').text());
    }
    for (i = 0; i < countWorkflow; i++) {
        for (ii = 0; ii < countWorkflow; ii++) {

            if ($('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() == "") {

            }
            else {
                Workflow += (Workflow == "" ? $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() : "," + $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text());
            }
        }
    }

    State = $('#slcState').val()
    for (i = 1; i <= countPrioState; i++) {
        State += "," + $('#slcState' + i + '').val()
    }



    if ($('#chkFor').prop('checked')) {
        EffectiveFor = $('#slcDays').val() + ' ' + $('#slcMHDWM').val() + '(s), ' + $('#slcForHr').val() + ':' + $('#slcForMins').val() + ' ' + $('#slcForAMPM').val();

        EffectiveTo = $('#txtToDate').val() + " " + $('#slcToHr').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcAMPM').val();
    }
    else if ($('#chkFrom').prop('checked')) {
        EffectiveFrom = $('#txtFromDate').val() + " " + $('#slcFromHr').val() + ':' + $('#slcFromMins').val() + ' ' + $('#slcFromAMPM').val()
        EffectiveTo = $('#txtToDate').val() + " " + $('#slcToHr').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcAMPM').val();
    }
    else {

    }

    console.log(Priority);
    console.log(Workflow);
    console.log(State);
    console.log(EffectiveFor);
    console.log(EffectiveFrom);
    console.log(EffectiveTo);

    $.ajax({
        type: 'POST',
        url: 'BusinessPrioritization.aspx/UpdateBusinessPrio',
        data: '{"Priority" : "' + Priority + '","Workflow" : "' + Workflow + '","State" : "' + State +
            '","EffectiveFor" : "' + EffectiveFor + '","EffectiveFrom" : "' + EffectiveFrom + '","EffectiveTo" : "' + EffectiveTo +
            '","id" : "' + idUpdate + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
            setTimeout(function () {
                $('#SaveAlert').modal('hide');
                fnBusinessPrio();
                fnStateList();
                fnWorkflowVal();
                fnSettingsPrio();

                for (i = 1; i <= countAppendState; i++) {
                    $('.dvState' + i + '').remove();
                }

                $('#slcDays').val('00');
                $('#slcMHDWM').val('');
                $('#slcForHr').val('00');
                $('#slcForMins').val('00');
                $('#slcForAMPM').val('AM/PM');
                $('#txtFromDate').val('');
                $('#slcFromHr').val('00');
                $('#slcFromMins').val('00');
                $('#slcFromAMPM').val('AM/PM');
                $('#txtToDate').val('');
                $('#slcToHr').val('00');
                $('#slcToMins').val('00');
                $('#slcAMPM').val('AM/PM');

                $('#chkFor').prop('checked', false);
                $('#chkFrom').prop('checked', false);

                $('.dvUpdatebtn').hide();
                $('.dvSavebtn').show();
                countAppendState = 1;
                countPrioState = 0;

            }, 2000);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});




$(".dvPrio").on("click", ".btnRenewal", function () {
    //$('#myModal').modal({ backdrop: 'static', keyboard: false });
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';


    AssignVal = '';
    $('#lblSubTask').text($('.dvPrio').find(this).val());
    var appendDesignSub = '';
    var appendDropZoneSub = '';
    $('.dvSubPrio').find('.dvRemoveSub').remove();
    $('.dvSubPrioDropZone').find('.dvRemoveSubDropZone').remove();
    $('.dvSubPrioNum').find('.removeNum').remove();
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlValuesVal',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);
                countsubTask = 0;
                $.each(records4, function (idx, val) {
                    splitVal += splitVal == "" ? val.Sublist_Option : "," + val.Sublist_Option;
                });
                slcValSub = splitVal.split(',');



                for (i = 0; i < slcValSub.length; i++) {
                    //ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";

                    appendDesignSub += '<div class="row topStyle dvRemoveSub">';
                    appendDesignSub += '<div class="col-lg-12">';
                    appendDesignSub += ' <div class="panelBox pointerCursor" ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendDesignSub += '     <div id="dragSub' + i + '" draggable="true" ondragstart="drag(event)">';
                    appendDesignSub += '         <div class="col-lg-12" style="padding-left: 0px">';
                    appendDesignSub += '             <label class="pointerCursor"  style="font-size:12px" id="lblSubTask' + i + '">' + slcValSub[i] + '</label>';
                    appendDesignSub += '         </div>';
                    //appendDesignSub += '         <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    ////appendDesignSub += '             <button type="button" class="btn btn-link btn-sm" style="padding-top: 0px" data-toggle="modal" data-target="#myModal"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    //appendDesignSub += '         </div>';
                    appendDesignSub += '     </div>';
                    appendDesignSub += ' </div>';
                    appendDesignSub += '</div>';
                    appendDesignSub += '</div>';


                    appendNum += '<div class="row topStyle removeNum">';
                    appendNum += '<div class="col-lg-12">';
                    appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendNum += i + 1;
                    appendNum += '</div></div></div>';

                    appendDropZoneSub += '<div class="row topStyle dvRemoveSubDropZone dvValSubTaskPrio' + i + '">';
                    appendDropZoneSub += '<div class="col-lg-12">';
                    appendDropZoneSub += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendDropZoneSub += '</div>';
                    appendDropZoneSub += '</div>';
                    countsubTask++
                }
                $('.dvSubPrioDropZone').append(appendDropZoneSub);
                $('.dvSubPrio').append(appendDesignSub);
                $('.dvSubPrioNum').append(appendNum);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$(".dvPrioDropZone").on("click", ".btnRenewal", function () {
    $('#myModal').modal({ backdrop: 'static', keyboard: false });
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';

    var appendDesignSub = '';
    var appendDropZoneSub = '';
    console.log($('.dvPrioDropZone').find(this).val());
    AssignVal = $('.dvPrioDropZone').find(this).val();
    $('#lblSubTask').text($('.dvPrioDropZone').find(this).val());
    $('.dvSubPrioNum').find('.removeNum').remove();
    $('.dvSubPrio').find('.dvRemoveSub').remove();
    $('.dvSubPrioDropZone').find('.dvRemoveSubDropZone').remove();

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'BusinessPrioritization.aspx/ddlValuesSubWorkflow',
        data: '{"workflow" : "' + $(this).val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);

                $.each(records4, function (idx, val) {
                    splitVal += splitVal == "" ? val.Sublist_Option : "," + val.Sublist_Option;
                });
                slcValSub = splitVal.split(',');

                for (i = 0; i < slcValSub.length; i++) {
                    //ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";

                    appendDesignSub += '<div class="row topStyle dvRemoveSub">';
                    appendDesignSub += '<div class="col-lg-12">';
                    appendDesignSub += ' <div class="panelBox pointerCursor" ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendDesignSub += '     <div  id="dragSub' + i + '" draggable="true" ondragstart="drag(event)">';
                    appendDesignSub += '         <div class="col-lg-9 " style="padding-left: 0px">';
                    appendDesignSub += '             <label class="pointerCursor" style="font-size:12px" id="lblSubTask' + i + '">' + slcValSub[i] + '</label>';
                    appendDesignSub += '         </div>';
                    appendDesignSub += '         <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    //appendDesignSub += '             <button type="button" class="btn btn-link btn-sm" style="padding-top: 0px" data-toggle="modal" data-target="#myModal"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    appendDesignSub += '         </div>';
                    appendDesignSub += '     </div>';
                    appendDesignSub += ' </div>';
                    appendDesignSub += '</div>';
                    appendDesignSub += '</div>';


                    appendNum += '<div class="row topStyle removeNum">';
                    appendNum += '<div class="col-lg-12">';
                    appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendNum += i + 1;
                    appendNum += '</div></div></div>'; 

                    appendDropZoneSub += '<div class="row topStyle dvRemoveSubDropZone dvValSubTaskPrio' + i + '">';
                    appendDropZoneSub += '<div class="col-lg-12">';
                    appendDropZoneSub += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendDropZoneSub += '</div>';
                    appendDropZoneSub += '</div>';
                    countsubTask++
                }
                $('.dvSubPrioDropZone').append(appendDropZoneSub);
                $('.dvSubPrio').append(appendDesignSub);
                $('.dvSubPrioNum').append(appendNum);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});

//txtValSubWorkFlow


$('#btnSaveSubTaskPrio').click(function () {

});