﻿$(document).ready(function () {
    loadrole_accesstype("Role");
});

$("#btnAccess").click(function () {
    $("#dvUser").hide();
    $("#dvAccess").show();
    $("#btnUser").removeClass("btn-primary").addClass("btn-default");
    $("#btnAccess").removeClass("btn-default").addClass("btn-primary");
});
$("#btnUser").click(function () {
    $("#dvUser").show();
    $("#dvAccess").hide();
    $("#btnAccess").removeClass("btn-primary").addClass("btn-default");
    $("#btnUser").removeClass("btn-default").addClass("btn-primary");
    fnDisplayUsers();
    fnEmplevel();
});
function idFormatter1(value) {
    return "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + value + "' />";
}
function fnDisplayUsers() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/userList',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            //$('#tblUser').bootstrapTable('destroy');

            //$('#tblUser').bootstrapTable({
            //    data: data.d.tblUserList,
            //    height: 400,
            //    width: 1000,
            //    pagination: true
            //});
               var obj = data.d.tblUserList;
                var records = data.d.tblUserList;
                var html = '';
                $('.dvUsers').find('.page-navigation').remove();
                $(".dvAppendTableUsers").empty()
                $.each(records, function (idx, val) {
                    html += '<tr><td>' + "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + val.id + "' />" +
                           '</td><td>' + val.empname + '</td><td>' + val.emplevel +
                           '</td><td>' + val.immsupp + '</td><td>' + val.role_id +
                           '</td></tr>';
                });
                $(".dvAppendTableUsers").append(html);

                $('#tblUserSettings').paginate({
                    limit: 8,
                    next: true,
                });
            $('#userSelect').empty();
            $('#userSelect').append("<option value='0' manager=''>Select All</option>");
            for (var i = 0; i < obj.length; i++) {
                var ta;
                ta1 = "<option value='" + obj[i].ntid + "' manager='" + obj[i].immsupp + "'>" + obj[i].empname + "</option>";
                $('#userSelect').append(ta1);


            }
            //$('.iCheck').iCheck({
            //	checkboxClass: 'icheckbox_flat-blue',
            //	radioClass: 'iradio_flat-blue'
            //});
            $("#tblUser").on("click", ".chkRow1", function () {
                
            });
            $('#chkRowAll').click(function () {

                if ($('#chkRowAll').is(':checked') == true) {
                    $('.chkRow1').prop('checked', true);
                }
                else {
                    $('.chkRow1').prop('checked', false);
                }
            });
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function fnEmplevel() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'UserSettings.aspx/empLevel',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records2 = d.data.asd2;
                var records3 = d.data.asd3;
                $('#slcEmplevel,#slcImmeSupp,#slcRole').empty();
                $('#slcEmplevel,#slcImmeSupp').append("<option value='0' >Select All</option>");
                //$('#slcRole').append("<option value='0' >Select</option>");
                $('#slcRole').append("<option value='0' >Select</option>");

                $.each(records, function (idx, val) {
                    var ta;
                    ta1 = "<option value='" + val.EmpLevel + "' manager='" + val.EmpLevel + "'>" + val.EmpLevel + "</option>";
                    $('#slcEmplevel').append(ta1);
                });
                $.each(records2, function (idx, val) {
                    var ta;
                    ta1 = "<option value='" + val.MngrName + "' manager='" + val.MngrName + "'>" + val.MngrName + "</option>";
                    $('#slcImmeSupp').append(ta1);


                });
                $.each(records3, function (idx, val) {
                    var ta;
                    ta1 = "<option value='" + val.id + "' manager='" + val.accessname + "'>" + val.accessname + "</option>";
                    $('#slcRole').append(ta1);


                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function loadrole_accesstype(type) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tbdy').empty();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/loadAccess',
        data: '{"type":"' + type + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            for (var i = 0; i < obj.length; i++) {
                var ta = " <tr>";
                ta += "<td style='width:40%; '></td>"
                ta += "<td style='width:70%'><a type='button' style='cursor: pointer;' id='role_" + obj[i].id + "' onclick='loadrole_accessfunc(" + obj[i].id + ")'>" + obj[i].name + "</a></td>";
                ta += "<td style='width:15%; '></td>"
                ta += "<td style='width:15%; display:none'><button type='button' class='btn btn-link btn-sm' onclick='editLine(" + obj[i].id + ")' style='width:32px;'data-toggle='modal' data-target='#modalEditAccess'><i class='fas fa-pencil-alt fa-lg'></i></button></td>"
                ta += "<td style='width:15%; display:none'><button type='button' class='btn btn-link btn-sm form-control' onclick='deleteLine(" + obj[i].id + ")' style='width:32px;'><i class='fas fa-trash-alt text-red fa-lg'></i></button></td>"
                $('#tbdy').append(ta);
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function loadrole_accessfunc(id) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tFunc').empty();
    $('#tison').empty();
    var type = $('#headType').text();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/loadFunction',
        data: '{"id" : "' + id + '","type" : "' + "Role" + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            $('#conId').val(id);
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].isOn == "True") {
                    var ta = " <tr>";
                    ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkA'></td>";
                    ta += "<td style='width:85%' >" + obj[i].name + "</td><tr>";
                    $('#tison').append(ta);
                }
                else {
                    var ta = " <tr>";
                    ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkR'></td>";
                    ta += "<td style='width:85%'>" + obj[i].name + "</td><tr>";
                    $('#tFunc').append(ta);
                }
            }
            $('#modalLoading').modal('hide');

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#appFunc').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var sList = "";
    $('.fncChkR').each(function () {
        var sThisVal = (this.checked ? $(this).attr('id') : "0");
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });
    console.log(sList);
    $('.fncChkA').each(function () {
        var sThisVal = $(this).attr('id');
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });
    console.log(sList);
    var id = $('#conId').val();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/updateFunction',
        data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            loadrole_accessfunc(id);
            $('#modalLoading').modal('hide');


        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$('#remFunc').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var sList = "";
    $('.fncChkA').each(function () {
        var sThisVal = (this.checked ? "0" : $(this).attr('id'));
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });

    console.log(sList);
    var id = $('#conId').val();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/updateFunction',
        data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadrole_accessfunc(id);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$('#btnNew_Type').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var type = 'Role';
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/addAccess',
        data: '{"name" : "' + $('#accessTypeName').val() + '","desc" : "' + $('#accessTypeDesc').val() + '","type" : "' + 'Role' + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#accessTypeName').val("");
            $('#accessTypeDesc').val("")

            loadrole_accesstype(type);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});
function deleteLine(id) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/deleteLine',
        data: '{"id" : "' + id + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadrole_accesstype(data.d);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#userSelect').change(function () {
    fnDisplayUsersSelect($('#userSelect').val());
});
function fnDisplayUsersSelect(ntid) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/userListSelect',
        data: '{"ntid" : "' + ntid + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            //$('#tblUser').bootstrapTable('destroy');
            fnEmplevel();
            //$('#tblUser').bootstrapTable({
            //    data: data.d.tblUserList,
            //    height: 400,
            //    width: 1000,
            //    pagination: true
            //});
            var obj = data.d.tblUserList;
            //$('#modalLoading').modal('hide');
            var records = data.d.tblUserList;
            var html = '';
            $('.dvUsers').find('.page-navigation').remove();
            $(".dvAppendTableUsers").empty()
            $.each(records, function (idx, val) {
                html += '<tr><td>' + "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + val.id + "' />" +
                       '</td><td>' + val.empname + '</td><td>' + val.emplevel +
                       '</td><td>' + val.immsupp + '</td><td>' + val.role_id +
                       '</td></tr>';
            });
            $(".dvAppendTableUsers").append(html);

            $('#tblUserSettings').paginate({
                limit: 8,
                next: true,
            });

            //$('.iCheck').iCheck({
            //	checkboxClass: 'icheckbox_flat-blue',
            //	radioClass: 'iradio_flat-blue'
            //});
            $("#tblUser").on("click", ".chkRow1", function () {

            });
            $('#chkRowAll').click(function () {

                if ($('#chkRowAll').is(':checked') == true) {
                    $('.chkRow1').prop('checked', true);
                }
                else {
                    $('.chkRow1').prop('checked', false);
                }
            });
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnSaveRole').click(function () {

    if ($('#slcRole').val() == "0") {
        //console.alert("Please select role!");
        //alertify.alert("Warning", "Please select role!");
        alertify.alert("Warning", "Please select role!", function () {

            }).set('closable', false);
    } else {

        var emp_id = "";
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        $('.chkRow1').each(function () {
            var sThisVal = (this.checked ? $(this).attr('data-id') : "0");
            emp_id += (emp_id == "" ? sThisVal : "," + sThisVal);
        });
        emp_id = emp_id.replace(/\,0/g, "");;
        $.ajax({
            type: 'POST',
            url: 'UserSettings.aspx/applyAction',
            data: '{"role_id" : "' + $('#slcRole').val() + '","emp_id" : "' + emp_id + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                fnDisplayUsers();
                fnEmplevel();
                $('#modalLoading').modal('hide');
            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }

});
$('#slcEmplevel').change(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/filterby',
        data: '{"empcat" : "' + $('#slcEmplevel').val() + '","immeSuper" : "' + $('#slcImmeSupp').val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj1 = [];
            obj1 = data.d.tblUserList;
            $('#tblUser').bootstrapTable('destroy');
            if (obj1 == null) {
                //$('#tblUser').bootstrapTable({
                //    height: 400,
                //    width: 1000,
                //    pagination: true
                //});

            }
            else {
                //$('#tblUser').bootstrapTable({
                //    data: obj1,
                //    height: 400,
                //    width: 1000,
                //    pagination: true
                //});
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        },
        failure: function (response) {
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        }
    });
});
$('#slcImmeSupp').change(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/filterby',
        data: '{"empcat" : "' + $('#slcEmplevel').val() + '","immeSuper" : "' + $('#slcImmeSupp').val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj1 = [];
            obj1 = data.d.tblUserList;
            $('#tblUser').bootstrapTable('destroy');
            if (obj1 == null) {
                $('#tblUser').bootstrapTable({
                    height: 400,
                    width: 1000,
                    pagination: true
                });
            }
            else {
                $('#tblUser').bootstrapTable({
                    data: obj1,
                    height: 400,
                    width: 1000,
                    pagination: true
                });
            }



            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        },
        failure: function (response) {
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        }
    });
});

