﻿$(document).ready(function () {
    fndropdownVal();
    fndisplayWorkflow();
    fndropdownValEdit();
});

var countManager = 1;
var countTeamLeader = 1;
var countSubTask = 1;


var countManagerEdit = 1;
var countTeamLeaderEdit = 1;
var countSubTaskEdit = 1;

$("#dvNewWorkflow").on("click", ".btnAddManager", function () {

    var appendDesign = '<div class="row topStyle dvManager' + countManager + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcMGR' + countManager + '" class="form-control input-sm tb1">';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemove' + countManager + '">';
    appendDesign += '<div class="col-lg-6 noPaddingLR" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManager"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button"  class="btn btn-link btnRemoveManager"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    $('#dvNewWorkflow').find(".btnAddManager,.btnRemoveManager").remove();
    $('#dvNewWorkflow').append(appendDesign);


    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records2 = d.data.asd2
                var slcVal;

                var countMGR = countManager - 1;
                $('#slcMGR' + countMGR + '').empty();
                $('#slcMGR' + countMGR + '').append("<option value='0'  selected='selected' disabled >Select</option>");


                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcMGR' + countMGR + '').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });



    countManager++;
});

$("#dvNewWorkflow").on("click", ".btnRemoveManager", function () {
    countManager--;
    $('.dvManager' + countManager).fadeOut('slow', function () {
        $('#dvNewWorkflow').find(".dvManager" + countManager).remove();
    });


    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThis" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManager"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThis">';
    appendDesign += '</div>';


    var appendDesign1 = '<div class="col-lg-6 noPaddingLR" >';
    appendDesign1 += '<button type="button" class="btn btn-link btnAddManager"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign1 += '</div>';
    appendDesign1 += '<div class="col-lg-6 noPaddingLR">';
    appendDesign1 += '<button type="button"  class="btn btn-link btnRemoveManager"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign1 += '</div>';


    if (countManager == 1) {
        $('#dvNewWorkflow').find(".dvRemoveThis").remove();
        $('.dvbtnAddRemove').append(appendDesign);

    }
    else {
        var addRemove = countManager - 1;
        $('.dvbtnAddRemove' + addRemove).append(appendDesign1);
        $('#dvNewWorkflow').find(".dvbtnAddRemove" + countManager).remove();
    }
});

$("#dvNewTeamLeader").on("click", ".btnAddTeamLeader", function () {

    var appendDesign = '<div class="row topStyle dvTeamLeader' + countTeamLeader + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcTL' + countTeamLeader + '" class="form-control input-sm tb1">';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemove' + countTeamLeader + '">';
    appendDesign += '<div class="col-lg-6 noPaddingLR" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeader"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button"  class="btn btn-link btnAddRemoveLeader"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    $('#dvNewTeamLeader').find(".btnAddTeamLeader,.btnAddRemoveLeader").remove();
    $('#dvNewTeamLeader').append(appendDesign);


    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records2 = d.data.asd2
                var slcVal;

                var countMGR = countTeamLeader - 1;
                $('#slcTL' + countMGR + '').empty();
                $('#slcTL' + countMGR + '').append("<option value='0'  selected='selected' disabled >Select</option>");


                $.each(records2, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcTL' + countMGR + '').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    countTeamLeader++;
});
$("#dvNewTeamLeader").on("click", ".btnAddRemoveLeader", function () {
    countTeamLeader--;
    $('.dvTeamLeader' + countTeamLeader).fadeOut('slow', function () {
        $('#dvNewTeamLeader').find(".dvTeamLeader" + countTeamLeader).remove();
    });

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeader"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader">';
    appendDesign += '</div>';

    var appendDesign2 = '<div class="col-lg-6 noPaddingLR" >';
    appendDesign2 += '<button type="button" class="btn btn-link btnAddTeamLeader"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign2 += '</div>';
    appendDesign2 += '<div class="col-lg-6 noPaddingLR">';
    appendDesign2 += '<button type="button"  class="btn btn-link btnAddRemoveLeader"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign2 += '</div>';

    if (countTeamLeader == 1) {
        $('#dvNewTeamLeader').find(".dvRemoveThisTeamLeader").remove();
        $('.dvbtnAddRemoveTeamLeader').append(appendDesign);

    }
    else {
        var addRemove = countTeamLeader - 1;
        $('.dvbtnAddRemove' + addRemove).append(appendDesign2);
        $('#dvNewTeamLeader').find(".dvbtnAddRemoveTeamLeader" + countTeamLeader).remove();
    }
});
$("#dvSubTask").on("click", ".btnNewSLA", function () {

    var appendDesign = '<div class="row topStyle dvAddSubTask' + countSubTask + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR"></div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcSubTask' + countSubTask + '" class="form-control input-sm tb1">';
    //appendDesign += '<option value="">Select</option>';
    //appendDesign += '<option value="For Eligibility Review">For Eligibility Review</option>';
    //appendDesign += '<option value="For Renewal Letter Sending">For Renewal Letter Sending</option>';
    //appendDesign += '<option value="90-day Renewal Letter Notification">90-day Renewal Letter Notification</option>';
    //appendDesign += '<option value="60-day Renewal Letter Notification">60-day Renewal Letter Notification</option>';
    //appendDesign += '<option value="45-day Renewal Letter Notification">45-day Renewal Letter Notification</option>';
    //appendDesign += '<option value="Awaiting Response/Negotiation in progress">Awaiting Response/Negotiation in progress</option>';
    //appendDesign += '<option value="For DocuSign Sending">For DocuSign Sending</option>';
    //appendDesign += '<option value="For Renewal Follow-up">For Renewal Follow-up</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 txtStyle text-right">';
    appendDesign += 'SLA';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR">';
    appendDesign += '<div class="row">';
    appendDesign += '<div class="col-lg-5 noPaddingLR">';
    appendDesign += '<select id="slcSLAnum' + countSubTask + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7 noPaddingR" style="padding-left: 2px">';
    appendDesign += '<select id="slcSLAdays' + countSubTask + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="0">Select</option>';
    appendDesign += '<option value="Hour/s">Hour/s</option>';
    appendDesign += '<option value="Day/s">Day/s</option>';
    appendDesign += '<option value="Week/s">Week/s</option>';
    appendDesign += '<option value="Month/s">Month/s</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-5 " style="padding-left: 17px">';
    appendDesign += '<div class="row">';
    appendDesign += '<div class="col-lg-3 noPaddingR" style="padding-left: 29px">';
    appendDesign += 'AHT Target <i>hh:mm:ss</i>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHThr' + countSubTask + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHTmm' + countSubTask + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '<option value="41">41</option>';
    appendDesign += '<option value="42">42</option>';
    appendDesign += '<option value="43">43</option>';
    appendDesign += '<option value="44">44</option>';
    appendDesign += '<option value="45">45</option>';
    appendDesign += '<option value="46">46</option>';
    appendDesign += '<option value="47">47</option>';
    appendDesign += '<option value="48">48</option>';
    appendDesign += '<option value="49">49</option>';
    appendDesign += '<option value="50">50</option>';
    appendDesign += '<option value="51">51</option>';
    appendDesign += '<option value="52">52</option>';
    appendDesign += '<option value="53">53</option>';
    appendDesign += '<option value="54">54</option>';
    appendDesign += '<option value="55">55</option>';
    appendDesign += '<option value="56">56</option>';
    appendDesign += '<option value="57">57</option>';
    appendDesign += '<option value="58">58</option>';
    appendDesign += '<option value="59">59</option>';
    appendDesign += '<option value="60">60</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHTss' + countSubTask + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '<option value="41">41</option>';
    appendDesign += '<option value="42">42</option>';
    appendDesign += '<option value="43">43</option>';
    appendDesign += '<option value="44">44</option>';
    appendDesign += '<option value="45">45</option>';
    appendDesign += '<option value="46">46</option>';
    appendDesign += '<option value="47">47</option>';
    appendDesign += '<option value="48">48</option>';
    appendDesign += '<option value="49">49</option>';
    appendDesign += '<option value="50">50</option>';
    appendDesign += '<option value="51">51</option>';
    appendDesign += '<option value="52">52</option>';
    appendDesign += '<option value="53">53</option>';
    appendDesign += '<option value="54">54</option>';
    appendDesign += '<option value="55">55</option>';
    appendDesign += '<option value="56">56</option>';
    appendDesign += '<option value="57">57</option>';
    appendDesign += '<option value="58">58</option>';
    appendDesign += '<option value="59">59</option>';
    appendDesign += '<option value="60">60</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-3 noPaddingLR dvSLAaddRemove' + countSubTask + '" style="padding-left: 3px">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLA"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button" class="btn btn-link btnRemoveSLA"><i class="fas fa-trash text-red  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';

    $('#dvSubTask').find(".btnNewSLA,.btnRemoveSLA").remove();
    $('#dvSubTask').append(appendDesign);


    //SubTaskOption
    //$.ajax({
    //    type: 'POST',
    //    contentType: 'application/json; charset=utf-8',
    //    url: 'WorkflowMaker.aspx/ddlValues',
    //    success: function (data) {
    //        var d = $.parseJSON(data.d);
    //        if (d.Success) {
    //            var records4 = d.data.asd4
    //            var slcValSub;
    //            var countSub = countSubTask - 1;
    //            $('#slcSubTask' + countSub + '').empty();
    //            $('#slcSubTask' + countSub + '').append("<option value='0'  selected='selected' disabled >Select</option>");

    //            $.each(records4, function (idx, val) {
    //                slcValSub = val.List_Option.split(',');

    //            });
    //            for (i = 0; i < slcValSub.length; i++) {
    //                ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
    //                $('#slcSubTask' + countSub + '').append(ta1);
    //            }

    //            $('#modalLoading').modal('hide');
    //        }
    //    }, error: function (response) {
    //        console.log(response.responseText);
    //    }, failure: function (response) {
    //        console.log(response.responseText);
    //    }
    //});
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValuesSubTask',
        data: '{"workflow" : "' + $('#slcNewWorkflow').val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records4 = d.data.asd4
                var slcValSub;
                var countSub = countSubTask - 1;
                $('#slcSubTask' + countSub + '').empty();
                $('#slcSubTask' + countSub + '').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records4, function (idx, val) {
                    ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
                    $('#slcSubTask' + countSub + '').append(ta1);

                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });


    countSubTask++;
});
$("#dvSubTask").on("click", ".btnRemoveSLA", function () {
    countSubTask--;
    $('.dvAddSubTask' + countSubTask).fadeOut('slow', function () {
        $('#dvSubTask').find(".dvAddSubTask" + countSubTask).remove();
    });

    var appendDesign = '<div class="col-lg-4 noPaddingLR dvRemoveThis">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLA"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThis">';
    appendDesign += '</div>';

    var appendDesign2 = '<div class="col-lg-4 noPaddingLR dvRemoveThis">';
    appendDesign2 += '<button type="button" class="btn btn-link btnNewSLA"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign2 += '</div>';
    appendDesign2 += '<div class="col-lg-6 noPaddingLR dvRemoveThis">';
    appendDesign2 += '<button type="button" class="btn btn-link btnRemoveSLA"><i class="fas fa-trash text-red  fa-lg"></i></button>';
    appendDesign2 += '</div>';

    if (countSubTask == 1) {
        $('#dvSubTask').find(".dvRemoveThis").remove();
        $('.dvSLAaddRemove').append(appendDesign);
    }
    else {
        var addRemove = countSubTask - 1;
        $('.dvSLAaddRemove' + addRemove).append(appendDesign2);
        $('#dvSubTask').find(".dvSLAaddRemove" + countSubTask).remove();
    }
});

//----------------------------------------------------------------------------------------------
$("#dvNewWorkflowEdit").on("click", ".btnAddManagerEdit", function () {

    var appendDesign = '<div class="row topStyle dvManagerEdit' + countManagerEdit + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcMGREdit' + countManagerEdit + '" class="form-control input-sm tb1">';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemoveEdit' + countManagerEdit + '">';
    appendDesign += '<div class="col-lg-6 noPaddingLR" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button"  class="btn btn-link btnRemoveManagerEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    $('#dvNewWorkflowEdit').find(".btnAddManagerEdit,.btnRemoveManagerEdit").remove();
    $('#dvNewWorkflowEdit').append(appendDesign);


    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records2 = d.data.asd2
                var slcVal;

                var countMGR = countManagerEdit - 1;
                $('#slcMGREdit' + countMGR + '').empty();
                $('#slcMGREdit' + countMGR + '').append("<option value='0'  selected='selected' disabled >Select</option>");


                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcMGREdit' + countMGR + '').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });



    countManagerEdit++;
});
$("#dvNewWorkflowEdit").on("click", ".btnRemoveManagerEdit", function () {
    countManagerEdit--;
    $('.dvManagerEdit' + countManagerEdit).fadeOut('slow', function () {
        $('#dvNewWorkflowEdit').find(".dvManagerEdit" + countManagerEdit).remove();
    });


    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '</div>';


    var appendDesign1 = '<div class="col-lg-6 noPaddingLR" >';
    appendDesign1 += '<button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign1 += '</div>';
    appendDesign1 += '<div class="col-lg-6 noPaddingLR">';
    appendDesign1 += '<button type="button"  class="btn btn-link btnRemoveManagerEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign1 += '</div>';


    if (countManagerEdit == 1) {
        $('#dvNewWorkflowEdit').find(".dvRemoveThisEdit").remove();
        $('.dvbtnAddRemoveEdit').append(appendDesign);
    }
    else {
        var addRemove = countManagerEdit - 1;
        $('#dvNewWorkflowEdit').find('.dvbtnAddRemoveEdit' + addRemove).append(appendDesign1);
        $('#dvNewWorkflowEdit').find(".dvbtnAddRemoveEdit" + countManagerEdit).remove();
    }


});

$("#dvNewTeamLeaderEdit").on("click", ".btnAddTeamLeaderEdit", function () {

    var appendDesign = '<div class="row topStyle dvTeamLeaderEdit' + countTeamLeaderEdit + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcTLEdit' + countTeamLeaderEdit + '" class="form-control input-sm tb1">';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemoveEdit' + countTeamLeaderEdit + '">';
    appendDesign += '<div class="col-lg-6 noPaddingLR" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button"  class="btn btn-link btnAddRemoveLeaderEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    $('#dvNewTeamLeaderEdit').find(".btnAddTeamLeaderEdit,.btnAddRemoveLeaderEdit").remove();
    $('#dvNewTeamLeaderEdit').append(appendDesign);


    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records2 = d.data.asd2
                var slcVal;

                var countMGR = countTeamLeaderEdit - 1;
                $('#slcTLEdit' + countMGR + '').empty();
                $('#slcTLEdit' + countMGR + '').append("<option value='0'  selected='selected' disabled >Select</option>");


                $.each(records2, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcTLEdit' + countMGR + '').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    countTeamLeaderEdit++;
});
$("#dvNewTeamLeaderEdit").on("click", ".btnAddRemoveLeaderEdit", function () {
    countTeamLeaderEdit--;
    $('.dvTeamLeaderEdit' + countTeamLeaderEdit).fadeOut('slow', function () {
        $('#dvNewTeamLeaderEdit').find(".dvTeamLeaderEdit" + countTeamLeaderEdit).remove();
    });

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit">';
    appendDesign += '</div>';

    var appendDesign2 = '<div class="col-lg-6 noPaddingLR" >';
    appendDesign2 += '<button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign2 += '</div>';
    appendDesign2 += '<div class="col-lg-6 noPaddingLR">';
    appendDesign2 += '<button type="button"  class="btn btn-link btnAddRemoveLeaderEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
    appendDesign2 += '</div>';

    if (countTeamLeaderEdit == 1) {
        $('#dvNewTeamLeaderEdit').find(".dvRemoveThisTeamLeaderEdit").remove();
        $('.dvbtnAddRemoveTeamLeaderEdit').append(appendDesign);

    }
    else {
        var addRemove = countTeamLeaderEdit - 1;
        $('#dvNewTeamLeaderEdit').find('.dvbtnAddRemoveEdit' + addRemove).append(appendDesign2);
        $('#dvNewTeamLeaderEdit').find(".dvbtnAddRemoveTeamLeaderEdit" + countTeamLeaderEdit).remove();
    }
});

$("#dvSubTaskEdit").on("click", ".btnNewSLAEdit", function () {

    var appendDesign = '<div class="row topStyle dvAddSubTaskEdit' + countSubTaskEdit + '">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<div class="col-lg-5 txtStyle noPaddingR"></div>';
    appendDesign += '<div class="col-lg-7">';
    appendDesign += '<select id="slcSubTaskEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-1 txtStyle text-right">';
    appendDesign += 'SLA';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR">';
    appendDesign += '<div class="row">';
    appendDesign += '<div class="col-lg-5 noPaddingLR">';
    appendDesign += '<select id="slcSLAnumEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-7 noPaddingR" style="padding-left: 2px">';
    appendDesign += '<select id="slcSLAdaysEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="0">Select</option>';
    appendDesign += '<option value="Hour/s">Hour/s</option>';
    appendDesign += '<option value="Day/s">Day/s</option>';
    appendDesign += '<option value="Week/s">Week/s</option>';
    appendDesign += '<option value="Month/s">Month/s</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-5 " style="padding-left: 17px">';
    appendDesign += '<div class="row">';
    appendDesign += '<div class="col-lg-3 noPaddingR" style="padding-left: 29px">';
    appendDesign += 'AHT Target <i>hh:mm:ss</i>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHThrEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHTmmEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '<option value="41">41</option>';
    appendDesign += '<option value="42">42</option>';
    appendDesign += '<option value="43">43</option>';
    appendDesign += '<option value="44">44</option>';
    appendDesign += '<option value="45">45</option>';
    appendDesign += '<option value="46">46</option>';
    appendDesign += '<option value="47">47</option>';
    appendDesign += '<option value="48">48</option>';
    appendDesign += '<option value="49">49</option>';
    appendDesign += '<option value="50">50</option>';
    appendDesign += '<option value="51">51</option>';
    appendDesign += '<option value="52">52</option>';
    appendDesign += '<option value="53">53</option>';
    appendDesign += '<option value="54">54</option>';
    appendDesign += '<option value="55">55</option>';
    appendDesign += '<option value="56">56</option>';
    appendDesign += '<option value="57">57</option>';
    appendDesign += '<option value="58">58</option>';
    appendDesign += '<option value="59">59</option>';
    appendDesign += '<option value="60">60</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
    appendDesign += '<select id="slcAHTssEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
    appendDesign += '<option value="00">00</option>';
    appendDesign += '<option value="01">01</option>';
    appendDesign += '<option value="02">02</option>';
    appendDesign += '<option value="03">03</option>';
    appendDesign += '<option value="04">04</option>';
    appendDesign += '<option value="05">05</option>';
    appendDesign += '<option value="06">06</option>';
    appendDesign += '<option value="07">07</option>';
    appendDesign += '<option value="08">08</option>';
    appendDesign += '<option value="09">09</option>';
    appendDesign += '<option value="10">10</option>';
    appendDesign += '<option value="11">11</option>';
    appendDesign += '<option value="12">12</option>';
    appendDesign += '<option value="13">13</option>';
    appendDesign += '<option value="14">14</option>';
    appendDesign += '<option value="15">15</option>';
    appendDesign += '<option value="16">16</option>';
    appendDesign += '<option value="17">17</option>';
    appendDesign += '<option value="18">18</option>';
    appendDesign += '<option value="19">19</option>';
    appendDesign += '<option value="20">20</option>';
    appendDesign += '<option value="21">21</option>';
    appendDesign += '<option value="22">22</option>';
    appendDesign += '<option value="23">23</option>';
    appendDesign += '<option value="24">24</option>';
    appendDesign += '<option value="25">25</option>';
    appendDesign += '<option value="26">26</option>';
    appendDesign += '<option value="26">27</option>';
    appendDesign += '<option value="28">28</option>';
    appendDesign += '<option value="29">29</option>';
    appendDesign += '<option value="30">30</option>';
    appendDesign += '<option value="31">31</option>';
    appendDesign += '<option value="32">32</option>';
    appendDesign += '<option value="33">33</option>';
    appendDesign += '<option value="34">34</option>';
    appendDesign += '<option value="35">35</option>';
    appendDesign += '<option value="36">36</option>';
    appendDesign += '<option value="37">37</option>';
    appendDesign += '<option value="38">38</option>';
    appendDesign += '<option value="39">39</option>';
    appendDesign += '<option value="40">40</option>';
    appendDesign += '<option value="41">41</option>';
    appendDesign += '<option value="42">42</option>';
    appendDesign += '<option value="43">43</option>';
    appendDesign += '<option value="44">44</option>';
    appendDesign += '<option value="45">45</option>';
    appendDesign += '<option value="46">46</option>';
    appendDesign += '<option value="47">47</option>';
    appendDesign += '<option value="48">48</option>';
    appendDesign += '<option value="49">49</option>';
    appendDesign += '<option value="50">50</option>';
    appendDesign += '<option value="51">51</option>';
    appendDesign += '<option value="52">52</option>';
    appendDesign += '<option value="53">53</option>';
    appendDesign += '<option value="54">54</option>';
    appendDesign += '<option value="55">55</option>';
    appendDesign += '<option value="56">56</option>';
    appendDesign += '<option value="57">57</option>';
    appendDesign += '<option value="58">58</option>';
    appendDesign += '<option value="59">59</option>';
    appendDesign += '<option value="60">60</option>';
    appendDesign += '</select>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-3 noPaddingLR dvSLAaddRemoveEdit' + countSubTaskEdit + '" style="padding-left: 3px">';
    appendDesign += '<div class="col-lg-4 noPaddingLR">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR">';
    appendDesign += '<button type="button" class="btn btn-link btnRemoveSLAEdit"><i class="fas fa-trash text-red  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';
    appendDesign += '</div>';

    $('#dvSubTaskEdit').find(".btnNewSLAEdit,.btnRemoveSLAEdit").remove();
    $('#dvSubTaskEdit').append(appendDesign);



    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records4 = d.data.asd4
                var slcValSub;
                var countSub = countSubTaskEdit - 1;
                $('#slcSubTaskEdit' + countSub + '').empty();
                $('#slcSubTaskEdit' + countSub + '').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records4, function (idx, val) {
                    slcValSub = val.List_Option.split(',');

                });
                for (i = 0; i < slcValSub.length; i++) {
                    ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
                    $('#slcSubTaskEdit' + countSub + '').append(ta1);
                }

                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

    countSubTaskEdit++;
});
$("#dvSubTaskEdit").on("click", ".btnRemoveSLAEdit", function () {
    countSubTaskEdit--;
    $('.dvAddSubTaskEdit' + countSubTaskEdit).fadeOut('slow', function () {
        $('#dvSubTaskEdit').find(".dvAddSubTaskEdit" + countSubTaskEdit).remove();
    });

    var appendDesign = '<div class="col-lg-4 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '</div>';

    var appendDesign2 = '<div class="col-lg-4 noPaddingLR dvRemoveThisEdit">';
    appendDesign2 += '<button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign2 += '</div>';
    appendDesign2 += '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit">';
    appendDesign2 += '<button type="button" class="btn btn-link btnRemoveSLAEdit"><i class="fas fa-trash text-red  fa-lg"></i></button>';
    appendDesign2 += '</div>';

    if (countSubTaskEdit == 1) {
        $('#dvSubTaskEdit').find(".dvRemoveThisEdit").remove();
        $('.dvSLAaddRemoveEdit').append(appendDesign);
    }
    else {
        var addRemove = countSubTaskEdit - 1;
        $('.dvSLAaddRemoveEdit' + addRemove).append(appendDesign2);
        $('#dvSubTaskEdit').find(".dvSLAaddRemoveEdit" + countSubTaskEdit).remove();
    }
});


function fndropdownVal() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records2 = d.data.asd2
                var records3 = d.data.asd3
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('#slcWorkflow,#slcNewWorkflow,#slcMGR,#slcTL,#slcCreatedBy,#slcSubTask').empty();
                $('#slcWorkflow,#slcNewWorkflow,#slcMGR,#slcTL,#slcCreatedBy,#slcSubTask').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records, function (idx, val) {
                    //slcVal = val.List_Option.split(',');
                    ta1 = "<option value='" + val.List_Option + "' >" + val.List_Option + "</option>";
                    $('#slcWorkflow,#slcNewWorkflow').append(ta1);
                });
                //for (i = 0; i < slcVal.length; i++) {
                //    ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                //    $('#slcWorkflow,#slcNewWorkflow').append(ta1);
                //}
                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcMGR').append(ta1);
                });
                $.each(records2, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcTL').append(ta1);
                });
                $.each(records3, function (idx, val) {
                    ta1 = "<option value='" + val.Created_By + "' >" + val.Created_By + "</option>";
                    $('#slcCreatedBy').append(ta1);
                });


                $.each(records4, function (idx, val) {
                    slcValSub = val.List_Option.split(',');

                });
                //for (i = 0; i < slcValSub.length; i++) {
                //    ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
                //    $('#slcSubTask').append(ta1);
                //}

                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnSaveWorkflow').click(function () {

    var MGR = '';
    var TL = '';


    var arrSubTask = [];
    var arrSLAnum = [];
    var arrSLAdays = [];
    var arrAHThr = [];
    var arrAHTmm = [];
    var arrAHTss = [];



    MGR += $('#slcMGR').val();
    TL += $('#slcTL').val();
    for (i = 1; i < countManager; i++) {
        MGR += ',' + $('#slcMGR' + i + '').val();
    }
    for (i = 1; i < countTeamLeader; i++) {
        TL += ',' + $('#slcTL' + i + '').val();
    }
    console.log($('#slcNewWorkflow').val() + ' ' + $('#slcQueue').val() + ' ' + MGR + ' ' + TL);

    console.log($('#slcSubTask').val() + ' ' + $('#slcSLAnum').val() + ' ' +
        $('#slcSLAdays').val() + ' ' + $('#slcAHThr').val() + ':' + $('#slcAHTmm').val() + ':' + $('#slcAHTss').val() + ' ');


    arrSubTask.push($('#slcSubTask').val());
    arrSLAnum.push($('#slcSLAnum').val());
    arrSLAdays.push($('#slcSLAdays').val());
    arrAHThr.push($('#slcAHThr').val());
    arrAHTmm.push($('#slcAHTmm').val());
    arrAHTss.push($('#slcAHTss').val());

    for (i = 1; i < countSubTask; i++) {

        arrSubTask.push($('#slcSubTask' + i + '').val());
        arrSLAnum.push($('#slcSLAnum' + i + '').val());
        arrSLAdays.push($('#slcSLAdays' + i + '').val());
        arrAHThr.push($('#slcAHThr' + i + '').val());
        arrAHTmm.push($('#slcAHTmm' + i + '').val());
        arrAHTss.push($('#slcAHTss' + i + '').val());

        console.log($('#slcSubTask' + i + '').val() + ' ' + $('#slcSLAnum' + i + '').val() + ' ' +
        $('#slcSLAdays' + i + '').val() + ' ' + $('#slcAHThr' + i + '').val() + ':' + $('#slcAHTmm' + i + '').val() + ':' + $('#slcAHTss' + i + '').val() + ' ');
    }
    $.ajax({
        type: 'POST',
        url: 'WorkflowMaker.aspx/SaveWorkflow',
        data: '{"slcNewWorkflow" : "' + $('#slcNewWorkflow').val() + '","slcQueue" : "' + $('#slcQueue').val() + '","MGR" : "' + MGR + '","TL" : "' + TL +
            '",slcSubTask : ' + JSON.stringify(arrSubTask) +
            ',slcSLAnum : ' + JSON.stringify(arrSLAnum) +
            ',slcSLAdays : ' + JSON.stringify(arrSLAdays) +
            ',slcAHThr : ' + JSON.stringify(arrAHThr) +
            ',slcAHTmm : ' + JSON.stringify(arrAHTmm) +
            ',slcAHTss : ' + JSON.stringify(arrAHTss) + '}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#slcNewWorkflow').val('0');
            $('#slcQueue').val('0');
            $('#slcMGR').val('0');
            $('#slcTL').val('0');
            $('#slcSubTask').val('0');
            $('#slcSLAnum').val('00');
            $('#slcSLAdays').val('0');
            $('#slcAHThr').val('00');
            $('#slcAHTmm').val('00');
            $('#slcAHTss').val('00');
            fnRemoveMGR();
            fnRemoveTL();
            fnSubTask();
            //asd
            fndisplayWorkflow();
            $('#myModal').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});
$('#btnSaveWorkflowEdit').click(function () {

    var MGR = '';
    var TL = '';


    var arrSubTask = [];
    var arrSLAnum = [];
    var arrSLAdays = [];
    var arrAHThr = [];
    var arrAHTmm = [];
    var arrAHTss = [];



    MGR += $('#slcMGREdit').val();
    TL += $('#slcTLEdit').val();
    for (i = 1; i < countManagerEdit; i++) {
        MGR += ',' + $('#slcMGREdit' + i + '').val();
    }
    for (i = 1; i < countTeamLeaderEdit; i++) {
        TL += ',' + $('#slcTLEdit' + i + '').val();
    }
    console.log($('#slcNewWorkflowEdit').val() + ' ' + $('#slcQueueEdit').val() + ' ' + MGR + ' ' + TL);

    console.log($('#slcSubTaskEdit').val() + ' ' + $('#slcSLAnumEdit').val() + ' ' +
        $('#slcSLAdaysEdit').val() + ' ' + $('#slcAHThrEdit').val() + ':' + $('#slcAHTmmEdit').val() + ':' + $('#slcAHTssEdit').val() + ' ');


    arrSubTask.push($('#slcSubTaskEdit').val());
    arrSLAnum.push($('#slcSLAnumEdit').val());
    arrSLAdays.push($('#slcSLAdaysEdit').val());
    arrAHThr.push($('#slcAHThrEdit').val());
    arrAHTmm.push($('#slcAHTmmEdit').val());
    arrAHTss.push($('#slcAHTssEdit').val());

    for (i = 1; i < countSubTaskEdit; i++) {

        arrSubTask.push($('#slcSubTaskEdit' + i + '').val());
        arrSLAnum.push($('#slcSLAnumEdit' + i + '').val());
        arrSLAdays.push($('#slcSLAdaysEdit' + i + '').val());
        arrAHThr.push($('#slcAHThrEdit' + i + '').val());
        arrAHTmm.push($('#slcAHTmmEdit' + i + '').val());
        arrAHTss.push($('#slcAHTssEdit' + i + '').val());

        console.log($('#slcSubTaskEdit' + i + '').val() + ' ' + $('#slcSLAnumEdit' + i + '').val() + ' ' +
        $('#slcSLAdaysEdit' + i + '').val() + ' ' + $('#slcAHThrEdit' + i + '').val() + ':' + $('#slcAHTmmEdit' + i + '').val() + ':' + $('#slcAHTssEdit' + i + '').val() + ' ');
    }
    $.ajax({
        type: 'POST',
        url: 'WorkflowMaker.aspx/SaveWorkflowEdit',
        data: '{"slcNewWorkflow" : "' + $('#slcNewWorkflowEdit').val() + '","slcQueue" : "' + $('#slcQueueEdit').val() + '","MGR" : "' + MGR + '","TL" : "' + TL +
            '",slcSubTask : ' + JSON.stringify(arrSubTask) +
            ',slcSLAnum : ' + JSON.stringify(arrSLAnum) +
            ',slcSLAdays : ' + JSON.stringify(arrSLAdays) +
            ',slcAHThr : ' + JSON.stringify(arrAHThr) +
            ',slcAHTmm : ' + JSON.stringify(arrAHTmm) +
            ',slcAHTss : ' + JSON.stringify(arrAHTss) +
            ',id : ' + $(this).val() + '}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#slcNewWorkflowEdit').val('0');
            $('#slcQueueEdit').val('0');
            $('#slcMGREdit').val('0');
            $('#slcTLEdit').val('0');
            $('#slcSubTaskEdit').val('0');
            $('#slcSLAnumEdit').val('00');
            $('#slcSLAdaysEdit').val('0');
            $('#slcAHThrEdit').val('00');
            $('#slcAHTmmEdit').val('00');
            $('#slcAHTssEdit').val('00');
            fnRemoveMGR();
            fnRemoveTL();
            fnSubTask();

            $('#myModalEdit').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});
function fnRemoveMGR() {
    for (i = 1; i <= countManager; i++) {
        $('#dvNewWorkflow').find(".dvManager" + i).remove();
    }

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThis" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManager"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThis">';
    appendDesign += '</div>';
    $('#dvNewWorkflow').find(".dvRemoveThis").remove();
    $('.dvbtnAddRemove').append(appendDesign);
    countManager = 1;
}
function fnRemoveTL() {

    for (i = 1; i <= countTeamLeader; i++) {
        $('#dvNewTeamLeader').find(".dvTeamLeader" + i).remove();
    }

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeader"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader">';
    appendDesign += '</div>';

    $('#dvNewTeamLeader').find(".dvRemoveThisTeamLeader").remove();
    $('.dvbtnAddRemoveTeamLeader').append(appendDesign);

    countTeamLeader = 1;
}
function fnSubTask() {

    for (i = 1; i <= countSubTask; i++) {
        $('#dvSubTask').find(".dvAddSubTask" + i).remove();
    }



    var appendDesign = '<div class="col-lg-4 noPaddingLR dvRemoveThis">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLA"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThis">';
    appendDesign += '</div>';

    $('#dvSubTask').find(".dvRemoveThis").remove();
    $('.dvSLAaddRemove').append(appendDesign);

    countSubTask = 1;
}
function fndisplayWorkflow() {

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'WorkflowMaker.aspx/displayWorkflow',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblWorkflow').bootstrapTable('destroy');
                $('#tblWorkflow').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function dateFormat(value, row, index) {
    return moment(value).format('MM/DD/YYYY');
}
$('#btnApply').click(function () {
    console.log($('#slcWorkflow').val() + ' ' + $('#slcCreatedBy').val() + ' ' + $('#dFrom').val() + ' ' + $('#dTo').val() + ' ');

    if ($('#slcWorkflow').val() == null) {
        alert("Please! select Workflow.");
    }
    else if ($('#slcCreatedBy').val() == null) {
        alert("Please! select Created By.");
    }
    else {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: "{'slcWorkflow' : '" + $('#slcWorkflow').val() +
                  "','slcCreatedBy' : '" + $('#slcCreatedBy').val() +
                  "','dFrom' : '" + $('#dFrom').val() +
                  "','dTo' : '" + $('#dTo').val() + "'}",
            url: 'WorkflowMaker.aspx/displayWorkflowSearch',
            success: function (data) {
                var d = $.parseJSON(data.d);
                if (d.Success) {
                    var records = d.data.asd;
                    $('#tblWorkflow').bootstrapTable('destroy');
                    $('#tblWorkflow').bootstrapTable({
                        data: records,
                        pagination: true,
                        width: 1000
                    });
                    $('#modalLoading').modal('hide');
                }
            }, error: function (response) {
                console.log(response.responseText);
            }, failure: function (response) {
                console.log(response.responseText);
            }
        });
    }



});
function edit(value, row, index) {
    return '<button type="button" value="' + value + '" class="btn btn-link btnEdit"><i class="far fa-edit fa-lg"></i></button><button value="' + value
        + '"  type="button" class="btn btn-link btnDelete"><i class="far fa-trash-alt text-red fa-lg"></i></button>';
}
function SubTaskMerge(value, row, index) {
    var SubTaskVal = value;
    var countAdded = 0;
    var MergeTask;
    if (SubTaskVal == null) {

    } else {
        MergeTask = SubTaskVal.split(' ');
        countAdded = MergeTask.length;
    }


    //for (i = 0; i < SubTaskMerge.length;i++)
    //{

    //}

    return '<button type="button" value="' + row.id + '" class="btn btn-link btnEdit">' + countAdded + ' added</button>';
}
$("#tblWorkflow").on("click", ".btnEdit", function () {
    fnRemoveMGREdit();
    fnRemoveTLEdit();
    fnSubTaskEdit();
    //GetEdit
  
    console.log($(this).val());
    $('#btnSaveWorkflowEdit').val($(this).val());
    var strWorkflow = "";
    var strSubtask = [];
    var strQueueMethod = "";
    var strManager = "";
    var arrManager;
    var strTeamleader = "";
    var strSLA = [];
    var strAHT_Target = [];

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{'id' : '" + $(this).val() + "'}",
        url: 'WorkflowMaker.aspx/EditWorkflow',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#myModalEdit').modal({ backdrop: 'static', keyboard: false });
                $.each(records, function (idx, val) {
                    strWorkflow = val.Workflow;
                    strSubtask.push(val.SubTask);
                    strQueueMethod = val.QueueMethod;
                    strManager = val.Manager.split(',');
                    strTeamleader = val.TeamLeader.split(',');
                    strSLA.push(val.SLA);
                    strAHT_Target.push(val.AHT_Target);
                });
                $('#slcNewWorkflowEdit').val(strWorkflow);
                $('#slcQueueEdit').val(strQueueMethod);
                $('#slcMGREdit').val(strManager[0]);

                AppendEditMNG(strManager, strManager.length);


                $('#slcTLEdit').val(strTeamleader[0]);

                AppendTeamLeaderEdit(strTeamleader, strTeamleader.length);

                
                fndropdownSubVal(strSubtask[0]);
                var SLAnum = strSLA[0].split(' ');



                var AHThr = strAHT_Target[0].split(':');

                AppendSubTaskEdit(strSubtask, strSubtask.length, strSLA, strAHT_Target);

                for (i = 0; i < strSLA.length; i++) {
                    SLAnum = strSLA[i].split(' ');
                    if (i == 0) {
                        $('#slcSLAnumEdit').val(SLAnum[0]);
                        $('#slcSLAdaysEdit').val(SLAnum[1]);
                    }
                    else {

                        $('#slcSLAnumEdit' + i + '').val(SLAnum[0]);
                        $('#slcSLAdaysEdit' + i + '').val(SLAnum[1]);
                    }

                }

                for (i = 0; i < strAHT_Target.length; i++) {
                    AHThr = strAHT_Target[i].split(':');
                    if (i == 0) {

                        $('#slcAHThrEdit').val(AHThr[0]);
                        $('#slcAHTmmEdit').val(AHThr[1]);
                        $('#slcAHTssEdit').val(AHThr[2]);
                    }
                    else {
                        AHThr = strAHT_Target[i].split(':');
                        $('#slcAHThrEdit' + i + '').val(AHThr[0]);
                        $('#slcAHTmmEdit' + i + '').val(AHThr[1]);
                        $('#slcAHTssEdit' + i + '').val(AHThr[2]);
                    }

                }



                //   $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});

function fndropdownSubVal(subVal)
{
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValuesSubTask',
        data: '{"workflow" : "' + $('#slcNewWorkflowEdit').val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('#slcSubTaskEdit').empty();
                $('#slcSubTaskEdit').append("<option value='0' selected='selected' disabled >Select</option>");

                $.each(records4, function (idx, val) {
                    ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
                    $('#slcSubTaskEdit').append(ta1);

                });
                $('#slcSubTaskEdit').val(subVal);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

}

function fndropdownValEdit() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records2 = d.data.asd2
                var records3 = d.data.asd3
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('#slcWorkflowEdit,#slcNewWorkflowEdit,#slcMGREdit,#slcTLEdit,#slcCreatedByEdit').empty();
                $('#slcWorkflowEdit,#slcNewWorkflowEdit,#slcMGREdit,#slcTLEdit,#slcCreatedByEdit').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records, function (idx, val) {
                    //slcVal = val.List_Option.split(',');
                    ta1 = "<option value='" + val.List_Option + "' >" + val.List_Option + "</option>";
                    $('#slcWorkflowEdit,#slcNewWorkflowEdit').append(ta1);
                });
                //for (i = 0; i < slcVal.length; i++) {
                //    ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                //    $('#slcWorkflowEdit,#slcNewWorkflowEdit').append(ta1);
                //}
                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcMGREdit').append(ta1);
                });
                $.each(records2, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcTLEdit').append(ta1);
                });
                $.each(records3, function (idx, val) {
                    ta1 = "<option value='" + val.Created_By + "' >" + val.Created_By + "</option>";
                    $('#slcCreatedByEdit').append(ta1);
                });


                //$.each(records4, function (idx, val) {
                //    slcValSub = val.List_Option.split(',');

                //});
                //for (i = 0; i < slcValSub.length; i++) {
                //    ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
                //    $('#slcSubTaskEdit').append(ta1);
                //}

                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });


  


}

function AppendEditMNG(valMNG, valCount) {
    for (i = 1; i < valCount; i++) {
        var appendDesign = '<div class="row topStyle dvManagerEdit' + countManagerEdit + '">';
        appendDesign += '<div class="col-lg-4 noPaddingLR">';
        appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-7">';
        appendDesign += '<select id="slcMGREdit' + countManagerEdit + '" class="form-control input-sm tb1 slcEditMGR">';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemoveEdit' + countManagerEdit + '">';
        appendDesign += '<div class="col-lg-6 noPaddingLR" >';
        appendDesign += '<button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-6 noPaddingLR">';
        appendDesign += '<button type="button"  class="btn btn-link btnRemoveManagerEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';

        $('#dvNewWorkflowEdit').find(".btnAddManagerEdit,.btnRemoveManagerEdit").remove();
        $('#dvNewWorkflowEdit').append(appendDesign);

        countManagerEdit++;
    }

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records2 = d.data.asd2
                var slcVal;

                // for (i = 1; i < countManagerEdit; i++)
                //{
                //   var countMGR = countManagerEdit - 1;
                $('.slcEditMGR').empty();
                $('.slcEditMGR').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('.slcEditMGR').append(ta1);
                });

                //   $('.slcEditMGR').val(valMNG);
                //}
                for (i = 1; i < countManagerEdit; i++) {
                    var countMGR = countManagerEdit - i;

                    $('#slcMGREdit' + countMGR + '').val(valMNG[countMGR]);
                }
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });




};

function AppendTeamLeaderEdit(valTL, valCount) {
    for (i = 1; i < valCount; i++) {
        var appendDesign = '<div class="row topStyle dvTeamLeaderEdit' + countTeamLeaderEdit + '">';
        appendDesign += '<div class="col-lg-4 noPaddingLR">';
        appendDesign += '<div class="col-lg-5 txtStyle noPaddingR">';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-7">';
        appendDesign += '<select id="slcTLEdit' + countTeamLeaderEdit + '" class="form-control input-sm tb1 slcTLEditVal">';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-1 noPaddingLR dvbtnAddRemoveEdit' + countTeamLeaderEdit + '">';
        appendDesign += '<div class="col-lg-6 noPaddingLR" >';
        appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-6 noPaddingLR">';
        appendDesign += '<button type="button"  class="btn btn-link btnAddRemoveLeaderEdit"><i class="fas fa-trash text-red fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        $('#dvNewTeamLeaderEdit').find(".btnAddTeamLeaderEdit,.btnAddRemoveLeaderEdit").remove();
        $('#dvNewTeamLeaderEdit').append(appendDesign);
        countTeamLeaderEdit++;
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records2 = d.data.asd2
                var slcVal;

                var countTL = countTeamLeaderEdit - 1;
                $('.slcTLEditVal').empty();
                $('.slcTLEditVal').append("<option value='0'  selected='selected' disabled >Select</option>");


                $.each(records2, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('.slcTLEditVal').append(ta1);
                });


                for (i = 1; i < countTeamLeaderEdit; i++) {
                    var countTL = countTeamLeaderEdit - i;
                    $('#slcTLEdit' + countTL + '').val(valTL[countTL]);
                }
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

};


function AppendSubTaskEdit(valSub, valCount, valSLAnum, valAHThr) {
    $('.dvRemoveAppSubTask').remove();
    countSubTaskEdit = 1;
    for (i = 1; i < valCount; i++) {
        var appendDesign = '<div class="row topStyle dvRemoveAppSubTask dvAddSubTaskEdit' + countSubTaskEdit + '">';
        appendDesign += '<div class="col-lg-4 noPaddingLR">';
        appendDesign += '<div class="col-lg-5 txtStyle noPaddingR"></div>';
        appendDesign += '<div class="col-lg-7">';
        appendDesign += '<select id="slcSubTaskEdit' + countSubTaskEdit + '" class="form-control input-sm tb1 slcSubTaskEditVal">';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-1 txtStyle text-right">';
        appendDesign += 'SLA';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-2 noPaddingR">';
        appendDesign += '<div class="row">';
        appendDesign += '<div class="col-lg-5 noPaddingLR">';
        appendDesign += '<select id="slcSLAnumEdit' + countSubTaskEdit + '" class="form-control input-sm tb1 slcSLAnumEditVal">';
        appendDesign += '<option value="00">00</option>';
        appendDesign += '<option value="01">01</option>';
        appendDesign += '<option value="02">02</option>';
        appendDesign += '<option value="03">03</option>';
        appendDesign += '<option value="04">04</option>';
        appendDesign += '<option value="05">05</option>';
        appendDesign += '<option value="06">06</option>';
        appendDesign += '<option value="07">07</option>';
        appendDesign += '<option value="08">08</option>';
        appendDesign += '<option value="09">09</option>';
        appendDesign += '<option value="10">10</option>';
        appendDesign += '<option value="11">11</option>';
        appendDesign += '<option value="12">12</option>';
        appendDesign += '<option value="13">13</option>';
        appendDesign += '<option value="14">14</option>';
        appendDesign += '<option value="15">15</option>';
        appendDesign += '<option value="16">16</option>';
        appendDesign += '<option value="17">17</option>';
        appendDesign += '<option value="18">18</option>';
        appendDesign += '<option value="19">19</option>';
        appendDesign += '<option value="20">20</option>';
        appendDesign += '<option value="21">21</option>';
        appendDesign += '<option value="22">22</option>';
        appendDesign += '<option value="23">23</option>';
        appendDesign += '<option value="24">24</option>';
        appendDesign += '<option value="25">25</option>';
        appendDesign += '<option value="26">26</option>';
        appendDesign += '<option value="26">27</option>';
        appendDesign += '<option value="28">28</option>';
        appendDesign += '<option value="29">29</option>';
        appendDesign += '<option value="30">30</option>';
        appendDesign += '<option value="31">31</option>';
        appendDesign += '<option value="32">32</option>';
        appendDesign += '<option value="33">33</option>';
        appendDesign += '<option value="34">34</option>';
        appendDesign += '<option value="35">35</option>';
        appendDesign += '<option value="36">36</option>';
        appendDesign += '<option value="37">37</option>';
        appendDesign += '<option value="38">38</option>';
        appendDesign += '<option value="39">39</option>';
        appendDesign += '<option value="40">40</option>';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-7 noPaddingR" style="padding-left: 2px">';
        appendDesign += '<select id="slcSLAdaysEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
        appendDesign += '<option value="0">Select</option>';
        appendDesign += '<option value="Hour/s">Hour/s</option>';
        appendDesign += '<option value="Day/s">Day/s</option>';
        appendDesign += '<option value="Week/s">Week/s</option>';
        appendDesign += '<option value="Month/s">Month/s</option>';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-5 " style="padding-left: 17px">';
        appendDesign += '<div class="row">';
        appendDesign += '<div class="col-lg-3 noPaddingR" style="padding-left: 29px">';
        appendDesign += 'AHT Target <i>hh:mm:ss</i>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
        appendDesign += '<select id="slcAHThrEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
        appendDesign += '<option value="00">00</option>';
        appendDesign += '<option value="01">01</option>';
        appendDesign += '<option value="02">02</option>';
        appendDesign += '<option value="03">03</option>';
        appendDesign += '<option value="04">04</option>';
        appendDesign += '<option value="05">05</option>';
        appendDesign += '<option value="06">06</option>';
        appendDesign += '<option value="07">07</option>';
        appendDesign += '<option value="08">08</option>';
        appendDesign += '<option value="09">09</option>';
        appendDesign += '<option value="10">10</option>';
        appendDesign += '<option value="11">11</option>';
        appendDesign += '<option value="12">12</option>';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
        appendDesign += '<select id="slcAHTmmEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
        appendDesign += '<option value="00">00</option>';
        appendDesign += '<option value="01">01</option>';
        appendDesign += '<option value="02">02</option>';
        appendDesign += '<option value="03">03</option>';
        appendDesign += '<option value="04">04</option>';
        appendDesign += '<option value="05">05</option>';
        appendDesign += '<option value="06">06</option>';
        appendDesign += '<option value="07">07</option>';
        appendDesign += '<option value="08">08</option>';
        appendDesign += '<option value="09">09</option>';
        appendDesign += '<option value="10">10</option>';
        appendDesign += '<option value="11">11</option>';
        appendDesign += '<option value="12">12</option>';
        appendDesign += '<option value="13">13</option>';
        appendDesign += '<option value="14">14</option>';
        appendDesign += '<option value="15">15</option>';
        appendDesign += '<option value="16">16</option>';
        appendDesign += '<option value="17">17</option>';
        appendDesign += '<option value="18">18</option>';
        appendDesign += '<option value="19">19</option>';
        appendDesign += '<option value="20">20</option>';
        appendDesign += '<option value="21">21</option>';
        appendDesign += '<option value="22">22</option>';
        appendDesign += '<option value="23">23</option>';
        appendDesign += '<option value="24">24</option>';
        appendDesign += '<option value="25">25</option>';
        appendDesign += '<option value="26">26</option>';
        appendDesign += '<option value="26">27</option>';
        appendDesign += '<option value="28">28</option>';
        appendDesign += '<option value="29">29</option>';
        appendDesign += '<option value="30">30</option>';
        appendDesign += '<option value="31">31</option>';
        appendDesign += '<option value="32">32</option>';
        appendDesign += '<option value="33">33</option>';
        appendDesign += '<option value="34">34</option>';
        appendDesign += '<option value="35">35</option>';
        appendDesign += '<option value="36">36</option>';
        appendDesign += '<option value="37">37</option>';
        appendDesign += '<option value="38">38</option>';
        appendDesign += '<option value="39">39</option>';
        appendDesign += '<option value="40">40</option>';
        appendDesign += '<option value="41">41</option>';
        appendDesign += '<option value="42">42</option>';
        appendDesign += '<option value="43">43</option>';
        appendDesign += '<option value="44">44</option>';
        appendDesign += '<option value="45">45</option>';
        appendDesign += '<option value="46">46</option>';
        appendDesign += '<option value="47">47</option>';
        appendDesign += '<option value="48">48</option>';
        appendDesign += '<option value="49">49</option>';
        appendDesign += '<option value="50">50</option>';
        appendDesign += '<option value="51">51</option>';
        appendDesign += '<option value="52">52</option>';
        appendDesign += '<option value="53">53</option>';
        appendDesign += '<option value="54">54</option>';
        appendDesign += '<option value="55">55</option>';
        appendDesign += '<option value="56">56</option>';
        appendDesign += '<option value="57">57</option>';
        appendDesign += '<option value="58">58</option>';
        appendDesign += '<option value="59">59</option>';
        appendDesign += '<option value="60">60</option>';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-2 noPaddingR" style="padding-left: 3px">';
        appendDesign += '<select id="slcAHTssEdit' + countSubTaskEdit + '" class="form-control input-sm tb1">';
        appendDesign += '<option value="00">00</option>';
        appendDesign += '<option value="01">01</option>';
        appendDesign += '<option value="02">02</option>';
        appendDesign += '<option value="03">03</option>';
        appendDesign += '<option value="04">04</option>';
        appendDesign += '<option value="05">05</option>';
        appendDesign += '<option value="06">06</option>';
        appendDesign += '<option value="07">07</option>';
        appendDesign += '<option value="08">08</option>';
        appendDesign += '<option value="09">09</option>';
        appendDesign += '<option value="10">10</option>';
        appendDesign += '<option value="11">11</option>';
        appendDesign += '<option value="12">12</option>';
        appendDesign += '<option value="13">13</option>';
        appendDesign += '<option value="14">14</option>';
        appendDesign += '<option value="15">15</option>';
        appendDesign += '<option value="16">16</option>';
        appendDesign += '<option value="17">17</option>';
        appendDesign += '<option value="18">18</option>';
        appendDesign += '<option value="19">19</option>';
        appendDesign += '<option value="20">20</option>';
        appendDesign += '<option value="21">21</option>';
        appendDesign += '<option value="22">22</option>';
        appendDesign += '<option value="23">23</option>';
        appendDesign += '<option value="24">24</option>';
        appendDesign += '<option value="25">25</option>';
        appendDesign += '<option value="26">26</option>';
        appendDesign += '<option value="26">27</option>';
        appendDesign += '<option value="28">28</option>';
        appendDesign += '<option value="29">29</option>';
        appendDesign += '<option value="30">30</option>';
        appendDesign += '<option value="31">31</option>';
        appendDesign += '<option value="32">32</option>';
        appendDesign += '<option value="33">33</option>';
        appendDesign += '<option value="34">34</option>';
        appendDesign += '<option value="35">35</option>';
        appendDesign += '<option value="36">36</option>';
        appendDesign += '<option value="37">37</option>';
        appendDesign += '<option value="38">38</option>';
        appendDesign += '<option value="39">39</option>';
        appendDesign += '<option value="40">40</option>';
        appendDesign += '<option value="41">41</option>';
        appendDesign += '<option value="42">42</option>';
        appendDesign += '<option value="43">43</option>';
        appendDesign += '<option value="44">44</option>';
        appendDesign += '<option value="45">45</option>';
        appendDesign += '<option value="46">46</option>';
        appendDesign += '<option value="47">47</option>';
        appendDesign += '<option value="48">48</option>';
        appendDesign += '<option value="49">49</option>';
        appendDesign += '<option value="50">50</option>';
        appendDesign += '<option value="51">51</option>';
        appendDesign += '<option value="52">52</option>';
        appendDesign += '<option value="53">53</option>';
        appendDesign += '<option value="54">54</option>';
        appendDesign += '<option value="55">55</option>';
        appendDesign += '<option value="56">56</option>';
        appendDesign += '<option value="57">57</option>';
        appendDesign += '<option value="58">58</option>';
        appendDesign += '<option value="59">59</option>';
        appendDesign += '<option value="60">60</option>';
        appendDesign += '</select>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-3 noPaddingLR dvSLAaddRemoveEdit' + countSubTaskEdit + '" style="padding-left: 3px">';
        appendDesign += '<div class="col-lg-4 noPaddingLR">';
        appendDesign += '<button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '<div class="col-lg-6 noPaddingLR">';
        appendDesign += '<button type="button" class="btn btn-link btnRemoveSLAEdit"><i class="fas fa-trash text-red  fa-lg"></i></button>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';
        appendDesign += '</div>';

        $('#dvSubTaskEdit').find(".btnNewSLAEdit,.btnRemoveSLAEdit").remove();
        $('#dvSubTaskEdit').append(appendDesign);

        countSubTaskEdit++;
    }
    
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValuesSubTask',
        data: '{"workflow" : "' + $('#slcNewWorkflowEdit').val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records4 = d.data.asd4
                var slcValSub;
                $('.slcSubTaskEditVal').empty();
                $('.slcSubTaskEditVal').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records4, function (idx, val) {
                    ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
                    $('.slcSubTaskEditVal').append(ta1);
                });
                //for (i = 0; i < slcValSub.length; i++) {
                //    ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
                //    $('.slcSubTaskEditVal').append(ta1);
                //}
                //valSLAnum, valAHThr
                //for (i = 1; i < countSubTask; i++) {
                //    var countSub = countSubTask - i;
                //    var SLANumSplit = valSLAnum[countSub];
                //    $('#slcTLEdit' + countSub + '').val(SLANumSplit[0]);
                //}
                for (i = 1; i < countSubTaskEdit; i++) {
                    var countSub = countSubTaskEdit - i;
                    var SLANumSplit = valSLAnum[countSub];
                    $('#slcSubTaskEdit' + countSub + '').val(valSub[countSub]);
                }
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    //$.ajax({
    //    type: 'POST',
    //    contentType: 'application/json; charset=utf-8',
    //    url: 'WorkflowMaker.aspx/ddlValuesSubTask',
    //    data: '{"workflow" : "' + $('#slcNewWorkflow').val() + '"}',
    //    success: function (data) {
    //        var d = $.parseJSON(data.d);
    //        if (d.Success) {
    //            var records4 = d.data.asd4
    //            var slcVal;
    //            var slcValSub;
    //            $('#slcSubTask').empty();
    //            $('#slcSubTask').append("<option value='0'  selected='selected' disabled >Select</option>");

    //            $.each(records4, function (idx, val) {
    //                ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
    //                $('#slcSubTask').append(ta1);
    //            });
    //            //for (i = 0; i < slcValSub.length; i++) {
    //            //    ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
    //            //    $('#slcSubTask').append(ta1);
    //            //}
    //            $('#modalLoading').modal('hide');
    //        }
    //    }, error: function (response) {
    //        console.log(response.responseText);
    //    }, failure: function (response) {
    //        console.log(response.responseText);
    //    }
    //});
};





function fnRemoveMGREdit() {
    for (i = 1; i <= countManagerEdit; i++) {
        $('#dvNewWorkflowEdit').find(".dvManagerEdit" + i).remove();
    }

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '</div>';
    $('#dvNewWorkflowEdit').find(".dvRemoveThisEdit").remove();
    $('.dvbtnAddRemoveEdit').append(appendDesign);
    countManagerEdit = 1;
}

function fnRemoveTLEdit() {

    for (i = 1; i <= countTeamLeaderEdit; i++) {
        $('#dvNewTeamLeaderEdit').find(".dvTeamLeaderEdit" + i).remove();
    }

    var appendDesign = '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit" >';
    appendDesign += '<button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit">';
    appendDesign += '</div>';

    $('#dvNewTeamLeaderEdit').find(".dvRemoveThisTeamLeaderEdit").remove();
    $('.dvbtnAddRemoveTeamLeaderEdit').append(appendDesign);

    countTeamLeaderEdit = 1;
}
function fnSubTaskEdit() {

    for (i = 1; i <= countSubTaskEdit; i++) {
        $('#dvSubTaskEdit').find(".dvAddSubTaskEdit" + i).remove();
    }



    var appendDesign = '<div class="col-lg-4 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '<button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>';
    appendDesign += '</div>';
    appendDesign += '<div class="col-lg-6 noPaddingLR dvRemoveThisEdit">';
    appendDesign += '</div>';

    $('#dvSubTaskEdit').find(".dvRemoveThisEdit").remove();
    $('.dvSLAaddRemoveEdit').append(appendDesign);

    countSubTaskEdit = 1;
} 
$('#slcNewWorkflow').click(function () {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/ddlValuesSubTask',
        data: '{"workflow" : "' + $('#slcNewWorkflow').val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('#slcSubTask').empty();
                $('#slcSubTask').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records4, function (idx, val) {
                    ta1 = "<option value='" + val.SubList_Option + "' >" + val.SubList_Option + "</option>";
                        $('#slcSubTask').append(ta1);
                });
                //for (i = 0; i < slcValSub.length; i++) {
                //    ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";
                //    $('#slcSubTask').append(ta1);
                //}
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});


$("#tblWorkflow").on("click", ".btnDelete", function () {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'WorkflowMaker.aspx/deleteWorkflow',
        data: '{"id" : "' + $(this).val() + '"}',
        success: function (data) {
            fndisplayWorkflow();
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});
