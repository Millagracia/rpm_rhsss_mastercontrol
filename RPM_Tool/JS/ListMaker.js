﻿$(document).ready(function () {
    //$("#input-44").fileinput({
    //    uploadUrl: '/file-upload-batch/2',
    //    maxFilePreviewSize: 10240
    //});
    ListTitle();
    $("#fileUpload").fileinput({});

    fnAppendWorkflow();
    fndropdownVal();
    displayRepository();
    fnGetHeaders();

});
var countManager = 1;
var listTitle_id = "";
var countHeaders = 0;

var countHeadersBuilding = 0;
var countHeadersLeases = 0;
var countHeadersRRI = 0;


var countHeadersGet = 0;

var countHeadersGetBuilding = 0;
var countHeadersGetLeases = 0;
var countHeadersGetRRI = 0;

$("#dvListOption").on("click", ".btnAdd", function () {
    var countNo = countManager + 1
    var appendDesign = '<div class="row dvappendAddRemove' + countManager + ' topStyle" >';
    appendDesign += '<div class="col-lg-3 "></div>';
    appendDesign += '<div class="col-lg-1 text-right" style="padding-top:3px">' + countNo + '</div>';
    appendDesign += '<div class="col-lg-3"><input id="txtListOption' + countManager + '" type="text" class="form-control" /></div>';
    appendDesign += '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
    appendDesign += '<div class="col-lg-1 dvListOption noPaddingL" ><button type="button"  class="btn btn-default btn-sm form-control btnDelete" style="background-color: transparent"><i class="fas fa-trash-alt text-red  fa-lg"></i></button></div>';
    appendDesign += '</div>';
    $('#dvListOption').find(".dvListOption").remove();
    $('#dvListOption').append(appendDesign);

    countManager++;
});

$("#dvListOption").on("click", ".btnDelete", function () {
    countManager--;
    $('.dvManager' + countManager).fadeOut('slow', function () {
        $('#dvNewWorkflow').find(".dvManager" + countManager).remove();
    });

    var appendDesign = '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: ';
    appendDesign += 'transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
    appendDesign += '<div class="col-lg-1 dvListOption noPaddingL" ></div>';

    var appendDesign1 = '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: ';
    appendDesign1 += 'transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
    appendDesign1 += '<div class="col-lg-1 dvListOption noPaddingL" ><button type="button"  class="btn btn-default btn-sm form-control btnDelete" style="background-';
    appendDesign1 += 'color: transparent"><i class="fas fa-trash-alt text-red  fa-lg"></i></button></div>';

    if (countManager == 1) {
        $('#dvListOption').find(".dvRemoveThis").remove
        $('.dvappendAddRemove' + countManager).remove();
        $('.dvappendAddRemove').append(appendDesign);
    }
    else {
        var addRemove = countManager - 1;
        $('.dvappendAddRemove' + addRemove).append(appendDesign1);
        $('#dvListOption').find(".dvappendAddRemove" + countManager).remove();
    }
});

$('#btnList').click(function () {
    $('#dvList').fadeIn().show();
    $('#dvFieldSelection').hide();
    $('#dvRepository').hide();
    $("#btnList").removeClass("btn-default").addClass("btn-primary");
    $("#btnWorkflow").removeClass("btn-primary").addClass("btn-default");
});
$('#btnWorkflow').click(function () {
    $('#dvCurrent').hide();
    $('#dvFuture').show();
    $("#btnList").removeClass("btn-primary").addClass("btn-default");
    $("#btnWorkflow").removeClass("btn-default").addClass("btn-primary");
});
$('#btnFiled').click(function () {
    $('#dvList').hide();
    $('#dvFieldSelection').fadeIn().show();
    $('#dvRepository').hide();
    fndropdownValClick();
});
$('#btnRepository').click(function () {
    $('#dvList').hide();
    $('#dvFieldSelection').hide();
    $('#dvRepository').fadeIn().show();
});
$('#btnAddNewList').click(function () {
    $('#dvAddNewList').fadeIn('slow').show();
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'ListMaker.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd 
                $('#slcDropdown').empty();
                $('#slcDropdown').append("<option value='0' >Select</option>");
                document.getElementById("slcDropdown").disabled = true;

                $.each(records, function (idx, val) {
                    var ta;
                    ta1 = "<option value='" + val.List_Title + "' >" + val.List_Title + "</option>";
                    document.getElementById("slcDropdown").disabled = false;
                    $('#slcDropdown').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnAddNewWorkflow').click(function () {
    $('#dvAddNewWorkflow').fadeIn('slow').show();
});

$('#btnSaveWorkflow').click(function () {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/SaveWorkflow',
        data: '{"workflow" : "' + $('#txtWorkflow').val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#dvAddNewWorkflow').fadeOut('slow').hide();
            $('#txtWorkflow').val('');
            $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
            setTimeout(function () {
                $('#SaveAlert').modal('hide');
                fnAppendWorkflow();
                ListTitle();
            }, 1000);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnCancelList').click(function () {
    $('#dvAddNewList').fadeOut('slow').hide();
});



$('#btnSaveList').click(function () {

    if ($('#slcDropdown').val() == '0') {

    }
    else {
        $.ajax({
            type: 'POST',
            url: 'ListMaker.aspx/SaveList',
            data: '{"ListTitle" : "' + $('#slcDropdown').val() + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('#dvAddNewList').fadeOut('slow').hide();
                $('#txtListTitle').val('');
                $('.dvListTitle').remove();
                $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
                setTimeout(function () {
                    $('#SaveAlert').modal('hide');
                    ListTitle();
                }, 2000);



            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }

});
function ListTitle() {
    var arrValList = [];

    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetListTitle',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var d = $.parseJSON(data.d);
            var records = d.data.asd
            if (d.Success) {
                if (records == 0) {
                    $('.dvListTitle').hide();
                }
                else {

                    $.each(records, function (idx, val) {
                        var appendList = '<div class="row dvListTitle dvListSetVal' + idx + '"  style="display: none">';
                        appendList += '<div class="col-lg-5 text-center"></div>';
                        appendList += '<div class="col-lg-3 text-left" style="padding-left: 0px; padding-top: 0px">';
                        appendList += '<button type="button"  class="btn btn-link btnListTitle btnValList' + idx + '"></button>';
                        appendList += '</div>';
                        appendList += '<div class="col-lg-2 noPaddingL text-right">';
                        appendList += '<button type="button" class="btn btn-default btn-sm btnEdit" style="background-color: transparent"><i class="fas fa-pencil-alt fa-lg"></i></button>';
                        appendList += '</div>';
                        appendList += '<div class="col-lg-2 noPaddingLR">';
                        appendList += '<button type="button" class="btn btn-default btn-sm btnDeleteList" style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-lg"></i></button>';
                        appendList += '</div>';
                        appendList += '</div>';
                        $('.dvListAppend').append(appendList);
                        $('.btnValList' + idx + '').html(val.List_Title);
                        $('.btnValList' + idx + '').val(val.id);
                        $('.dvListSetVal' + idx + '').find('.btnEdit').val(val.id);
                        $('.dvListSetVal' + idx + '').find('.btnDeleteList').val(val.id);

                        $('.dvListTitle').show();

                        arrValList.push(val.List_Title + ',' + val.id);
                    });
                }

            }
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

}


//$('.btnListTitle').click(function () {
$(".dvListAppend").on("click", ".btnListTitle", function () {
    // alert($(this).val());
    $('#dvListOption').find(".dvRemoveAll,.dvListOption").remove();
    $('.dvappendAddRemove').fadeIn().show();
    listTitle_id = $(this).val();
    $('#dvListOption').find(".dvRemoveAll").remove();

    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetListOption',
        data: '{"id" : "' + listTitle_id + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;

            var valSplit;
            var appendDesign = '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: ';
            appendDesign += 'transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
            appendDesign += '<div class="col-lg-1 dvListOption noPaddingL" ></div>';
            $.each(records, function (idx, val) {
                valSplit = val.List_Option
            });

            if (valSplit == null) {
                $('.dvappendAddRemove').append(appendDesign);
                $('#txtListOption').val('');

            }
            else {
                var arrVal = valSplit.split(',');
                var appendDesign = '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: ';
                appendDesign += 'transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
                appendDesign += '<div class="col-lg-1 dvListOption noPaddingL" ></div>';

                $('#txtListOption').val(arrVal[0])
                $('.dvappendAddRemove').append(appendDesign);


                for (i = 1; i < arrVal.length; i++) {
                    $('#dvListOption').find(".dvappendAddRemove" + i).remove();

                }


                countManager = 1;
                for (i = 1; i < arrVal.length; i++) {
                    var countNo = countManager + 1
                    var appendDesign = '<div class="row dvRemoveAll dvappendAddRemove' + countManager + ' topStyle" >';
                    appendDesign += '<div class="col-lg-3 "></div>';
                    appendDesign += '<div class="col-lg-1 text-right" style="padding-top:3px">' + countNo + '</div>';
                    appendDesign += '<div class="col-lg-3"><input value="' + arrVal[i] + '" id="txtListOption' + countManager + '" type="text" class="form-control" /></div>';
                    appendDesign += '<div class="col-lg-1 dvListOption"><button type="button"  class="btn btn-default btn-sm form-control btnAdd" style="background-color: transparent"><i class="fas fa-plus text-green  fa-lg"></i></button></div>';
                    appendDesign += '<div class="col-lg-1 dvListOption noPaddingL" ><button type="button"  class="btn btn-default btn-sm form-control btnDelete" style="background-color: transparent"><i class="fas fa-trash-alt text-red  fa-lg"></i></button></div>';
                    appendDesign += '</div>';
                    $('#dvListOption').find(".dvListOption").remove();
                    $('#dvListOption').append(appendDesign);

                    countManager++;
                }
            }



        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});
//$('.btnDeleteList').click(function () {
$(".dvListAppend").on("click", ".btnDeleteList", function () {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/DeleteList',
        data: '{"delete_id" : "' + $(this).val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('.dvListTitle').remove();
            ListTitle();
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$('#btnSaveListOption').click(function () {
    var valList = $('#txtListOption').val();
    for (i = 1; i < countManager; i++) {
        var txtField = 'txtListOption' + i;
        $('#' + txtField + '').val();
        valList += ',' + $('#' + txtField + '').val();
        if (i == countManager) {

        }
    }
    console.log(valList);

    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/SaveListOption',
        data: '{"ListOption" : "' + valList + '","id" : "' + listTitle_id + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$('#dvAddNewList').fadeOut('slow').hide();
            //$('#txtListTitle').val('');
            //ListTitle();
            $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
            setTimeout(function () {
                $('#SaveAlert').modal('hide');
            }, 2000);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnGetHeaders').click(function () {
    var filename = $('#fileUpload').val();
    var fileUpload = $("#fileUpload").get(0);
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filename, files[i]);
        filename = files[i].name;
    }
    $.ajax({
        url: "ExcelFileUpload.ashx",
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            GetHeaders(filename);
        },
        error: function (err) {
        }
    });
});

function GetHeaders(val) {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetHeaders',
        data: '{"filename" : "' + val + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            $('#lblFileName').html(data.d);
            DisplayHeaders();


        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function DisplayHeaders() {
    $('.dvRemoveHeaders').remove();
    countHeaders = 0;
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetDisplayHeaders',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var appendHeaders = "";
            appendHeaders += '<div class="row dvRemoveHeaders">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';
            $.each(records, function (idx, val) {

                appendHeaders += '<div class="row dvRemoveHeaders">';
                appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders' + idx + '" value="' + val.Headers + '" class="chkRowHeaders iCheck"  /></div>';
                appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                appendHeaders += '               </div>';

                countHeaders = idx;

            });
            $('.dvHeaders').append(appendHeaders);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


$(".dvHeaders").on("click", "#chkHeaders", function () {

    if ($(this).prop('checked') == true) {
        $('.chkRowHeaders').prop('checked', true);
    }
    else {
        $('.chkRowHeaders').prop('checked', false);
    }
});

$('#btnRight').click(function () {

    
    
    
    $('.dvRemoveHeadersGet').remove();
    //$('.dvGetHeadersBuildings').remove();
    //$('.dvGetHeadersLeases').remove();
    //$('.dvGetHeadersRRI').remove();
    var appendHeaders = "";
    for (i = 0; i < countHeadersBuilding; i++) {

        if ($('#chkHeadersBuildings' + i + '').prop('checked') == true) {
            appendHeaders += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslc' + i + '">';
            appendHeaders += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersBuildings' + i + '" value="' + $('#chkHeadersBuildings' + i + '').val() + '" class="chkRowHeadersSlc iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + $('#chkHeadersBuildings' + i + '').val() + '</div>';
            appendHeaders += '               </div>';
        }
        else {
        }
        countHeadersGetBuilding = i;
    }
    $('.dvGetHeadersBuildings').append(appendHeaders);
    appendHeaders = "";
    for (i = 0; i < countHeadersLeases; i++) {
        if ($('#chkHeadersLeases' + i + '').prop('checked') == true) {
            appendHeaders += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslcLeases' + i + '">';
            appendHeaders += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersLeases' + i + '" value="' + $('#chkHeadersLeases' + i + '').val() + '" class="chkRowHeadersSlc iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + $('#chkHeadersLeases' + i + '').val() + '</div>';
            appendHeaders += '               </div>';
        }
        else {
        }
        countHeadersGetLeases = i;

    }
    $('.dvGetHeadersLeases').append(appendHeaders);
    appendHeaders = "";
    for (i = 0; i < countHeadersRRI; i++) {
        if ($('#chkHeadersRRI' + i + '').prop('checked') == true) {
            appendHeaders += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslcRRI' + i + '">';
            appendHeaders += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersRRI' + i + '" value="' + $('#chkHeadersRRI' + i + '').val() + '" class="chkRowHeadersSlc iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + $('#chkHeadersRRI' + i + '').val() + '</div>';
            appendHeaders += '               </div>';
        }
        else {
        }
        countHeadersGetRRI = i;
    }
    $('.dvGetHeadersRRI').append(appendHeaders);
    appendHeaders = "";
});
$('#btnLeft').click(function () {
    for (i = 0; i < countHeadersBuilding; i++) {
        if ($('#chkGetHeadersBuildings' + i + '').prop('checked') == true) {
            $('.dvRemoveHeadersslc' + i + '').remove();
            $('#chkHeadersBuildings' + i + '').prop('checked', false);
        }
        else {
            
        }
    }
    for (i = 0; i < countHeadersLeases; i++) {
        if ($('#chkGetHeadersLeases' + i + '').prop('checked') == true) {
            $('.dvRemoveHeadersslcLeases' + i + '').remove();
            $('#chkHeadersLeases' + i + '').prop('checked', false);
        }
        else {

        }
    }
    for (i = 0; i < countHeadersRRI; i++) {
        if ($('#chkGetHeadersRRI' + i + '').prop('checked') == true) {
            $('.dvRemoveHeadersslcRRI' + i + '').remove();
            $('#chkHeadersRRI' + i + '').prop('checked', false);
        }
        else {

        }
    }
});
function fndropdownVal() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'ListMaker.aspx/ddlValues1',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var slcVal;
                var slcValSub;
                $('#slcWorkflow,#slcWorkflowHead').empty();
                $('#slcWorkflow,#slcWorkflowHead').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records, function (idx, val) {
                    //slcVal = val.List_Option.split(',');
                    ta1 = "<option value='" + val.List_Option + "' >" + val.List_Option + "</option>";
                    $('#slcWorkflow,#slcWorkflowHead').append(ta1);
                });
                //for (i = 0; i < slcVal.length; i++) {
                //    ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                //    $('#slcWorkflow,#slcWorkflowHead').append(ta1);
                //}

                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function fndropdownValClick() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'ListMaker.aspx/ddlValues1',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var slcVal;
                var slcValSub;
                $('#slcWorkflow').empty();
                $('#slcWorkflow').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records, function (idx, val) {
                    //slcVal = val.List_Option.split(',');
                    ta1 = "<option value='" + val.List_Option + "' >" + val.List_Option + "</option>";
                    $('#slcWorkflow').append(ta1);
                });
                //for (i = 0; i < slcVal.length; i++) {
                //    ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                //    $('#slcWorkflow,#slcWorkflowHead').append(ta1);
                //}

                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}


$('#btnSaveHeaders').click(function () {
    var myData = [];

    var myDataBuildings = [];
    var myDataLeases = [];
    var myDataRRI = [];

    var Workflow = $('#slcWorkflow').val();

    for (i = 0 ; i < countHeadersBuilding; i++) {
        try {
            if ($('#chkGetHeadersBuildings' + i + '').val() == undefined) {

            }
            else {
                myDataBuildings.push($('#chkGetHeadersBuildings' + i + '').val());
            }
        }
        catch (error) {
        }
    }

    for (i = 0 ; i < countHeadersLeases; i++) {
        try {
            if ($('#chkGetHeadersLeases' + i + '').val() == undefined) {

            }
            else {
                myDataLeases.push($('#chkGetHeadersLeases' + i + '').val());
            }
        }
        catch (error) {
        }
    }

    for (i = 0 ; i < countHeadersRRI; i++) {
        try {
            if ($('#chkGetHeadersRRI' + i + '').val() == undefined) {

            }
            else {
                myDataRRI.push($('#chkGetHeadersRRI' + i + '').val());
            }
        }
        catch (error) {
        }
    }
    $.ajax({
        type: 'POST',
        data: '{myDataBuildings : ' + JSON.stringify(myDataBuildings) +
            ',myDataLeases : ' + JSON.stringify(myDataLeases) +
            ',myDataRRI : ' + JSON.stringify(myDataRRI) +
            ',"Workflow" : "' + Workflow + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'ListMaker.aspx/InsertDataHeaders',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var exist = d.Message;

            if (exist == 'Not Save')
            {
                $('#MessageAlert').modal({ backdrop: 'static', keyboard: false });
                setTimeout(function () {
                    $('#MessageAlert').modal('hide');
                }, 2000);
            }
            else
            {
                $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
                setTimeout(function () {
                    $('#SaveAlert').modal('hide');
                    displayRepository();
                    $('.dvRemoveHeadersGet').remove();
                    $('.dvRemoveHeaders').remove();
                    $('#slcWorkflow').val('0');
                    $('#lblFileName').html('');
                    $('.dvSaveBtn').show();
                    $('.dvUPdateBtn').hide();
                    $('.chkRowHeaders ').prop('checked', false);
                    $('#dvList').hide();
                    $('#dvFieldSelection').hide();
                    $('#dvRepository').fadeIn().show();
                }, 2000);
            }
            

        },
        error: function (response) {
            console.log('Error');
        },
    });
});

function displayRepository() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'ListMaker.aspx/displayRepository',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblRepository').bootstrapTable('destroy');
                $('#tblRepository').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function dateFormat(value, row, index) {
    return moment(value).format('MM/DD/YYYY');
}
function Headers(value, row, index) {
    return '<button class="btn btn-link btnWorkflow " type="button" value="' + row.id + '"  >' + value + '</button>';
}
function edit(value, row, index) {
    return '<button type="button" value="' + value + '" class="btn btn-link btnEditHeaders"><i class="far fa-edit fa-lg"></i></button>';
}


$("#tblRepository").on("click", ".btnWorkflow", function () {
    console.log($(this).val());
    var id = $(this).val();

    $('.dvRemoveHeadersViewBuildings').remove();
    $('.dvRemoveHeadersViewLeases').remove();
    $('.dvRemoveHeadersViewRRI').remove();
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetDisplayHeadersView',
        data: '{"id" : "' + id + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var appendHeadersBuilding = "";
            var appendHeadersLeases = "";
            var appendHeadersRRI = "";
            $.each(records, function (idx, val) {
                if (val.Pick_Table == "Buildings") {
                    appendHeadersBuilding += '<div class="row dvRemoveHeadersViewBuildings">';
                    appendHeadersBuilding += '                   <div class="col-lg-1" style="padding-right:0px; padding-left:0px"></div>';
                    appendHeadersBuilding += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersBuilding += '               </div>';
                    countHeadersBuilding++;
                }

                if (val.Pick_Table == "Leases") {
                    appendHeadersLeases += '<div class="row dvRemoveHeadersViewLeases">';
                    appendHeadersLeases += '                   <div class="col-lg-1" style="padding-right:0px; padding-left:0px"></div>';
                    appendHeadersLeases += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersLeases += '               </div>';
                    countHeadersLeases++;
                }

                if (val.Pick_Table == "RRI") {
                    appendHeadersRRI += '<div class="row dvRemoveHeadersViewRRI">';
                    appendHeadersRRI += '                   <div class="col-lg-1" style="padding-right:0px; padding-left:0px"></div>';
                    appendHeadersRRI += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersRRI += '               </div>';
                    countHeadersRRI++;
                }




            });
            $('.dvDisplayViewHeadersBuilding').append(appendHeadersBuilding);
            $('.dvDisplayViewHeadersLeases').append(appendHeadersLeases);
            $('.dvDisplayViewHeadersRRI').append(appendHeadersRRI);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});

function fnGetHeaders() {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetHeaders',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //txtFileName
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var records1 = d.data.asd1;
            var records2 = d.data.asd2;
            var appendHeaders = "";
            //dvHeadersBuilding
            //dvHeadersLeases
            //dvHeadersRRI
            appendHeaders += '<div class="row dvRemoveHeadersBuildings">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';

            for (i = 0; i < records.length; i++) {
                //console.log(records[i]);
                appendHeaders += '<div class="row dvRemoveHeadersBuildings">';
                appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersBuildings' + i + '" value="' + records[i]
                                + '" class="chkRowHeaders iCheck"  /></div>';
                appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records[i] + '</div>';
                appendHeaders += '               </div>';
                countHeadersBuilding++
            }
            $('.dvHeadersBuilding').append(appendHeaders);
            appendHeaders = '<div class="row dvRemoveHeadersLeases">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';

            for (i = 0; i < records1.length; i++) {
                //console.log(records[i]);
                appendHeaders += '<div class="row dvRemoveHeadersLeases">';
                appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersLeases' + i + '" value="' + records1[i]
                                + '" class="chkRowHeaders iCheck"  /></div>';
                appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records1[i] + '</div>';
                appendHeaders += '               </div>';
                countHeadersLeases++
            }
            $('.dvHeadersLeases').append(appendHeaders);
            appendHeaders = '<div class="row dvRemoveHeadersRRI">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';

            for (i = 0; i < records2.length; i++) {
                //console.log(records[i]);
                appendHeaders += '<div class="row dvRemoveHeadersRRI">';
                appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersRRI' + i + '" value="' + records2[i]
                                + '" class="chkRowHeaders iCheck"  /></div>';
                appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records2[i] + '</div>';
                appendHeaders += '               </div>';
                countHeadersRRI++
            }
            $('.dvHeadersRRI').append(appendHeaders);
            appendHeaders = "";
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


$("#tblRepository").on("click", ".btnEditHeaders", function () {
    console.log($(this).val());
    var id = $(this).val();
    $('#btnUpdateHeaders').val($(this).val());
    $('.dvRemoveHeadersGet').remove();
    $('.dvRemoveHeadersGetBuilding').remove();
    $('.dvRemoveHeadersGetBuildingLeases').remove();
    $('.dvRemoveHeadersGetBuildingRRI').remove();
    countHeadersGetBuilding = 0
    countHeadersGetLeases = 0
    countHeadersGetRRI = 0
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetDisplayHeadersEdit',
        data: '{"id" : "' + id + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var records1 = d.data.asd1;
            var records2 = d.data.asd2;
            var appendHeadersBuilding = "";
            var appendHeadersLeases = "";
            var appendHeadersRRI = "";

            //dvHeadersBuilding
            //dvHeadersLeases
            //dvHeadersRRI

            //appendHeaders += '<div class="row dvRemoveHeaders">';
            //appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            //appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            //appendHeaders += '               </div>';
            //$.each(records, function (idx, val) {
            //    appendHeaders += '<div class="row dvRemoveHeaders">';
            //    appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders' + idx + '" value="' + val.Headers + '" class="chkRowHeaders iCheck"  /></div>';
            //    appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
            //    appendHeaders += '               </div>';
            //    countHeaders = idx;
            //});
            //$('.dvHeaders').append(appendHeaders);
            //appendHeaders = "";

            $.each(records1, function (idx, val) {

                if (val.Pick_Table == "Buildings") {

                    appendHeadersBuilding += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslcBuilding' + idx + '">';
                    appendHeadersBuilding += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersBuilding' + idx + '" value="' + val.Headers + '" class="chkRowHeadersSlcBuilding iCheck"  /></div>';
                    appendHeadersBuilding += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersBuilding += '               </div>';

                    for (i = 0; i < countHeadersBuilding; i++) {
                        if ($('#chkHeadersBuildings' + i + '').val() == val.Headers) {
                            $('#chkHeadersBuildings' + i + '').prop('checked', true);
                        }
                    }



                    countHeadersGetBuilding++
                }
                else if (val.Pick_Table == "Leases") {
                    appendHeadersLeases += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslcLeases' + idx + '">';
                    appendHeadersLeases += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersLeases' + idx + '" value="' + val.Headers + '" class="chkRowHeadersSlcLeases iCheck"  /></div>';
                    appendHeadersLeases += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersLeases += '               </div>';
                        
                    for (i = 0; i < countHeadersLeases; i++) {
                        if ($('#chkHeadersLeases' + i + '').val() == val.Headers) {
                            $('#chkHeadersLeases' + i + '').prop('checked', true);
                        }
                    }
                    countHeadersGetLeases++
                }
                else {
                    appendHeadersRRI += '<div class="row dvRemoveHeadersGet dvRemoveHeadersslcRRI' + idx + '">';
                    appendHeadersRRI += '                   <div class="col-lg-1"><input type="checkbox" id="chkGetHeadersRRI' + idx + '" value="' + val.Headers + '" class="chkRowHeadersSlcRRI iCheck"  /></div>';
                    appendHeadersRRI += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + val.Headers + '</div>';
                    appendHeadersRRI += '               </div>';
                    for (i = 0; i < countHeadersRRI; i++) {
                        if ($('#chkHeadersRRI' + i + '').val() == val.Headers) {
                            $('#chkHeadersRRI' + i + '').prop('checked', true);
                        }
                    }
                    countHeadersGetRRI++
                }
                countHeadersGet++
            });
            $('.dvGetHeadersBuildings').append(appendHeadersBuilding);
            $('.dvGetHeadersLeases').append(appendHeadersLeases);
            $('.dvGetHeadersRRI').append(appendHeadersRRI);
            $.each(records2, function (idx, val) {
                $('#slcWorkflow,#slcWorkflowHead').val(val.Workflow);
                //$('#lblFileName').html(val.FileName);
            });

            $('#dvList').hide();
            $('#dvFieldSelection').fadeIn().show();
            $('#dvRepository').hide();

            $('.dvSaveBtn').hide();
            $('.dvUPdateBtn').show();

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});


$('#btnCancelHeaders').click(function () {
    $('.dvRemoveHeadersGet').remove();
    $('.dvRemoveHeadersGetBuilding').remove();
    $('.dvRemoveHeadersGetBuildingLeases').remove();
    $('.dvRemoveHeadersGetBuildingRRI').remove();
    
    $('.chkRowHeaders').prop('checked',false);
    $('#slcWorkflowHead').val('0');
    $('#slcWorkflow').val('0');
    $('#lblFileName').html('');
    $('.dvSaveBtn').show();
    $('.dvUPdateBtn').hide();
});
$('#btnUpdateHeaders').click(function () {
    var myDataBuilding = [];
    var myDataLeases = [];
    var myDataRRI = [];
    var Workflow = $('#slcWorkflow').val();

    for (i = 0 ; i < countHeadersBuilding; i++) {
        try {
            if ($('#chkGetHeadersBuildings' + i + '').val() == undefined) {

            }
            else {
                myDataBuilding.push($('#chkGetHeadersBuildings' + i + '').val());
            }
        }
        catch (error) {
        }
    }

    for (i = 0 ; i < countHeadersLeases; i++) {
        try {
            if ($('#chkGetHeadersLeases' + i + '').val() == undefined) {

            }
            else {
                myDataLeases.push($('#chkGetHeadersLeases' + i + '').val());
            }
        }
        catch (error) {
        }
    }

    for (i = 0 ; i < countHeadersRRI; i++) {
        try {
            if ($('#chkGetHeadersRRI' + i + '').val() == undefined) {

            }
            else {
                myDataRRI.push($('#chkGetHeadersRRI' + i + '').val());
            }
        }
        catch (error) {
        }
    }

    //   $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        data: '{myDataBuilding : ' + JSON.stringify(myDataBuilding) +
            ',myDataLeases : ' + JSON.stringify(myDataLeases) +
            ',myDataRRI : ' + JSON.stringify(myDataRRI) +
            ',"Workflow" : "' + Workflow +
            '","id" : "' + $(this).val() + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'ListMaker.aspx/UpdateHeaders',
        success: function (data) {
            $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
            setTimeout(function () {
                $('#SaveAlert').modal('hide');
                displayRepository();

                $('.dvRemoveHeadersGet').remove();
                $('.dvRemoveHeaders').remove();
                $('#slcWorkflow').val('0');
                $('#lblFileName').html('');
                $('.dvSaveBtn').show();
                $('.dvUPdateBtn').hide();
                $('.chkRowHeaders ').prop('checked', false);
                $('#dvList').hide();
                $('#dvFieldSelection').hide();
                $('#dvRepository').fadeIn().show();
            }, 2000);




        },
        error: function (response) {
            console.log(response.responseText);
        },
    });
});


$('#slcWorkflowHead').click(function () {
    countHeaders = 0;
    if ($('#slcWorkflowHead').val() == 'Renewal') {
        $.ajax({
            type: 'POST',
            url: 'ListMaker.aspx/GetTableHeader',
            data: '{"workflow" : "' + $('#slcWorkflowHead').val() + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var d = $.parseJSON(data.d);
                var records = d.data.asd;
                var appendHeaders = "";
                var splitHeaders = records.split(',');

                var appendHeaders = "";
                appendHeaders += '<div class="row dvRemoveHeaders">';
                appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
                appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
                appendHeaders += '               </div>';
                for (i = 0; i < splitHeaders.length; i++) {
                    appendHeaders += '<div class="row dvRemoveHeaders">';
                    appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders' + i + '" value="' + splitHeaders[i] + '" class="chkRowHeaders iCheck"  /></div>';
                    appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + splitHeaders[i] + '</div>';
                    appendHeaders += '               </div>';

                    countHeaders = i;
                }

                $('.dvHeaders').append(appendHeaders);

            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }


});

function fnAppendWorkflow() {
    $('.dvRemoveWorkflowValue').remove();
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetWorkflowVal',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var appendWorkflow = "";
            var appendWorkflow = "";
            $.each(records, function (idx, val) {
                appendWorkflow += '<div class="row dvRemoveWorkflowValue" style="padding-top:5px">';
                appendWorkflow += '<div class="col-lg-3 text-center ">';
                appendWorkflow += '</div>';
                appendWorkflow += '<div class="col-lg-6 text-left ">';
                appendWorkflow += '<button type="button" value="' + val.List_Option + '"  class="btn btn-link btnListWorkflow ">' + val.List_Option + '</button>    ';
                appendWorkflow += '</div>';
                appendWorkflow += '<div class="col-lg-2">';
                appendWorkflow += '<button type="button" value="' + val.List_Option + '" class="btn btn-default btn-sm btnDeleteWorkflow" style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-sm"></i></button>';
                appendWorkflow += '</div>';
                appendWorkflow += '</div>';
            });
            $('.dvAppendWorkflowValue').append(appendWorkflow);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function fnAppendSubWorkflow(valWorkflow) {
    $('.dvRemoveSubWorkflowValue').remove();
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetSubWorkflowVal',
        data: '{"workflow" : "' + valWorkflow + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var appendWorkflow = "";
            var appendWorkflow = "";
            $.each(records, function (idx, val) {

                if (val.Sublist_Option == undefined) {
                    console.log('undefined');
                }
                else if (val.Sublist_Option == '') {
                    console.log('null');
                }
                else {
                    appendWorkflow += '<div class="row dvRemoveSubWorkflowValue" style="padding-top:5px">';
                    appendWorkflow += '<div class="col-lg-3 text-center ">';
                    appendWorkflow += '</div>';
                    appendWorkflow += '<div class="col-lg-4 text-left ">' + val.Sublist_Option + '';
                    appendWorkflow += '</div>';
                    appendWorkflow += '<div class="col-lg-2">';
                    appendWorkflow += '<button type="button" value="' + valWorkflow + ',' + val.Sublist_Option + '"  class="btn btn-default btn-sm btnDeleteSubWorkflow" style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-sm"></i></button>';
                    appendWorkflow += '</div>';
                    appendWorkflow += '</div>';
                }
            });

            //appendWorkflow += '<div class="row dvRemoveSubWorkflowValue">';
            //appendWorkflow += ' <div class="col-lg-12 text-center">';
            //appendWorkflow += '     <button type="button" id="btnAddNewSubWorkflow" class="btn btn-link btn-sm "><i class="fas fa-plus text-green"></i>Add New List</button>';
            //appendWorkflow += ' </div>';
            //appendWorkflow += '</div>';


            $('.dvAppendSubWorkflowValue').append(appendWorkflow);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
var subWorkflowAdd = "";
$(".dvAppendWorkflowValue").on("click", ".btnListWorkflow", function () {

    fnAppendSubWorkflow($(this).val());
    subWorkflowAdd = $(this).val();

});


$('#btnAddNewSubWorkflow').click(function () {
    $('#dvAddNewSubWorkflow').fadeIn('slow').show();
});
$('#btnCancelSubWorkflow').click(function () {
    $('#dvAddNewSubWorkflow').fadeOut('slow').hide();
    $('#txtSubWorkflow').val('');
});


$('#btnSaveSubWorkflow').click(function () {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/SaveSubWorkflow',
        data: '{"workflow" : "' + subWorkflowAdd + '","subWorkflowAdd" : "' + $('#txtSubWorkflow').val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#dvAddNewSubWorkflow').fadeOut('slow').hide();
            $('#txtSubWorkflow').val('');
            fnAppendSubWorkflow(subWorkflowAdd);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$(".dvAppendSubWorkflowValue").on("click", ".btnDeleteSubWorkflow", function () {
    console.log($(this).val());
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/DeleteSubWorkflow',
        data: '{"deleteVal" : "' + $(this).val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            fnAppendSubWorkflow(subWorkflowAdd);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});


$(".dvAppendWorkflowValue").on("click", ".btnDeleteWorkflow", function () {
    console.log($(this).val());
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/DeleteWorkflow',
        data: '{"deleteVal" : "' + $(this).val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('.dvRemoveSubWorkflowValue').remove();
            fnAppendWorkflow();
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});


//$(function () {
//    $("[class$=autocom]").autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                url: 'ListMaker.aspx/GetSearchBuilding',
//                data: "{ 'prefix': '" + request.term + "'}",
//                dataType: "json",
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                success: function (data) {
//                    response($.map(data.d, function (item) {
//                        return {
//                            label: item.split('-')[0],
//                            val: item.split('-')[1]
//                        }
//                    }))
//                },
//                error: function (response) {
//                    alert(response.responseText);
//                },
//                failure: function (response) {
//                    alert(response.responseText);
//                }
//            });
//        },
//        select: function (e, i) {
//            $("[id$=hfCustomerId]").val(i.item.val);
//        },
//        appendTo: $("#txtTo").next(),
//        minLength: 1,
//    });
//});


$("#txtSearchBuildings").keydown(function () {
    fnSearchBuilding($('#txtSearchBuildings').val());
});
$("#txtSearchLeases").keydown(function () {
    fnSearchLeases($('#txtSearchLeases').val());
});
$("#txtSearchRRI").keydown(function () {
    fnSearchRRI($('#txtSearchRRI').val());
});
function fnSearchBuilding(valSearch)
{
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetHeaders',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //txtFileName
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var records1 = d.data.asd1;
            var records2 = d.data.asd2;
            var appendHeaders = "";
            var strSearch = "";

            //dvHeadersBuilding
            //dvHeadersLeases
            //dvHeadersRRI
            $('.dvRemoveHeadersBuildings').remove();
            appendHeaders += '<div class="row dvRemoveHeadersBuildings">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';

            for (i = 0; i < records.length; i++) {
                //console.log(records[i]);
                strSearch = records[i];
                if (strSearch.toLowerCase().includes(valSearch.toLowerCase()) == true)
                {
                    appendHeaders += '<div class="row dvRemoveHeadersBuildings">';
                    appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersBuildings' + i + '" value="' + records[i]
                                    + '" class="chkRowHeaders iCheck"  /></div>';
                    appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records[i] + '</div>';
                    appendHeaders += '               </div>';
                }
                countHeadersBuilding++
            }
            $('.dvHeadersBuilding').append(appendHeaders);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function fnSearchLeases(valSearch) {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetHeaders',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //txtFileName
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var records1 = d.data.asd1;
            var records2 = d.data.asd2;
            var appendHeaders = "";
            var strSearch = "";
            $('.dvRemoveHeadersLeases').remove();
            appendHeaders = '<div class="row dvRemoveHeadersLeases">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';
            for (i = 0; i < records1.length; i++) {
                //console.log(records[i]);
                strSearch = records1[i];
                if (strSearch.toLowerCase().includes(valSearch.toLowerCase()) == true) {
                    appendHeaders += '<div class="row dvRemoveHeadersLeases">';
                    appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersLeases' + i + '" value="' + records1[i]
                                    + '" class="chkRowHeaders iCheck"  /></div>';
                    appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records1[i] + '</div>';
                    appendHeaders += '               </div>';
                }
                countHeadersLeases++
            }
            $('.dvHeadersLeases').append(appendHeaders);
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function fnSearchRRI(valSearch) {
    $.ajax({
        type: 'POST',
        url: 'ListMaker.aspx/GetHeaders',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //txtFileName
            var d = $.parseJSON(data.d);
            var records = d.data.asd;
            var records1 = d.data.asd1;
            var records2 = d.data.asd2;
            var appendHeaders = "";
            var strSearch = "";
            //dvHeadersBuilding
            //dvHeadersLeases
            //dvHeadersRRI
            $('.dvRemoveHeadersRRI').remove();
            appendHeaders = '<div class="row dvRemoveHeadersRRI">';
            appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeaders" class="chkRow1 iCheck"  /></div>';
            appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">Select All</div>';
            appendHeaders += '               </div>';
            for (i = 0; i < records2.length; i++) {
                //console.log(records[i]);
                strSearch = records1[i];
                if (strSearch.toLowerCase().includes(valSearch.toLowerCase()) == true) {
                    appendHeaders += '<div class="row dvRemoveHeadersRRI">';
                    appendHeaders += '                   <div class="col-lg-2"><input type="checkbox" id="chkHeadersRRI' + i + '" value="' + records2[i]
                                    + '" class="chkRowHeaders iCheck"  /></div>';
                    appendHeaders += '                   <div class="col-lg-8" style="padding-right:0px; padding-left:0px">' + records2[i] + '</div>';
                    appendHeaders += '               </div>';
                }
                countHeadersRRI++
            }
            $('.dvHeadersRRI').append(appendHeaders);
            appendHeaders = "";
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
