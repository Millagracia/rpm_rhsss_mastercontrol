﻿var countWorkflow = 0;
var countsubTask = 0;
var AssignVal = '';
$(document).ready(function () {
    fnWorkflowVal();
    fnEmployeeList();
});
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}


$('#btnAssign').click(function () {
    $('.dvAssign').show();
    $('.dvReassign').hide();
    $("#btnAssign").removeClass("btn-default").addClass("btn-primary");
    $("#btnReassign").removeClass("btn-primary").addClass("btn-default");
    
});

$('#btnReassign').click(function () {
    $('.dvAssign').show();
    $('.dvReassign').hide();
    $("#btnReassign").removeClass("btn-default").addClass("btn-primary");
    $("#btnAssign").removeClass("btn-primary").addClass("btn-default");
});









function fnWorkflowVal() {
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';

    var appendDesignSub = '';
    var appendDropZoneSub = '';

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'AgentTaskManager.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records1 = d.data.asd1
                var records4 = d.data.asd4
                var records5 = d.data.asd5
                var ListOption = "";
                var slcVal;
                var slcValSub;
                $.each(records, function (idx, val) {
                    ListOption += ListOption == "" ? val.List_Option : "," + val.List_Option;
                   
                    countWorkflow = 0;
                });
                slcVal = ListOption.split(',');
                for (i = 0; i < slcVal.length; i++) {
                    //ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                    appendDesign += '<div class="row topStyle dvRefresh">';
                    appendDesign += ' <div class="col-lg-12">';
                    appendDesign += '   <div class="panelBox pointerCursor"  ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendDesign += '       <div id="drag' + i + '" draggable="true" ondragstart="drag(event)" >';
                    appendDesign += '           <div class="col-lg-9" style="padding-left: 0px">';
                    appendDesign += '               <label class="pointerCursor" style="font-size:12px"  id="lblWorkflow' + i + '" value="' + slcVal[i] + '">' + slcVal[i] + '</label>';
                    appendDesign += '           </div>';
                    appendDesign += '           <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    appendDesign += '               <button type="button" class="btn btn-link btn-sm btnRenewal" value="' + slcVal[i] + '" style="padding-top: 0px"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    appendDesign += '           </div>';
                    appendDesign += '       </div>';
                    appendDesign += '   </div>';
                    appendDesign += '</div>';
                    appendDesign += '</div>';


                    appendNum += '<div class="row topStyle dvRefresh">';
                    appendNum += '<div class="col-lg-12">';
                    appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendNum += i + 1;
                    appendNum += '</div></div></div>';


                    appendDropZone += '<div class="row topStyle dvValWorkflowPrio' + i + ' dvRefresh" >';
                    appendDropZone += '<div class="col-lg-12">';
                    appendDropZone += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendDropZone += '</div>';
                    appendDropZone += ' </div>';


                    countWorkflow++;
                }
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);
                $('.dvPrioNum').append(appendNum);




                $('#slcEmpName,#slcMGR').empty();
                $('#slcEmpName,#slcMGR').append("<option value='0'  selected='selected' >Select</option>");
                $.each(records5, function (idx, val) {
                    ta1 = "<option value='" + val.NTID + "' >" + val.EmpName + "</option>";
                    $('#slcEmpName').append(ta1);
                });
                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.EmpName + "' >" + val.EmpName + "</option>";
                    $('#slcMGR').append(ta1);
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}


$(".dvPrio").on("click", ".btnRenewal", function () {
    //$('#myModal').modal({ backdrop: 'static', keyboard: false });
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';

    console.log($(this).val());

    AssignVal = '';
    $('#lblSubTask').text($('.dvPrio').find(this).val());
    var appendDesignSub = '';
    var appendDropZoneSub = '';
    $('.dvSubPrio').find('.dvRemoveSub').remove();
    $('.dvSubPrioDropZone').find('.dvRemoveSubDropZone').remove();
    $('.dvSubPrioNum').find('.removeNum').remove();
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'AgentTaskManager.aspx/ddlValuesSubWorkflow',
        data: '{"workflow" : "' + $(this).val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records4 = d.data.asd4
                var slcVal;
                var slcValSub;
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);
                countsubTask = 0;
                $.each(records4, function (idx, val) {
                    slcValSub = val.List_Option.split(',');

                });
                for (i = 0; i < slcValSub.length; i++) {
                    //ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";

                    appendDesignSub += '<div class="row topStyle dvRemoveSub">';
                    appendDesignSub += '<div class="col-lg-12">';
                    appendDesignSub += ' <div class="panelBox pointerCursor" ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                    appendDesignSub += '     <div id="dragSub' + i + '" draggable="true" ondragstart="drag(event)">';
                    appendDesignSub += '         <div class="col-lg-12" style="padding-left: 0px">';
                    appendDesignSub += '             <label class="pointerCursor"  style="font-size:12px" id="lblSubTask' + i + '">' + slcValSub[i] + '</label>';
                    appendDesignSub += '         </div>';
                    //appendDesignSub += '         <div class="col-lg-3 text-right" style="padding-right: 0px">';
                    ////appendDesignSub += '             <button type="button" class="btn btn-link btn-sm" style="padding-top: 0px" data-toggle="modal" data-target="#myModal"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                    //appendDesignSub += '         </div>';
                    appendDesignSub += '     </div>';
                    appendDesignSub += ' </div>';
                    appendDesignSub += '</div>';
                    appendDesignSub += '</div>';


                    appendNum += '<div class="row topStyle removeNum">';
                    appendNum += '<div class="col-lg-12">';
                    appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                    appendNum += i + 1;
                    appendNum += '</div></div></div>';

                    appendDropZoneSub += '<div class="row topStyle dvRemoveSubDropZone dvValSubTaskPrio' + i + '">';
                    appendDropZoneSub += '<div class="col-lg-12">';
                    appendDropZoneSub += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                    appendDropZoneSub += '</div>';
                    appendDropZoneSub += '</div>';
                    countsubTask++
                }
                $('.dvSubPrioDropZone').append(appendDropZoneSub);
                $('.dvSubPrio').append(appendDesignSub);
                $('.dvSubPrioNum').append(appendNum);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$(".dvPrioDropZone").on("click", ".btnRenewal", function () {
    $('#myModal').modal({ backdrop: 'static', keyboard: false });
    var appendDesign = '';
    var appendDropZone = '';
    var appendNum = '';

    var appendDesignSub = '';
    var appendDropZoneSub = '';
    console.log($('.dvPrioDropZone').find(this).val());
    AssignVal = $('.dvPrioDropZone').find(this).val();
    $('#lblSubTask').text($('.dvPrioDropZone').find(this).val());
    $('.dvSubPrioNum').find('.removeNum').remove();
    $('.dvSubPrio').find('.dvRemoveSub').remove();
    $('.dvSubPrioDropZone').find('.dvRemoveSubDropZone').remove();

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'AgentTaskManager.aspx/ddlValuesSubWorkflow',
        data: '{"workflow" : "' + $(this).val() + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                var records4 = d.data.asd4
                var slcVal;
                var splitVal = "";
                var slcValSub;
                $('.dvPrioDropZone').append(appendDropZone);
                $('.dvPrio').append(appendDesign);

                $.each(records4, function (idx, val) {
                    splitVal += splitVal == "" ? val.Sublist_Option : "," + val.Sublist_Option;
                });
                slcValSub = splitVal.split(',');

                if (slcValSub.length == 0)
                {

                }
                else
                {
                    for (i = 0; i < slcValSub.length; i++) {
                        //ta1 = "<option value='" + slcValSub[i] + "' >" + slcValSub[i] + "</option>";

                        appendDesignSub += '<div class="row topStyle dvRemoveSub">';
                        appendDesignSub += '<div class="col-lg-12">';
                        appendDesignSub += ' <div class="panelBox pointerCursor" ondrop="drop(event)" class="drag" ondragover="allowDrop(event)">';
                        appendDesignSub += '     <div  id="dragSub' + i + '" draggable="true" ondragstart="drag(event)">';
                        appendDesignSub += '         <div class="col-lg-9 " style="padding-left: 0px">';
                        appendDesignSub += '             <label class="pointerCursor" style="font-size:12px" id="lblSubTask' + i + '">' + slcValSub[i] + '</label>';
                        appendDesignSub += '         </div>';
                        appendDesignSub += '         <div class="col-lg-3 text-right" style="padding-right: 0px">';
                        //appendDesignSub += '             <button type="button" class="btn btn-link btn-sm" style="padding-top: 0px" data-toggle="modal" data-target="#myModal"><i class="fas fa-expand-arrows-alt fa-lg text-black"></i></button>';
                        appendDesignSub += '         </div>';
                        appendDesignSub += '     </div>';
                        appendDesignSub += ' </div>';
                        appendDesignSub += '</div>';
                        appendDesignSub += '</div>';

                        appendNum += '<div class="row topStyle removeNum">';
                        appendNum += '<div class="col-lg-12">';
                        appendNum += '<div class="panelBox text-right" style="border: 1px solid #ffffff;">';
                        appendNum += i + 1;
                        appendNum += '</div></div></div>';


                        appendDropZoneSub += '<div class="row topStyle dvRemoveSubDropZone dvValSubTaskPrio' + i + '">';
                        appendDropZoneSub += '<div class="col-lg-12">';
                        appendDropZoneSub += ' <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>';
                        appendDropZoneSub += '</div>';
                        appendDropZoneSub += '</div>';
                        countsubTask++

                    }
                }
             
                $('.dvSubPrioDropZone').append(appendDropZoneSub);
                $('.dvSubPrio').append(appendDesignSub);
                $('.dvSubPrioNum').append(appendNum);
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
});
$('#btnSaveWorkflow').click(function () {

    var valWorkflow = "";


    for (i = 0; i < countWorkflow; i++) {
        for (ii = 0; ii < countWorkflow; ii++) {

            if ($('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() == "") {

            }
            else {
                console.log($('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text());
                valWorkflow += (valWorkflow == "" ? $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text() : "," + $('.dvValWorkflowPrio' + i + '').find('#lblWorkflow' + ii + '').text());
            }
        }
    }


    var emp_id = "";
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('.chkRow1').each(function () {
        var sThisVal = (this.checked ? $(this).attr('data-id') : "0");


        if (sThisVal == 0) {

        }
        else {

            emp_id += (emp_id == "" ? sThisVal : "," + sThisVal);
        }

    });
    emp_id = emp_id.replace(/\,0/g, "");;
    console.log(emp_id);
    console.log(valWorkflow);



    if (emp_id == "") {
        alertify.error('Please! Select Employee Name');
    }
    else {
        $.ajax({
            type: 'POST',
            url: 'AgentTaskManager.aspx/applyAction',
            data: '{"valWorkflow" : "' + valWorkflow + '","emp_id" : "' + emp_id + '"}',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                //fnEmployeeList();
                //$('#tblEmployee').bootstrapTable('updateRow', { id: emp_id, row: row });
                $('#SaveAlert').modal({ backdrop: 'static', keyboard: false });
                setTimeout(function () {
                    $('#SaveAlert').modal('hide');
                }, 2000);




                //$('#modalLoading').modal('hide');
            },
            error: function (response) {
                console.log(response.responseText);
            },
            failure: function (response) {
                console.log(response.responseText);
            }
        });
    }

    






});
$('#btnSaveSubTaskPrio').click(function () {
    var SubTaskval = "";
    var emp_id = "";

    if (AssignVal == '') {
        console.log('No Selected');

    }
    else {
        //console.log('Saved : ' + AssignVal);
        for (i = 0; i < countsubTask; i++) {
            for (ii = 0; ii < countsubTask; ii++) {
                //  $('.dvValSubTaskPrio0').find('#lblSubTask7').text()
                if ($('.dvValSubTaskPrio' + i + '').find('#lblSubTask' + ii + '').text() == "") {

                }
                else {
                    //console.log($('.dvValSubTaskPrio' + i + '').find('#lblSubTask' + ii + '').text());
                    SubTaskval += (SubTaskval == "" ? $('.dvValSubTaskPrio' + i + '').find('#lblSubTask' + ii + '').text() : "," +
                        $('.dvValSubTaskPrio' + i + '').find('#lblSubTask' + ii + '').text());
                }
            }
        }
        $('.chkRow1').each(function () {
            var sThisVal = (this.checked ? $(this).attr('data-id') : "0");

            if (sThisVal == 0) {

            }
            else {

                emp_id += (emp_id == "" ? sThisVal : "," + sThisVal);
            }

        });
        console.log(emp_id);
        console.log(SubTaskval);
        if (emp_id == "") {

        }
        else {
            $.ajax({
                type: 'POST',
                url: 'AgentTaskManager.aspx/SaveSubTask',
                data: '{"emp_id" : "' + emp_id + '","AssignVal" : "' + AssignVal + '","SubTaskval" : "' + SubTaskval + '"}',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $('#myModal').modal('hide');
                },
                error: function (response) {
                    console.log(response.responseText);
                },
                failure: function (response) {
                    console.log(response.responseText);
                }
            });
        }




    }



});

function idFormatter1(value) {
    return "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + value + "' />";
}
function idFormatter2(value, row) {
    return "<button type='button' value='" + row.NTID + "' class='btn btn-link ibtn'><i class='fas fa-tasks text-green'></i></button> ";
}
function fnEmployeeList() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'AgentTaskManager.aspx/displayEmployee',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblEmployee').bootstrapTable('destroy');
                $('#tblEmployee').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }

            $('#chkRowAll').click(function () {

                if ($('#chkRowAll').is(':checked') == true)
                {
                    $('.chkRow1').prop('checked', true);

                }
                else
                {
                    $('.chkRow1').prop('checked', false);

                }

            });


        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnApply').click(function () {
    console.log($('#slcEmpName').val());
    console.log($('#slcMGR').val());

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"EmpName" : "' + $('#slcEmpName').val() + '","MGR" : "' + $('#slcMGR').val() + '"}',
        url: 'AgentTaskManager.aspx/displayEmployeeSearch',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblEmployee').bootstrapTable('destroy');
                $('#tblEmployee').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

});

$("#tblEmployee").on("click", ".ibtn", function () {
    $('#mdTask').modal({ backdrop: 'static', keyboard: false });
    console.log($(this).val());
    fnTask($(this).val());

});

function fnTask(ntid) {
    var SubTaskVal = "";
    var Work_Sub = [];
    var Workflow = "";
    var PrioWork = [];
    $('.dvRemoveTask').remove();
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'AgentTaskManager.aspx/GetTask',
        data: '{"ntid" : "' + ntid + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd
                $.each(records, function (idx, val) {
                    if (val.SubTask == null) {

                    }
                    else {
                        SubTaskVal = val.SubTask.split(',');

                    }
                    Workflow = val.Workflow.split(',');

                    for (i = 0; i < SubTaskVal.length; i++) {
                        Work_Sub.push(val.Workflow_Set + '_' + SubTaskVal[i]);

                    }




                });

                for (i = 0; i < Work_Sub.length; i++) {
                    var ValSplitWork = Work_Sub[i].split('_');
                    PrioWork.push(ValSplitWork[0]);
                }


                for (i = 0; i < Workflow.length; i++) {
                    var appendTask = "<div class='dvRemoveTask'><hr />";
                    var firstRun = 0;
                    if ($.inArray(Workflow[i], PrioWork) > -1) {
                        console.log(Workflow[i]);
                        for (ii = 0; ii < Work_Sub.length; ii++) {

                            var subTaskAppend = Work_Sub[ii].split('_');
                            if (firstRun == 0) {
                                if (Workflow[i] == subTaskAppend[0]) {
                                    firstRun = 1;
                                    appendTask += '<div class="row dvRemoveTask">';
                                    appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5" >';
                                    appendTask += subTaskAppend[0];
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-1">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5">';
                                    appendTask += subTaskAppend[1];
                                    appendTask += '</div>';
                                    appendTask += '</div>';

                                    appendTask += '<div class="row dvRemoveTask">';
                                    appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5" >';
                                    appendTask += '';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-1">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5">';
                                    appendTask += '';
                                    appendTask += '</div>';
                                    appendTask += '</div>';
                                }
                            }
                            else {

                                if (Workflow[i] == subTaskAppend[0]) {
                                    appendTask += '<div class="row dvRemoveTask">';
                                    appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5" >';
                                    appendTask += '';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-1">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5">';
                                    appendTask += '';
                                    appendTask += '</div>';
                                    appendTask += '</div>';

                                    appendTask += '<div class="row dvRemoveTask">';
                                    appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5" >';
                                    appendTask += '';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-1">';
                                    appendTask += '</div>';
                                    appendTask += '<div class="col-lg-5">';
                                    appendTask += subTaskAppend[1];
                                    appendTask += '</div>';
                                    appendTask += '</div>';
                                }
                            }

                        }

                        appendTask += "</div>"
                        $('.dvTask').append(appendTask);
                    }
                    else {
                        console.log(Workflow[i]);
                        appendTask += '<div class="row dvRemoveTask">';
                        appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-5" >';
                        appendTask += Workflow[i];
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-1">';
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-5">';
                        appendTask += '';
                        appendTask += '</div>';
                        appendTask += '</div>';

                        appendTask += '<div class="row dvRemoveTask">';
                        appendTask += '<div class="col-lg-1" style="padding-left: 10px">';
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-5" >';
                        appendTask += '';
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-1">';
                        appendTask += '</div>';
                        appendTask += '<div class="col-lg-5">';
                        appendTask += '';
                        appendTask += '</div>';
                        appendTask += '</div>';
                        appendTask += "</div>"
                        $('.dvTask').append(appendTask);
                    }
                }
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#btnRefresh').click(function () {
    $('.dvRefresh').remove();
    fnWorkflowVal();
});
