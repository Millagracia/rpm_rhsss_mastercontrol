﻿$(document).ready(function () {


    fnWorkflowBind();
    fnCount();
}); 
$('#btnWorkflow').click(function () {
    $('.dvWorkflow').show();
    $('.dvAgents').hide();
    $("#btnWorkflow").removeClass("btn-default").addClass("btn-primary");
    $("#btnAgents").removeClass("btn-primary").addClass("btn-default");
    fnWorkflowBind();
});

$('#btnAgents').click(function () {
    $('.dvAgents').show();
    $('.dvWorkflow').hide();
    $("#btnWorkflow").removeClass("btn-primary").addClass("btn-default");
    $("#btnAgents").removeClass("btn-default").addClass("btn-primary");
    fnAgentsBind();
});

function fnWorkflowBind()
{
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'SupervisorDashBoard.aspx/displayWorkflow',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                //$('#tblWorkflow').bootstrapTable('destroy');
                //$('#tblWorkflow').bootstrapTable({
                //    data: records,
                //    pagination: true,
                //    width: 1000
                //});

                var html = '';

                var noofagents = '';
                var Completed = '';
                var InProgress = '';
                var Escalated = '';

                $('.dvWorkflow').find('.page-navigation').remove();
                $(".dvAppendTable").empty()
                $.each(records, function (idx, val) {
                    //$('#myTable > tbody').append('<tr><td>' + val.WorkflowType + '</td><td>' + val.TotalInflow + '</td></tr>');

                    html += '<tr><td>' + val.WorkflowType +
                            '</td><td>' + val.TotalInflow + '</td><td>' + val.ActionRequired +
                            '</td><td>' + val.Count + '</td><td>' + val.TotalLeft +
                            '</td><td>' + val.TimeRequired + '</td><td>' + val.NoOfAgents +
                            '</td><td>' + val.Completed + '</td><td>' + val.InProgress +
                            '</td><td>' + val.Escalated + '</td></tr>';
                });
                $(".dvAppendTable").append(html);
                $('#tblWorkflow').paginate({
                    limit: 12,
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

  

}




function fnAgentsBind() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'SupervisorDashBoard.aspx/displayAgents',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                //$('#tblAgents').bootstrapTable('destroy');
                //$('#tblAgents').bootstrapTable({
                //    data: records,
                //    pagination: true,
                //    width: 1000
                //});

                var html = '';
                $('.dvAgents').find('.page-navigation').remove();
                $(".dvAppendTableAgents").empty()
                $.each(records, function (idx, val) {
                    html +='<tr><td>' + val.Associate +
                           '</td><td>' + val.ImmediateSupervisor + '</td><td>' + val.Status +
                           '</td><td>' + val.Duration + '</td><td>' + val.TotalProcessed +
                           '</td></tr>';
                    html +='<tr><td>' + val.Associate +
                           '</td><td>' + val.ImmediateSupervisor + '</td><td>' + val.Status +
                           '</td><td>' + val.Duration + '</td><td>' + val.TotalProcessed +
                           '</td></tr>';
                    html +='<tr><td>' + val.Associate +
                           '</td><td>' + val.ImmediateSupervisor + '</td><td>' + val.Status +
                           '</td><td>' + val.Duration + '</td><td>' + val.TotalProcessed +
                           '</td></tr>';
                    html +='<tr><td>' + val.Associate +
                           '</td><td>' + val.ImmediateSupervisor + '</td><td>' + val.Status +
                           '</td><td>' + val.Duration + '</td><td>' + val.TotalProcessed +
                           '</td></tr>';
                });
                $(".dvAppendTableAgents").append(html);
               
                $('#tblAgents').paginate({
                    limit: 12,
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function fnCount()
{

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            url: 'SupervisorDashBoard.aspx/displayCount',
            success: function (data) {
                var d = $.parseJSON(data.d);
                if (d.Success) {
                    var records = d.data.asd;
                    $.each(records, function (idx, val) {

                        if (val.Left == 1)
                        {
                            if (val.Number == null)
                            {
                                $('#lblBacklog').html('0');

                            }
                            else
                            {
                                $('#lblBacklog').html(val.Number);

                            }
                        }
                        else
                        {
                            if (val.Number == null) {
                                $('#lblTotalLeft').html('0');

                            }
                            else {
                                $('#lblTotalLeft').html(val.Number);

                            }
                        }
                    });
                }
            }, error: function (response) {
                console.log(response.responseText);
            }, failure: function (response) {
                console.log(response.responseText);
            }
        });



}