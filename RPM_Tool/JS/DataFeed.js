﻿$(document).ready(function () {
    var change = 0;
    fnWorkflowBind();
    fnBindHistory();
    loadData();
    minutes();
    hours();
});
var appendedNameCount = 0;
var countAppend = 0;
//ALERTS -- RECIEVED/NOT RECIEVED DROPDOWN -- DISABLED AT LOAD
$('.selectHrMinSec').attr('disabled', true);
$('.selectLabelHrMinSec').attr('disabled', true);
$('.selectEveryHrMinSec').attr('disabled', true);
$('.selectEveryLabelHrMinSec').attr('disabled', true);
$('.btnGetTo').attr('disabled', true);

$('.dvAlerts .editDataFeed').attr('disabled', true);
$('.txtLabel').attr('disabled', true);
$('.validateAlertCB').attr('disabled', false);

function fnBindHistory() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'DataFeed.aspx/bindHistory',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblHistory').bootstrapTable('destroy');
                $('#tblHistory').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function dateFormat(value, row, index) {
    return moment(value).format('MM/DD/YYYY');
}
function edit(value, row, index) {
    return '<button type="button" value="' + value + '" class="btn btn-link btnEdit"><i class="far fa-edit fa-lg"></i></button><button value="' + value
        + '"  type="button" class="btn btn-link btnDelete"><i class="far fa-trash-alt text-red fa-lg"></i></button>';
}
$(".dvAlerts").on("click", "#btnAddAppend", function () {
    var btnValue = $(this).val();
    var appendAlerts = '<div class="alertDiv" data-xxx="' + countAppend + '">' +
                        '<div class="row" style="margin-bottom: 5px;">' +
                        '<div class="col-lg-1"></div>' +
                        '<div class="col-lg-1">' +
                        '<div class="col-lg-3 noPaddingR" style="padding-top: 4px">' +
                        '<input type="checkbox" id="cbIsActive' + countAppend + '" class="editDataFeed validateAlertCB" data-xxx="' + countAppend + '"/>' +
                        '</div>' +
                        '<div class="col-lg-9 noPaddingR" style="padding-top: 4px">' +
                        'If file is' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-lg-2">' +
                        '<div class="col-lg-8" style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcReceived' + countAppend + '" class="input-sm tb1 form-control selectRecieved editDataFeed" data-xxx="' + countAppend + '">' +
                        '<option value="Received">Received</option>' +
                        '<option value="Not Received">Not Received</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-lg-4 text-center" style="padding-left: 2px; padding-right: 2px; padding-top: 4px">After</div>' +
                        '</div>' +
                        '<div class="col-lg-2">' +
                        '<div class="col-lg-5" style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcHrMinSec' + countAppend + '" class=" input-sm tb1 form-control selectHrMinSec minutesDropdown"  data-xxx="' + countAppend + '">' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-lg-7" style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcMHDWM' + countAppend + '" class=" input-sm tb1 form-control selectLabelHrMinSec"  data-xxx="' + countAppend + '">' +
                        '<option value="00">Select</option>' +
                        '<option value="minute(s)">minute(s)</option>' +
                        '<option value="hour(s)">hour(s)</option>' +
                        '<option value="day(s)">day(s)</option>' +
                        '<option value="week(s)">week(s)</option>' +
                        '<option value="month(s)">month(s)</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-lg-2">' +
                        '<div class="col-lg-4 text-center" style="padding-left: 2px; padding-right: 2px; padding-top: 4px">then send</div>' +
                        '<div class="col-lg-8" style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcSuccess' + countAppend + '" class=" input-sm tb1 form-control editDataFeed selectSend" data-xxx="' + countAppend + '">' +
                        '<option value="Success Alert">Success Alert</option>' +
                        '<option value="Failure Alert">Failure Alert</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-lg-1"></div>' +
                        '<div class="col-lg-2">' +
                        '<button id="btnAddAppend" class="btn btn-link editDataFeed" type="button" value="' + countAppend + '" data-xxx="' + countAppend + '"><i class="fas fa-plus text-green"></i></button>' +
                        '<button id="btnRemoveAppend" class="btn btn-link editDataFeed" type="button" value="' + countAppend + '" data-xxx="' + countAppend + '"><i class="fas fa-trash-alt text-red"></i></button>' +
                        '</div>' +
                        '<div class="col-lg-1"></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-lg-1"></div>' +
                        '<div class="col-lg-1">' +
                        '<div class="col-lg-7 noPaddingR"></div>' +
                        '<div class="col-lg-1 noPaddingR" style="padding-top: 4px">to</div>' +
                        '<div class="col-lg-3 noPaddingR"></div>' +
                        '</div>' +
                        '<div class="col-lg-2">' +
                        '<input id="txtTo' + countAppend + '" type="text" class="form-control txtLabel autocom " data-xxx="' + countAppend + '"/>' +
                        '<label id="lblToAppend' + countAppend + '"></label>' +
                        '<div class="appendLabel" style="width: 250px;" value="' + countAppend + '"></div>' +
                        '</div>' +
                        '<div class="col-lg-1 noPaddingLR">' +
                        '<div class="col-lg-4 noPaddingR">' +
                        '<button id="btnAddTo" value="' + countAppend + '" class="btn btn-link btnGetTo" type="button" data-xxx="' + countAppend + '"><i class="fas fa-plus text-green"></i></button>' +
                        '</div>' +
                        '<div class="col-lg-2 noPaddingR " style="padding-top: 4px">every</div>' +
                        '</div>' +
                        '<div class="col-lg-2">' +
                        '<div class="col-lg-5 " style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcForHrMinSec' + countAppend + '" class=" input-sm tb1 form-control selectEveryHrMinSec minutesDropdown"  data-xxx="' + countAppend + '">' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-lg-7 " style="padding-left: 2px; padding-right: 2px">' +
                        '<select id="slcForMHDWM' + countAppend + '" class=" input-sm tb1 form-control selectEveryLabelHrMinSec"  data-xxx="' + countAppend + '">' +
                        '<option value="00">Select</option>' +
                        '<option value="minute(s)">minute(s)</option>' +
                        '<option value="hour(s)">hour(s)</option>' +
                        '<option value="day(s)">day(s)</option>' +
                        '<option value="week(s)">week(s)</option>' + 
                        '<option value="month(s)">month(s)</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '<div class="6"></div>' +
                        '</div>' +
                        '</div>';
    $('.dvAlerts').append(appendAlerts);

    //ALERTS -- RECIEVED/NOT RECIEVED DROPDOWN -- DISABLED AT LOAD
    $('#slcHrMinSec' + countAppend).attr('disabled', true);
    $('#slcMHDWM' + countAppend).attr('disabled', true);
    $('#slcForHrMinSec' + countAppend).attr('disabled', true);
    $('#slcForMHDWM' + countAppend).attr('disabled', true);

    $('#btnAddAppend[value="' + btnValue + '"]').hide();
    $('#btnRemoveAppend[value="' + btnValue + '"]').hide();

    countAppend++;
    minutes();

    //ALERTS -- RECIEVED/NOT RECIEVED DROPDOWN
    $(".selectRecieved").on("change", function () {
        var number = $(this).attr('id').replace(/\D/g, '');
        if ($(this).val() == 'Received') {
            $('#slcHrMinSec' + number).attr('disabled', true);
            $('#slcMHDWM' + number).attr('disabled', true);
            $('#slcForHrMinSec' + number).attr('disabled', true);
            $('#slcForMHDWM' + number).attr('disabled', true);
        } else {
            $('#slcHrMinSec' + number).attr('disabled', false);
            $('#slcMHDWM' + number).attr('disabled', false);
            $('#slcForHrMinSec' + number).attr('disabled', false);
            $('#slcForMHDWM' + number).attr('disabled', false);
        }
    });
});
$(".dvAlerts").on("click", "#btnRemoveAppend", function () {
    var alertValue = $(this).val();
    var btns = parseInt(alertValue) - 1;
    if (btns == -1) { btns = 'x'; $('#btnRemoveAppend[value="' + btns + '"]').hide(); }
    if (alertValue != '' && alertValue > 0) {
        $('#btnAddAppend[value="' + btns + '"]').show();
        $('#btnRemoveAppend[value="' + btns + '"]').show();
        $('.alertDiv[data-xxx="' + alertValue + '"]').remove();
        countAppend--;
    } else if (alertValue == 0) {
        $('#btnAddAppend[value="' + btns + '"]').show();
        $('#btnRemoveAppend[value="' + btns + '"]').hide();
        $('.alertDiv[data-xxx="' + alertValue + '"]').remove();
        countAppend--;
    }
});
$(function () {
    $("[class$=autocom]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        appendTo: $("#txtTo").next(),
        minLength: 1,
    });
});
$(".dvAlerts").on("click", ".btnGetTo", function () {
    var data = $(this).data('xxx');
    appendedNameCount++;
    if ($(this).val() == "") {
        var valTo = $('#lblTo').html() == '' ? $('#txtTo').val() : '; ' + $('#txtTo').val();
        $('.appendLabel[value="x"]').append('<div class="appendedName' + appendedNameCount + '"><label id="lblTo' + appendedNameCount + '" class="labelEmail">' + valTo.replace('; ', '') + '</label><button value=' + appendedNameCount + ' type="button" id="btnRemove" class="btn btn-box-tool removeBtn"><i class="fa fa-remove"></i></button></div>');
        document.getElementById('txtTo').value = '';
    }
    else {
        var valTo = $('#lblTo' + $(this).val() + '').html() == '' ? $('#txtTo' + $(this).val() + '').val() : '; ' + $('#txtTo' + $(this).val() + '').val();
        //$("#lblTo" + $(this).val() + '').append(valTo);
        $('.appendLabel[value="' + data + '"]').append('<div class="appendedName' + appendedNameCount + '"><label id="lblTo' + appendedNameCount + '" class="labelEmail">' + valTo.replace('; ', '') + '</label><button value="' + appendedNameCount + '" type="button" id="btnRemove" class="btn btn-box-tool removeBtn"><i class="fa fa-remove"></i></button></div>');
        document.getElementById('txtTo' + $(this).val() + '').value = '';
    }
});
$('.dvAlerts').on('click', '#btnRemove', function () {
    var divValue = $(this).parent().parent().attr('value');
    var labelValue = $(this).val();
    $('.appendLabel[value=' + divValue + ']').find('.appendedName' + labelValue).remove();
});
function fnWorkflowBind() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/ddlValues',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var records1 = d.data.asd1;
                var slcVal;
                $('#slcWorkflow,#slcHistoryWorkflow,#slcCreatedBy').empty();
                $('#slcWorkflow,#slcHistoryWorkflow,#slcCreatedBy').append("<option value='0'  selected='selected' disabled >Select</option>");

                $.each(records, function (idx, val) {
                    //slcVal = val.List_Option.split(',');
                    ta1 = "<option value='" + val.List_Option + "' >" + val.List_Option + "</option>";
                    $('#slcWorkflow,#slcHistoryWorkflow').append(ta1);
                });
                $.each(records1, function (idx, val) {
                    ta1 = "<option value='" + val.CreatedBy + "' >" + val.CreatedBy + "</option>";
                    $('#slcCreatedBy').append(ta1);

                });
                //for (i = 0; i < slcVal.length; i++) {
                //    ta1 = "<option value='" + slcVal[i] + "' >" + slcVal[i] + "</option>";
                //    $('#slcWorkflow,#slcHistoryWorkflow').append(ta1);
                //}
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#btnSave').click(function () {
    var emails = [];
    $('.appendLabel label').each(function () {
        emails.push($(this).text() + ';');
    });
    var emailJoin = emails.join();

    var GetTitle = [];
    var GetCondition = [];
    var GetLAlertMassage = [];
    var Days = '';
    var atTime = $('#slcHrs').val() + ':' + $('#slcMins').val() + ' ' + $('#slcAMPM').val();
    var atTimetimeZone = $('#slcTimezone').val();
    var To = $('#slcToHrs').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcToAMPM').val();
    var TotimeZone = $('#slcToTimezone').val();
    var isActive = $('#cbIsActive').is(':checked') == true ? '1' : '0';
    var isReceived = $('#slcReceived').val();
    var NoMHDWM = $('#slcHrMinSec').val() + ' ' + $('#slcMHDWM').val();
    var isSuccess = $('#slcSuccess').val();
    var EmailTo = emailJoin.replace(';,', ';');
    var NoSec = $('#slcForHrMinSec').val() + ' ' + $('#slcForMHDWM').val();

    if ($('#weekday-mon').is(':checked') == true) {
        Days = Days == "" ? 'Monday' : ',Monday';
    }
    if ($('#weekday-tue').is(':checked') == true) {
        Days += Days == "" ? 'Tuesday' : ',Tuesday';
    }
    if ($('#weekday-wed').is(':checked') == true) {
        Days += Days == "" ? 'Wednesday' : ',Wednesday';
    }
    if ($('#weekday-thu').is(':checked') == true) {
        Days += Days == "" ? 'Thursday' : ',Thursday';
    }
    if ($('#weekday-fri').is(':checked') == true) {
        Days += Days == "" ? 'Friday' : ',Friday';
    }
    if ($('#weekday-sat').is(':checked') == true) {
        Days += Days == "" ? 'Saturday' : ',Saturday';
    }
    if ($('#weekday-sun').is(':checked') == true) {
        Days += Days == "" ? 'Sunday' : ',Sunday';
    }

    GetTitle.push($('#txtFileName').val());
    GetTitle.push('RRI');
    GetTitle.push($('#txtLocation').val());
    GetTitle.push(Days);
    GetTitle.push(atTime);
    GetTitle.push(To);
    GetTitle.push(atTimetimeZone + ',' + TotimeZone);

    for (i = 0; i < countAppend; i++) {
        GetCondition.push($('#cbIsActive' + i + '').is(':checked') == true ? '1' : '0');
        GetCondition.push($('#slcReceived' + i + '').val());
        GetCondition.push($('#slcHrMinSec' + i + '').val() + ' ' + $('#slcMHDWM' + i + '').val());
        GetCondition.push($('#slcSuccess' + i + '').val());
        GetCondition.push(emailJoin.replace(';,', '; '));
        GetCondition.push($('#slcForHrMinSec' + i + '').val() + ' ' + $('#slcForMHDWM' + i + '').val());
    }
    GetCondition.push(isActive);
    GetCondition.push(isReceived);
    GetCondition.push(NoMHDWM);
    GetCondition.push(isSuccess);
    GetCondition.push(EmailTo);
    GetCondition.push(NoSec);

    GetLAlertMassage.push($('#txtSuccAlert').val());
    GetLAlertMassage.push($('#txtSuccAreaMessage').val());
    GetLAlertMassage.push($('#txtFailAlert').val());
    GetLAlertMassage.push($('#txtFailAreaMessage').val());

    //VALIDATION
    validate();

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        data: '{GetTitle : ' + JSON.stringify(GetTitle) +
            ',GetLAlertMassage : ' + JSON.stringify(GetLAlertMassage) +
            ',GetCondition : ' + JSON.stringify(GetCondition) + '}',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/InsertData',
        success: function (data) {
            $(".dvDatafeed").hide();
            $(".dvHistory").show();
            $("#btnDataFeed").removeClass("btn-primary").addClass("btn-default");
            $("#btnHistory").removeClass("btn-default").addClass("btn-primary");
            $('.dvRemoveAll').remove();
            $('#txtFileName').val('');
            $('#weekday-mon').prop('checked', false);
            $('#weekday-tue').prop('checked', false);
            $('#weekday-wed').prop('checked', false);
            $('#weekday-thu').prop('checked', false);
            $('#weekday-fri').prop('checked', false);
            $('#weekday-sat').prop('checked', false);
            $('#weekday-sun').prop('checked', false);

            $('#slcWorkflow').val('0');
            $('#txtLocation').val('');

            $('#slcHrs').val('00');
            $('#slcMins').val('00');
            $('#slcAMPM').val('AM/PM');
            $('#slcToHrs').val('00');
            $('#slcToMins').val('00');
            $('#slcToAMPM').val('AM/PM');
            $('#slcToTimezone').val('00');
            $('#slcTimezone').val('00');

            $('.dvAlerts').find('.dvRemoveBtnAdd').remove();
            var appendBTN = '<div class="col-lg-1 dvRemoveBtnAdd">';
            appendBTN += '<button id="btnAddAppend" class="btn btn-link" type="button"><i class="fas fa-plus text-green"></i></button>';
            appendBTN += '</div>';

            $('.dvAlerts').find('.dvBtnAppend').append(appendBTN);

            $('#cbIsActive').prop('checked', false);
            $('#slcReceived').val('Received');
            $('#slcHrMinSec').val('00');
            $('#slcMHDWM').val('00');
            $('#slcSuccess').val('Success Alert');
            $('#txtTo').val('');
            $('#lblTo').html('');
            $('#slcForHrMinSec').val('00');
            $('#slcForMHDWM').val('00');

            $('#txtSuccAlert').val('');
            $('#txtSuccAreaMessage').val('');
            $('#txtFailAlert').val('');
            $('#txtFailAreaMessage').val('');

            fnBindHistory();
            countAppend = 0;
            $('#modalLoading').modal('hide');

            $('.editDataFeed').attr('disabled', true);
            $('#txtTo').attr('disabled', true);
            location.reload();
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });
});
$("#btnHistory").click(function () {
    $(".dvDatafeed").hide();
    $(".dvHistory").show();
    $("#btnDataFeed").removeClass("btn-primary").addClass("btn-default");
    $("#btnHistory").removeClass("btn-default").addClass("btn-primary");
});
$("#btnDataFeed").click(function () {
    $(".dvDatafeed").show();
    $(".dvHistory").hide();
    $("#btnHistory").removeClass("btn-primary").addClass("btn-default");
    $("#btnDataFeed").removeClass("btn-default").addClass("btn-primary");
});
$('#btnApply').click(function () {

    console.log($('#slcHistoryWorkflow').val());
    console.log($('#slcCreatedBy').val());
    console.log($('#txtFromDate').val());
    console.log($('#txtToDate').val());


});
$("#tblHistory").on("click", ".btnDelete", function () {

    $.ajax({
        type: 'POST',
        url: 'DataFeed.aspx/DeleteDataFeed',
        data: '{"id" : "' + $(this).val() + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            fnBindHistory();
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});
$("#tblHistory").on("click", ".btnEdit", function () {

    //console.log($(this).val());
    //$.ajax({
    //    type: 'POST',
    //    url: 'DataFeed.aspx/EditHistory',
    //    data: '{"id" : "' + $(this).val() + '"}',
    //    dataType: 'json',
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (data) {
    //        //txtFileName
    //        var d = $.parseJSON(data.d);
    //        var records = d.data.asd;

    //        $.each(records, function (idx, val) {

    //        });

    //    },
    //    error: function (response) {
    //        console.log(response.responseText);
    //    },
    //    failure: function (response) {
    //        console.log(response.responseText);
    //    }
    //});
});
//ALERTS -- RECIEVED/NOT RECIEVED DROPDOWN
$('.dvAlerts').on('change', '.selectRecieved', function () {
    var number = $(this).attr('id').replace(/\D/g, '');
    if ($(this).val() == 'Received') {
        //DISABLED WHEN RECIEVED
        $('#slcHrMinSec' + number).attr('disabled', true);
        $('#slcMHDWM' + number).attr('disabled', true);
        $('#slcForHrMinSec' + number).attr('disabled', true);
        $('#slcForMHDWM' + number).attr('disabled', true);
        //CLEAR VALUES
        $('#slcHrMinSec' + number).val('00');
        $('#slcMHDWM' + number).val('00');
        $('#slcForHrMinSec' + number).val('00');
        $('#slcForMHDWM' + number).val('00');
    } else {
        //ENABLED WHEN NOT RECIEVED
        $('#slcHrMinSec' + number).attr('disabled', false);
        $('#slcMHDWM' + number).attr('disabled', false);
        $('#slcForHrMinSec' + number).attr('disabled', false);
        $('#slcForMHDWM' + number).attr('disabled', false);
    }
});
//LOAD DATA
function loadData() {
    //GET DATA
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{}",
        url: 'DataFeed.aspx/DataLoad',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d != null) {
                var create = d.data.dataFeedCreated;
                var feed = d.data.dataFeed;
                var alerts = d.data.dataFeedAlert;
                var messages = d.data.dataFeedAlertConditions;

                for (var i = 0; i < feed.length; i++) {
                    //FILENAME & FILELOC --------------------------
                    $('#txtFileName').val(feed[i].Title);
                    $('#txtLocation').val(feed[i].AtLocation);

                    //DAYS ----------------------------------------
                    var days = feed[i].OnDays.split(',');
                    $.each(days, function (index, value) {
                        $('.' + days[index]).prop('checked', true);
                    });

                    //TIME AT
                    var timeAt = feed[i].TimeFrom.split(':');
                    $.each(timeAt, function (index, value) {
                        //TIME
                        if (index == 0) {
                            //AM/PM
                            if (timeAt[0] >= 13 && timeAt[0] <= 21) {
                                $('#slcHrs').val('0' + (timeAt[0] - 12).toString());
                                $('#slcAMPM').val('PM');
                            } else if (timeAt[0] > 21) {
                                $('#slcHrs').val(timeAt[0] - 12);
                                $('#slcAMPM').val('PM');
                            } else if (timeAt[0] == 0) {
                                $('#slcHrs').val('12');
                                $('#slcAMPM').val('AM');
                            } else {
                                $('#slcHrs').val(timeAt[0]);
                                $('#slcAMPM').val('AM');
                            }
                        } else if (index == 1) {
                            $('#slcMins').val(timeAt[1]);
                        }
                    });

                    //TIME TO -------------------------------------
                    var timeTo = feed[i].TimeTo.split(':');
                    $.each(timeTo, function (index, value) {
                        //TIME
                        if (index == 0) {
                            //AM/PM
                            if (timeTo[0] >= 13 && timeTo[0] <= 21) {
                                $('#slcToHrs').val('0' + (timeTo[0] - 12).toString());
                                $('#slcToAMPM').val('PM');
                            } else if (timeTo[0] > 21) {
                                $('#slcToHrs').val(timeTo[0] - 12);
                                $('#slcToAMPM').val('PM');
                            } else if (timeTo[0] == 0) {
                                $('#slcToHrs').val('12');
                                $('#slcToAMPM').val('AM');
                            } else {
                                $('#slcToHrs').val(timeTo[0]);
                                $('#slcToAMPM').val('AM');
                            }
                        } else if (index == 1) {
                            $('#slcToMins').val(timeTo[1]);
                        }
                    });

                    //ALERT MESSAGE ----------------------------------------
                    var indexCount = 1;
                    $.each(alerts, function () {
                        $.each(this, function (index, value) {
                            if (index == 'Subject' && indexCount == 1) {
                                $('#txtSuccAlert').val(value);
                            } else if (index == 'Message' && indexCount == 1) {
                                $('#txtSuccAreaMessage').val(value);
                            }
                            if (index == 'Subject' && indexCount == 2) {
                                $('#txtFailAlert').val(value);
                            } else if (index == 'Message' && indexCount == 2) {
                                $('#txtFailAreaMessage').val(value);
                            }
                        });
                        indexCount++;
                    });
                    indexCount = 1;
                    var data = 'x';
                    //ALERTS
                    $.each(messages, function (index, value) {
                        if (index == 0) {
                            if (messages[index].isActive == 1) {
                                $('#cbIsActive[data-xxx="' + data + '"]').prop('checked', true);
                            } else {
                                $('#cbIsActive[data-xxx="' + data + '"]').prop('checked', false);
                            }
                            $('#slcReceived[data-xxx="' + data + '"]').val(messages[index].isRecieve);
                            $('#slcHrMinSec[data-xxx="' + data + '"]').val(messages[index].AfterNo);
                            $('#slcMHDWM[data-xxx="' + data + '"]').val(messages[index].AfterSet);
                            $('#slcForHrMinSec[data-xxx="' + data + '"]').val(messages[index].EveryNo);
                            $('#slcForMHDWM[data-xxx="' + data + '"]').val(messages[index].EverySet);
                            $('#slcSuccess[data-xxx="' + data + '"]').val(messages[index].isSuccess);

                            if (messages[index].isRecieve == 'Received') {
                                $('#slcHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
                                $('#slcMHDWM[data-xxx="' + data + '"]').attr('disabled', true);
                                $('#slcForHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
                                $('#slcForMHDWM[data-xxx="' + data + '"]').attr('disabled', true);
                            } else {
                                $('#slcHrMinSec[data-xxx="' + data + '"]').attr('disabled', false);
                                $('#slcMHDWM[data-xxx="' + data + '"]').attr('disabled', false);
                                $('#slcForHrMinSec[data-xxx="' + data + '"]').attr('disabled', false);
                                $('#slcForMHDWM[data-xxx="' + data + '"]').attr('disabled', false);
                            }

                            $.each(messages[index].Email_To.split(';'), function (index, value) {
                                $('#btnAddTo[data-xxx="' + data + '"]').attr('disabled', false);
                                $('#btnAddTo[data-xxx="' + data + '"]').click();
                                $('#lblTo' + appendedNameCount).text(value);
                                $('#btnAddTo[data-xxx="' + data + '"]').attr('disabled', true);
                            });
                        } else if (index > 0 && index <= messages.length) {
                            $('#btnAddAppend[data-xxx="' + data + '"]').attr('disabled', false);
                            $('#btnAddAppend[data-xxx="' + data + '"]').click();
                            $('#btnAddAppend[data-xxx="' + data + '"]').attr('disabled', true);
                            for (var alertCount = 0; alertCount < messages.length - 1; alertCount++) {
                                if (messages[alertCount].isActive == 1) {
                                    $('#cbIsActive' + alertCount).prop('checked', true);
                                } else {
                                    $('#cbIsActive' + alertCount).prop('checked', false);
                                }
                                $('#slcReceived' + alertCount).val(messages[alertCount + 1].isRecieve);
                                $('#slcHrMinSec' + alertCount).val(messages[alertCount + 1].AfterNo);
                                $('#slcMHDWM' + alertCount).val(messages[alertCount + 1].AfterSet);
                                $('#slcForHrMinSec' + alertCount).val(messages[alertCount + 1].EveryNo);
                                $('#slcForMHDWM' + alertCount).val(messages[alertCount + 1].EverySet);
                                $('#slcSuccess' + alertCount).val(messages[alertCount + 1].isSuccess);
                                if (messages[alertCount + 1].isRecieve == 'Received') {
                                    $('#slcHrMinSec' + alertCount).attr('disabled', true);
                                    $('#slcMHDWM' + alertCount).attr('disabled', true);
                                    $('#slcForHrMinSec' + alertCount).attr('disabled', true);
                                    $('#slcForMHDWM' + alertCount).attr('disabled', true);
                                } else {
                                    //$('#slcHrMinSec' + alertCount).attr('disabled', false);
                                    //$('#slcMHDWM' + alertCount).attr('disabled', false);
                                    //$('#slcForHrMinSec' + alertCount).attr('disabled', false);
                                    //$('#slcForMHDWM' + alertCount).attr('disabled', false);
                                }
                                $.each(messages[alertCount + 1].Email_To.split(';'), function (index, value) {
                                    var xxx = countAppend - 1;
                                    $('#btnAddTo[value="' + xxx + '"]').click();
                                    $('.appendLabel[value="' + alertCount + '"]').find('#lblTo' + appendedNameCount).text(value);
                                });
                            }
                        }
                    });
                    $.each($('.labelEmail'), function (index, value) {
                        var text = value.textContent;
                        var parent = $(this).parent().attr('class');
                        if (text == '') {
                            $('.' + parent).remove();
                        }
                    });
                }

                $('#btnSave').css('display', 'none');
                $('#btnUpdate').css('display', '');
                $('#btnEdit').css('display', '');
                $('.editDataFeed').attr('disabled', true);
                $('.autocom').attr('disabled', true);
                $('.removeBtn').attr('disabled', true);
                $('.btnGetTo').attr('disabled', true);

            } else {
                $('#btnSave').css('display', '');
                $('#btnUpdate').css('display', 'none');
                $('#btnEdit').css('display', 'none');
                $('.editDataFeed').attr('disabled', false);
                $('.autocom').attr('disabled', false);
                $('.btnGetTo').attr('disabled', true);
                $('.removeBtn').attr('disabled', false);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

}
//EDIT FIELDS
$('#btnEdit').click(function () {
    $('.editDataFeed').attr('disabled', false);
    $('#txtTo').attr('disabled', false);
    $('.removeBtn').attr('disabled', false);
    $('.autocom').attr('disabled', false);
    $('#btnUpdate').attr('disabled', false);

    $.each($('.alertDiv'), function (index, value) {
        var data = $(this).val();
        if ($('.validateAlertCB[data-xxx="' + data + '"]').prop('checked') && $('.selectRecieved[data-xxx="' + data + '"]').val() == 'Not Received') {
            $('.selectHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
            $('.selectLabelHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
            $('.selectEveryHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
            $('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);
        }
    });
});
//VALIDATION FOR WORKFLOW FEEDER
$('.dvAlerts').on('input', '.autocom', function () {
    var textValue = $(this).val();
    var data = $(this).data('xxx');
    if (textValue != '') {
        $('.btnGetTo[data-xxx="' + data + '"]').attr('disabled', false);
    } else {
        $('.btnGetTo[data-xxx="' + data + '"]').attr('disabled', true);
    }
});
$('.dvAlerts').on('change', '.validateAlertCB', function () {
    var data = $(this).data('xxx');
    if ($(this).prop('checked')) {
        $('.dvAlerts .editDataFeed[data-xxx="' + data + '"]').attr('disabled', false);
        $('.txtLabel[data-xxx="' + data + '"]').attr('disabled', false);
        $('.btnGetTo[data-xxx="' + data + '"]').attr('disabled', false);
        $('.validateAlertCB[data-xxx="' + data + '"]').attr('disabled', false);
    } else {
        $('.dvAlerts .editDataFeed[data-xxx="' + data + '"]').attr('disabled', true);
        $('.txtLabel[data-xxx="' + data + '"]').attr('disabled', true);
        $('.btnGetTo[data-xxx="' + data + '"]').attr('disabled', true);
        $('.selectHrMinSec[data-xxx="' + data + '"], .selectLabelHrMinSec[data-xxx="' + data + '"],' +
            '.selectEveryHrMinSec[data-xxx="' + data + '"], .selectEveryLabelHrMinSec[data-xxx="' + data + '"]').attr('disabled', true);

        $('.validateAlertCB[data-xxx="' + data + '"]').attr('disabled', false);

        $('.selectHrMinSec[data-xxx="' + data + '"], .selectLabelHrMinSec[data-xxx="' + data + '"],' +
             '.selectEveryHrMinSec[data-xxx="' + data + '"], .selectEveryLabelHrMinSec[data-xxx="' + data + '"]').val('00');
        $('.selectRecieved[data-xxx="' + data + '"]').val('Received');
        $('.selectSend[data-xxx="' + data + '"]').val('Success Alert');
        $('.appendLabel[value="' + data + '"]').empty();

        $('.txtLabel[data-xxx="' + data + '"]').css('border-color', '');
        $('.selectHrMinSec[data-xxx="' + data + '"]').css('border', '');
        $('.selectLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');
        $('.selectEveryHrMinSec[data-xxx="' + data + '"]').css('border', '');
        $('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');

    }
});
//MINUTES DROPDOWN
function minutes() {
    for (var i = 0; i <= 59; i++) {
        if (i <= 9) {
            $('.minutesDropdown').append('<option value="0' + i + '">0' + i + '</option>');
        } else {
            $('.minutesDropdown').append('<option value="' + i + '">' + i + '</option>');
        }
    }
}
//HOURS DROPDOWN
function hours() {
    for (var i = 0; i <= 12; i++) {
        if (i <= 9) {
            $('.hoursDropdown').append('<option value="0' + i + '">0' + i + '</option>');
        } else {
            $('.hoursDropdown').append('<option value="' + i + '">' + i + '</option>');
        }
    }
}
function validate() {
    //if ($('#btnSave').is(':visible')) {
    $.each($('.validateSave[type="text"]'), function (index, value) {
        if ($(value).val() == '') {
            $('.validateSave[type="text"][id="' + $(value).attr('id') + '"]').css('border-color', 'red');
        } else {
            $('.validateSave[type="text"][id="' + $(value).attr('id') + '"]').css('border-color', '');
        }
    });

    $.each($('select.validateSave'), function (index, value) {
        if ($(value).val() == '00' || $(value).val() == 'AM/PM') {
            $('select.validateSave[id="' + $(value).attr('id') + '"]').css('border-color', 'red');
        } else {
            $('select.validateSave[id="' + $(value).attr('id') + '"]').css('border-color', '');
        }
    });
    var alertMess = 4; //counts the alert messages modal textbox/textarea
    $.each($('.validateSaveMessage'), function (index, value) {
        if ($(value).val() == '') {
            $('.validateSaveMessage#' + $(value).attr('id')).css('border-color', 'red');
        } else {
            $('.validateSaveMessage#' + $(value).attr('id')).css('border-color', '');
            alertMess--;
        }
        if (alertMess == 0) { $('#btnAlert').css('border-color', '') } else { $('#btnAlert').css('border-color', 'red') }
    });
    var days = 0; // checks if either 1 day is toggled
    $.each($('.validateSaveDay'), function (index, value) {
        if ($(value).prop('checked')) { $('.validateSaveDay').next().css('outline', ''); return false; } else { $('.validateSaveDay').next().css('outline', '1px solid red'); }
    });

    $.each($('.validateAlertCB'), function (index, value) {
        var dataXXX = $(this).data('xxx');
        if ($(this).prop('checked')) {
            if ($('.labelEmail').length == 0) {
                $('.txtLabel[data-xxx="' + dataXXX + '"]').css('border', '1px solid red');
            } else {
                $('.txtLabel[data-xxx="' + dataXXX + '"]').css('border', '');
            }
        } else {
            $('.txtLabel[data-xxx="' + dataXXX + '"]').css('border', '');
        }
    });
    //RECEIVED OR NOT RECIEVED
    $.each($('.selectRecieved'), function (index, value) {
        var id = $(this).attr('id');
        var data = $(this).data('xxx');
        if ($('[id="' + id + '"]').val() == 'Not Received') {
            if ($('.selectHrMinSec[data-xxx="' + data + '"]').val() == "00") {
                $('.selectHrMinSec[data-xxx="' + data + '"]').css('border', '1px solid red');
            } else {
                $('.selectHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectLabelHrMinSec[data-xxx="' + data + '"]').val() == "00") {
                $('.selectLabelHrMinSec[data-xxx="' + data + '"]').css('border', '1px solid red');
            } else {
                $('.selectLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectEveryHrMinSec[data-xxx="' + data + '"]').val() == "00") {
                $('.selectEveryHrMinSec[data-xxx="' + data + '"]').css('border', '1px solid red');
            } else {
                $('.selectEveryHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').val() == "00") {
                $('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').css('border', '1px solid red');
            } else {
                $('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
        } else {
            if ($('.selectHrMinSec[data-xxx="' + data + '"]').val() != "00") {
                $('.selectHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectLabelHrMinSec[data-xxx="' + data + '"]').val() != "00") {
                $('.selectLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectEveryHrMinSec[data-xxx="' + data + '"]').val() != "00") {
                $('.selectEveryHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
            if ($('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').val() != "00") {
                $('.selectEveryLabelHrMinSec[data-xxx="' + data + '"]').css('border', '');
            }
        }
    });
    //}

    return true;
}
//UPDATE
$('#btnUpdate').click(function () {
    var days = "";
    var obj = {};

    if ($('#weekday-mon').is(':checked') == true) {
        days = days == "" ? 'Monday' : ',Monday';
    }
    if ($('#weekday-tue').is(':checked') == true) {
        days += days == "" ? 'Tuesday' : ',Tuesday';
    }
    if ($('#weekday-wed').is(':checked') == true) {
        days += days == "" ? 'Wednesday' : ',Wednesday';
    }
    if ($('#weekday-thu').is(':checked') == true) {
        days += days == "" ? 'Thursday' : ',Thursday';
    }
    if ($('#weekday-fri').is(':checked') == true) {
        days += days == "" ? 'Friday' : ',Friday';
    }
    if ($('#weekday-sat').is(':checked') == true) {
        days += days == "" ? 'Saturday' : ',Saturday';
    }
    if ($('#weekday-sun').is(':checked') == true) {
        days += days == "" ? 'Sunday' : ',Sunday';
    }

    var alerts = [];

    $.each($('.alertDiv'), function (index, value) {
        var xxx = $(this).data('xxx');
        var emails = "";

        $.each($('.appendLabel[value="' + xxx + '"] .labelEmail'), function (index, value) {
            //email.push($(this).text());
            //console.log(email.pop());
            emails += $(this).text() + "; ";
        });

        alerts.push($('.validateAlertCB[data-xxx="' + xxx + '"]').val());               //isActive
        alerts.push($('.selectRecieved[data-xxx="' + xxx + '"]').val());                //Received or not recveived
        alerts.push($('.selectHrMinSec[data-xxx="' + xxx + '"]').val());                //After HR
        alerts.push($('.selectLabelHrMinSec[data-xxx="' + xxx + '"]').val());           //After Label
        alerts.push($('.selectSend[data-xxx="' + xxx + '"]').val());                    //Success or failed alert
        alerts.push(emails.slice(0, -2));                                                            //emails
        alerts.push($('.selectEveryHrMinSec[data-xxx="' + xxx + '"]').val());           //every HR
        alerts.push($('.selectEveryLabelHrMinSec[data-xxx="' + xxx + '"]').val());      //every Label   
    });

    var workflowData = JSON.stringify({
        Title: $('#txtFileName').val(),
        OnDays: days,
        TimeFrom: $('#slcHrs').val() + ':' + $('#slcMins').val() + ' ' + $('#slcAMPM').val(),
        TimeTo: $('#slcToHrs').val() + ':' + $('#slcToMins').val() + ' ' + $('#slcToAMPM').val(),
        TimeZone: $('#slcToTimezone').val(),
        SubjectSuccess: $('#txtSuccAlert').val(),
        MessageSuccess: $('#txtSuccAreaMessage').val(),
        SubjectFail: $('#txtFailAlert').val(),
        MessageFail: $('#txtFailAreaMessage').val(),
    });

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        type: "POST",
        url: "DataFeed.aspx/UpdateWorkflow",
        data: "{'data':'" + workflowData + "', location:" + JSON.stringify($('#txtLocation').val()) + ", alerts:" + JSON.stringify(alerts) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            location.reload();
        }, failure: function () {
            console.log("failed");
        }, error: function () {
            console.log("error");
        }
    });
});