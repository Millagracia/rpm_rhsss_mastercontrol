﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="MyWorkflow.aspx.cs" Inherits="RPM_Tool.MyWorkflow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .RPMbackground {
            /*background-image: -webkit-linear-gradient(215deg, white 50%, transparent), url('dist/img/LOGO RPM.png');*/
            background-image: url('dist/img/LOGO RPM.png');
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        .panel-darkblue > .panel-heading {
            color: #fff;
            background-color: #142541;
            border-color: #fff;
        }

        .form-control[readonly] {
            background-color: #f7f7f7;
            opacity: 1;
        }


        .watermark {
            background-image: url('dist/img/LOGO RPM.png');
            background-repeat: no-repeat;
            pointer-events: none;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper dvNotReady RPMbackground">
        <section class="content-header">
        </section>
        <section class="content" style="padding-bottom: 0px">
            <br />
            <br />
            <br />
            <br />
            <br />
        </section>
    </div>

    <div class="content-wrapper dvWorking watermark" style="display: none">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>My Workflow</a></li>
            </ol>
        </section>
        <br />
        <br />





        <section class="content" style="padding-bottom: 0px; background-color: #fff;padding-top:5px">
            <div class="row ">
                <div class="col-lg-12">
                    <div class="panel panel-darkblue">
                        <div class="panel-body dvAppendWorkload" style="height: 438px; background-color: #ecf0f5; overflow: scroll">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h4 class="text-bold"><u>Task Details</u></h4>
                                </div>

                            </div>
                            <br />
                            <div class="row topStyle" style="display:none">
                                <%--<div class="col-lg-1"></div>--%>
                                <div class="col-lg-3 text-right" style="padding-top: 5px">Workflow</div>
                                <div class="col-lg-2">
                                    <input type="text" id="txtWorkflow" class="form-control" readonly="readonly" />
                                </div>
                                <div class="col-lg-2 text-right" style="padding-top: 5px">Action</div>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <%--  <div class="row topStyle">
                               
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-lg-12">
                    <div class="panel panel-darkblue">
                        <div class="panel-body" style="height: 178px; background-color: #ecf0f5">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h4 class="text-bold"><u>Status</u></h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-2" style="padding-top: 5px">
                                </div>
                                <div class="col-lg-1" style="padding-top: 5px">
                                    Disposition
                                </div>
                                <div class="col-lg-2">
                                    <select id="slcDisposition" class=" input-md tb1 form-control">
                                        <option value="0">Select</option>
                                        <option value="Completed">Completed</option>
                                        <option value="In progress">In progress</option>
                                        <option value="Escalated">Escalated</option>
                                    </select>
                                </div>
                                <%-- <div class="col-lg-1" style="padding-top: 5px">
                                </div>--%>
                                <div class="col-lg-2 text-right" style="padding-top: 5px">
                                    Remarks
                                </div>
                                <div class="col-lg-2">
                                    <select id="slcRemark" class=" input-md tb1 form-control">
                                        <option value="0">Select</option>
                                        <option value="Document Required">Document Required</option>
                                        <option value="Application Issues">Application Issues</option>
                                        <option value="Lease Addendum">Lease Addendum</option>
                                    </select>
                                </div>
                                <div class="col-lg-1 text-right">
                                    <button class="btn btn-primary form-control" id="btnSaveWorkload" type="button">Save</button>
                                </div>
                            </div>
                            <div class="row topStyle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
