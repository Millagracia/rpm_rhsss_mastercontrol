﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="UserSettings.aspx.cs" Inherits="RPM_Tool.UserSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .panel-darkblue > .panel-heading {
            color: #fff;
            background-color: #142541;
            border-color: #fff;
        }

        .page-navigation a {
            margin: 0 2px;
            margin: 0 2px;
            background-color: #ffffff; /* Green */
            border: 1px solid #428bca;
            border-radius: 5px;
            color: #428bca;
            padding: 1px 9px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
        }

            .page-navigation a[data-selected] {
                background-color: #428bca;
                color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper ">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Master Control</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;User Settings</span>
                </li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="btn-group btn-group-justified">
                        <a href="#" id="btnAccess" class="btn btn-primary">Role</a>
                        <a href="#" id="btnUser" class="btn btn-default">User</a>
                    </div>
                </div>
            </div>
            <div id="dvAccess">
                <div class="row" style="padding-top: 5px">
                    <div class="col-lg-3" style="padding-right: 2px">
                        <div class="panel panel-default">
                            <div class="panel-heading">Access Type</div>
                        </div>
                    </div>
                    <div class="col-lg-3" style="padding-left: 2px">
                        <div class="panel panel-default">
                            <div class="panel-heading">Current Function<label id="conId" hidden></label></div>

                        </div>
                    </div>
                    <div class="col-lg-1" style="padding-left: 2px">
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Available Functions</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3" style="padding-right: 2px;">
                        <div class="panel panel-default" style="height: 508px">
                            <div class="panel-body">
                                <div>
                                    <table class="table">
                                        <tbody id="tbdy">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center" style="margin-top: 1%;">
                                    <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#modalNewAccess"><i class="fa fa-plus text-green"></i>Add New Access</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3" style="padding-left: 2px">
                        <div class="panel panel-default" style="height: 508px">
                            <div class="panel-body">
                                <div>
                                    <table class="table">
                                        <tbody id="tison">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="row" style="padding-bottom: 166px">
                            <div class="col-lg-12">
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-lg-12">
                                <button type="button" id="appFunc" class="btn btn-link"><i class="fas fa-arrow-alt-circle-left text-primary fa-5x"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" id="remFunc" class="btn btn-link"><i class="fas fa-arrow-alt-circle-right text-primary fa-5x"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default" style="height: 508px">
                            <div class="panel-body">
                                <div>
                                    <table class="table">
                                        <tbody id="tFunc">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvUser" style="display: none">
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-12">
                        <div class="panel panel-body" style="height: 566px">
                            <div class="row">
                                <div class="col-lg-1 txtStyle">
                                    <b>Search:</b>
                                </div>
                                <div class="col-lg-1 txtStyle" style="padding-left: 0px; padding-right: 0px;">
                                    Employee Name:
                                </div>
                                <div class="col-lg-2">
                                    <select id="userSelect" class="form-control input-sm tb1">
                                        <%--    <option value="">All</option>
                                        <option value="true">Atup, Ichel</option>
                                        <option value="false">Blash, Heherson</option>
                                        <option value="false">Cipriano, Russel</option>
                                        <option value="false">Doria, Jameboy</option>
                                        <option value="false">Jacinto, Leo</option>
                                        <option value="false">Mendoza, Jenesse</option>
                                        <option value="false">Millagracia, Jaime</option>
                                        <option value="false">Nunez, Christian</option>
                                        <option value="false">Rudio, Bjorn</option>
                                        <option value="false">Soliman, Joseph</option>--%>
                                    </select>
                                </div>
                                <div class="col-lg-1 txtStyle">
                                    <b>Filter By:</b>
                                </div>
                                <div class="col-lg-2 txtStyle">
                                    Employee Category:
                                </div>
                                <div class="col-lg-2">
                                    <select id="slcEmplevel" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 5px">
                                <div class="col-lg-1 txtStyle">
                                </div>
                                <div class="col-lg-1 txtStyle" style="padding-left: 0px; padding-right: 0px;">
                                </div>
                                <div class="col-lg-2">
                                </div>
                                <div class="col-lg-1 txtStyle">
                                </div>
                                <div class="col-lg-2 txtStyle">
                                    Immediate Supervisor:
                                </div>
                                <div class="col-lg-2">
                                    <select id="slcImmeSupp" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">

                                    <%--   <table id="tblUser" class="table table-bordered table-hover" data-single-select="true" style="display:none">
                                        <thead>
                                            <tr>
                                                <th data-field="id" data-formatter="idFormatter1">
                                                    <input type="checkbox" id="chkRowAll" class="chkRowAll " />
                                                </th>
                                                <th data-sortable="true" data-field="empname">Employee Name</th>
                                                <th data-sortable="true" data-field="emplevel">Employee Category</th>
                                                <th data-sortable="true" data-field="immsupp">Immediate Superior</th>
                                                <th data-field="role_id">Role : 
                                                    <select id="slcRole" class=" input-sm tb1">
                                                        <option value="">Developer</option>
                                                        <option value="true">Manager</option>
                                                        <option value="false">AM</option>
                                                    </select></th>
                                            </tr>
                                        </thead>
                                    </table>--%>
                                    <div class="row topStyle dvUsers">
                                        <table id="tblUserSettings" style="width: 100%" class="table table-bordered table-striped ">
                                            <thead>
                                                <tr>
                                                    <th data-field="Associate">
                                                        <input type="checkbox" id="chkRowAll" class="chkRowAll " /></th>
                                                    <th data-field="ImmediateSupervisor">Employee Name</th>
                                                    <th data-field="Status">Employee Category</th>
                                                    <th data-field="Duration">Immediate Superior</th>
                                                    <th data-field="role_id">Role : 
                                                    <select id="slcRole" class=" input-sm tb1">
                                                        <option value="">Developer</option>
                                                        <option value="true">Manager</option>
                                                        <option value="false">AM</option>
                                                    </select></th>
                                                </tr>
                                            </thead>
                                            <tbody class="dvAppendTableUsers">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="btnSaveRole" class="btn btn-primary btn-radius-custom">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modalNewAccess">
        <div class="modal-dialog" role="document">
            <div class="panel panel-darkblue">
                <div class="panel-heading text-center">

                    <label>New Access Type</label>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">

                        <table class="table">
                            <tr>
                                <td style="width: 20%;">
                                    <label class="inline">Access Name: </label>
                                </td>
                                <td style="width: 80%;">
                                    <input id="accessTypeName" type="text" class="inline form-control" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="inline">Description: </label>
                                </td>
                                <td>
                                    <input id="accessTypeDesc" type="text" class="inline form-control" /></td>
                            </tr>
                        </table>

                    </div>
                    <div class="text-center">
                        <button id="btnNew_Type" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="js/UserSettings.js"></script>
</asp:Content>
