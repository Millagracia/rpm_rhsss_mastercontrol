﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WorkflowMaker.aspx.cs" Inherits="RPM_Tool.WorkflowMaker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <section class="content-header">
             <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Master Control</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;Workflow Maker</span>
                </li>
            </ol>
        </section>
        <br />
        <section class="content">
            <div class="panel panel-body" style="height: 610px">
                <div class="row">
                    <div class="col-lg-1"><b>Filter:</b></div>
                    <div class="col-lg-1">
                        <label id="lblslcText" style="font-weight: normal !important">Workflow</label>
                    </div>
                    <div class="col-lg-2">
                        <select id="slcWorkflow" class="form-control input-sm tb1">
                            <option value="">All</option>
                        </select>
                    </div>
                    <div class="col-lg-1 noPaddingR"><b>Created Date:</b></div>
                    <div class="col-lg-1">From:</div>
                    <div class="col-lg-2">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="dFrom" class="form-control input-sm">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar text-green"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 noPaddingR">
                        <button type="button" id="btnApply" class="btn btn-primary btn-radius-custom form-control">Apply</button>
                    </div>

                </div>
                <div class="row topStyle">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-1">Created By:</div>
                    <div class="col-lg-2">
                        <select id="slcCreatedBy" class="form-control input-sm tb1">
                             
                        </select>
                    </div>
                    <div class="col-lg-1 noPaddingR"></div>
                    <div class="col-lg-1">To:</div>
                    <div class="col-lg-2">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="dTo" class="form-control input-sm">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar text-green"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row topStyle">
                    <div class="col-lg-12" style="overflow:scroll; overflow-x:hidden!important; height:466px" >
                        <table id="tblWorkflow"  class="table table-bordered table-hover" data-page-size="7" data-single-select="true">
                            <thead  style="background-color: #142541; color: #fff">
                                <tr>
                                    <th data-sortable="true" data-field="Workflow">Workflow</th>
                                    <th data-sortable="true" data-field="SubTask" data-formatter="SubTaskMerge">Sub-tasks</th>
                                    <th data-sortable="true" data-field="Created_By">Created By</th>
                                    <th data-sortable="true" data-field="Created_Date" data-formatter="dateFormat">Created Date</th>
                                    <th  data-field="id" data-formatter="edit"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="row topStyle">
                    <div class="col-lg-12">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-link"><i class="fas fa-plus text-green"></i>Add New Workflow</button>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #142541; color: #fff">
                    <button type="button" class="close text-red" style="color: #ff0000" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Workflow</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-4 noPaddingLR">
                            <div class="col-lg-5 txtStyle noPaddingR">
                                Workflow:
                            </div>
                            <div class="col-lg-7">
                                <select id="slcNewWorkflow" class="form-control input-sm tb1">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row topStyle">
                        <div class="col-lg-4 noPaddingLR">
                            <div class="col-lg-5 txtStyle noPaddingR">
                                Queue Method:
                            </div>
                            <div class="col-lg-7 ">
                                <select id="slcQueue" class="form-control input-sm tb1">
                                    <option value="0">Select</option>
                                    <option value="Push">Push</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="dvNewWorkflow">
                        <div class="row topStyle dvManager">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Manager:
                                </div>
                                <div class="col-lg-7">
                                    <select id="slcMGR" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 noPaddingLR dvbtnAddRemove">
                                <div class="col-lg-6 noPaddingLR dvRemoveThis">
                                    <button type="button" class="btn btn-link btnAddManager"><i class="fas fa-plus text-green fa-lg"></i></button>
                                </div>
                                <div class="col-lg-6 noPaddingLR dvRemoveThis">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="dvNewTeamLeader">
                        <div class="row topStyle dvTeamLeader">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Team Leader:
                                </div>
                                <div class="col-lg-7 ">
                                    <select id="slcTL" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 noPaddingLR dvbtnAddRemoveTeamLeader">
                                <div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader">
                                    <button type="button" class="btn btn-link btnAddTeamLeader"><i class="fas fa-plus text-green  fa-lg"></i></button>
                                </div>
                                <div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeader">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dvSubTask">
                        <div class="row topStyle dvAddSubTask">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Sub-task:
                                </div>
                                <div class="col-lg-7">
                                    <select id="slcSubTask" class="form-control input-sm tb1">
                                      <%--  <option value="0">Select</option>
                                        <option value="For Eligibility Review">For Eligibility Review</option>
                                        <option value="For Renewal Letter Sending">For Renewal Letter Sending</option>
                                        <option value="90-day Renewal Letter Notification">90-day Renewal Letter Notification</option>
                                        <option value="60-day Renewal Letter Notification">60-day Renewal Letter Notification</option>
                                        <option value="45-day Renewal Letter Notification">45-day Renewal Letter Notification</option>
                                        <option value="Awaiting Response/Negotiation in progress">Awaiting Response/Negotiation in progress</option>
                                        <option value="For DocuSign Sending">For DocuSign Sending</option>
                                        <option value="For Renewal Follow-up">For Renewal Follow-up</option>--%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 txtStyle text-right">
                                SLA
                            </div>
                            <div class="col-lg-2 noPaddingR">
                                <div class="row">
                                    <div class="col-lg-5 noPaddingLR">
                                        <select id="slcSLAnum" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="26">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-7 noPaddingR" style="padding-left: 2px">
                                        <select id="slcSLAdays" class="form-control input-sm tb1">
                                            <option value="0">Select</option>
                                            <option value="Hour/s">Hour/s</option>
                                            <option value="Day/s">Day/s</option>
                                            <option value="Week/s">Week/s</option>
                                            <option value="Month/s">Month/s</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 " style="padding-left: 17px">
                                <div class="row">
                                    <div class="col-lg-3 noPaddingR" style="padding-left: 29px">
                                        AHT Target
                                     <i>hh:mm:ss</i>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHThr" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHTmm" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                            <option value="60">60</option>

                                        </select>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHTss" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                            <option value="60">60</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 noPaddingLR dvSLAaddRemove" style="padding-left: 3px">
                                        <div class="col-lg-4 noPaddingLR dvRemoveThis">
                                            <button type="button" class="btn btn-link btnNewSLA"><i class="fas fa-plus text-green  fa-lg"></i></button>
                                        </div>
                                        <div class="col-lg-6 noPaddingLR dvRemoveThis">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <button type="button" id="btnSaveWorkflow" class="btn btn-primary ">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalEdit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #142541; color: #fff">
                    <button type="button" class="close text-red" style="color: #ff0000" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Workflow</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-4 noPaddingLR">
                            <div class="col-lg-5 txtStyle noPaddingR">
                                Workflow:
                            </div>
                            <div class="col-lg-7">
                                <select id="slcNewWorkflowEdit" class="form-control input-sm tb1">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row topStyle">
                        <div class="col-lg-4 noPaddingLR">
                            <div class="col-lg-5 txtStyle noPaddingR">
                                Queue Method:
                            </div>
                            <div class="col-lg-7 ">
                                <select id="slcQueueEdit" class="form-control input-sm tb1">
                                    <option value="0">Select</option>
                                    <option value="Push">Push</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="dvNewWorkflowEdit">
                        <div class="row topStyle dvManagerEdit">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Manager:
                                </div>
                                <div class="col-lg-7">
                                    <select id="slcMGREdit" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 noPaddingLR dvbtnAddRemoveEdit">
                                <div class="col-lg-6 noPaddingLR dvRemoveThisEdit">
                                    <button type="button" class="btn btn-link btnAddManagerEdit"><i class="fas fa-plus text-green fa-lg"></i></button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="dvNewTeamLeaderEdit">
                        <div class="row topStyle dvTeamLeaderEdit">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Team Leader:
                                </div>
                                <div class="col-lg-7 ">
                                    <select id="slcTLEdit" class="form-control input-sm tb1">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 noPaddingLR dvbtnAddRemoveTeamLeaderEdit">
                                <div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit">
                                    <button type="button" class="btn btn-link btnAddTeamLeaderEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>
                                </div>
                                <div class="col-lg-6 noPaddingLR dvRemoveThisTeamLeaderEdit">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dvSubTaskEdit">
                        <div class="row topStyle dvAddSubTaskEdit">
                            <div class="col-lg-4 noPaddingLR">
                                <div class="col-lg-5 txtStyle noPaddingR">
                                    Sub-task:
                                </div>
                                <div class="col-lg-7">
                                    <select id="slcSubTaskEdit" class="form-control input-sm tb1">
                                      <%--  <option value="0">Select</option>
                                        <option value="For Eligibility Review">For Eligibility Review</option>
                                        <option value="For Renewal Letter Sending">For Renewal Letter Sending</option>
                                        <option value="90-day Renewal Letter Notification">90-day Renewal Letter Notification</option>
                                        <option value="60-day Renewal Letter Notification">60-day Renewal Letter Notification</option>
                                        <option value="45-day Renewal Letter Notification">45-day Renewal Letter Notification</option>
                                        <option value="Awaiting Response/Negotiation in progress">Awaiting Response/Negotiation in progress</option>
                                        <option value="For DocuSign Sending">For DocuSign Sending</option>
                                        <option value="For Renewal Follow-up">For Renewal Follow-up</option>--%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 txtStyle text-right">
                                SLA
                            </div>
                            <div class="col-lg-2 noPaddingR">
                                <div class="row">
                                    <div class="col-lg-5 noPaddingLR">
                                        <select id="slcSLAnumEdit" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="26">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-7 noPaddingR" style="padding-left: 2px">
                                        <select id="slcSLAdaysEdit" class="form-control input-sm tb1">
                                            <option value="0">Select</option>
                                            <option value="Hour/s">Hour/s</option>
                                            <option value="Day/s">Day/s</option>
                                            <option value="Week/s">Week/s</option>
                                            <option value="Month/s">Month/s</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 " style="padding-left: 17px">
                                <div class="row">
                                    <div class="col-lg-3 noPaddingR" style="padding-left: 29px">
                                        AHT Target
                                     <i>hh:mm:ss</i>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHThrEdit" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHTmmEdit" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                            <option value="60">60</option>

                                        </select>
                                    </div>
                                    <div class="col-lg-2 noPaddingR" style="padding-left: 3px">
                                        <select id="slcAHTssEdit" class="form-control input-sm tb1">
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                            <option value="60">60</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 noPaddingLR dvSLAaddRemoveEdit" style="padding-left: 3px">
                                        <div class="col-lg-4 noPaddingLR dvRemoveThisEdit">
                                            <button type="button" class="btn btn-link btnNewSLAEdit"><i class="fas fa-plus text-green  fa-lg"></i></button>
                                        </div>
                                        <div class="col-lg-6 noPaddingLR dvRemoveThisEdit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <button type="button" id="btnSaveWorkflowEdit" class="btn btn-primary ">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="JS/Workflow.js"></script>
</asp:Content>
