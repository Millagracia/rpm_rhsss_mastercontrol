﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataFeed.aspx.cs" Inherits="RPM_Tool.DataFeed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .weekDays-selector input {
            display: none !important;
        }

            .weekDays-selector input[type=checkbox] + label {
                display: inline-block;
                border-radius: 6px;
                background: #dddddd;
                height: 40px;
                width: 30px;
                margin-right: 3px;
                line-height: 40px;
                text-align: center;
                cursor: pointer;
            }

            .weekDays-selector input[type=checkbox]:checked + label {
                background: #2AD705;
                color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper dvNotReady RPMbackground">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Workflow Feeder</a></li>

            </ol>
        </section>
        <section class="content" style="padding-bottom: 0px">
            <div class="row" style="display: none;">
                <div class="col-lg-3">
                    <div class="btn-group btn-group-justified">
                        <a href="#" id="btnHistory" class="btn btn-primary">History</a>
                        <a href="#" id="btnDataFeed" class="btn btn-default">Data Feed</a>
                    </div>
                </div>
            </div>
            <div class="dvHistory topStyle" style="display: none;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-body" style="height: 581px; overflow: scroll">
                            <div class="row topStyle">
                                <div class="col-lg-1">
                                    <label>Filter:</label>
                                </div>
                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            Workflow
                                        </div>
                                        <div class="col-lg-8">
                                            <select id="slcHistoryWorkflow" class=" input-sm tb1 form-control">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                    <label>Created:</label>
                                </div>
                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            From
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="txtFromDate" class="form-control input-sm">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar text-green"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <button type="button" id="btnApply" class="btn btn-primary">Apply</button>
                                </div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            Created By
                                        </div>
                                        <div class="col-lg-8">
                                            <select id="slcCreatedBy" class=" input-sm tb1 form-control">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            To
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="txtToDate" class="form-control input-sm">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar text-green"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-10">
                                    <table id="tblHistory" class="table table-bordered table-hover" data-single-select="true">
                                        <thead style="background-color: #142541; color: #fff">
                                            <tr>
                                                <th data-field="Name">FileName</th>
                                                <th data-field="CreatedBy">Created By</th>
                                                <th data-field="CreatedDate" data-formatter="dateFormat">Created Date</th>
                                                <th data-formatter="edit" data-field="id"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col-lg-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dvDatafeed topStyle">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-body" style="overflow-x: hidden; overflow-y: scroll; padding: 0; height: 554px;">
                            <div class="row">
                                <div class="col-lg-1 noPaddingR"> 
                                </div>
                                <div class="col-lg-1">
                                    Title/Filename:
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" id="txtFileName" class="form-control editDataFeed validateSave" />
                                </div>
                                <div class="col-lg-1 noPaddingR">
                                </div>
                                <div class="col-lg-1 noPaddingR">
                                    On Days
                                </div>
                                <div class="col-lg-3 noPaddingR">
                                    <div class="weekDays-selector">
                                        <input type="checkbox" id="weekday-mon" class="weekday Monday editDataFeed validateSaveDay" />
                                        <label for="weekday-mon">M</label>
                                        <input type="checkbox" id="weekday-tue" class="weekday Tuesday editDataFeed validateSaveDay" />
                                        <label for="weekday-tue">T</label>
                                        <input type="checkbox" id="weekday-wed" class="weekday Wednesday editDataFeed validateSaveDay" />
                                        <label for="weekday-wed">W</label>
                                        <input type="checkbox" id="weekday-thu" class="weekday Thursday editDataFeed validateSaveDay" />
                                        <label for="weekday-thu">TH</label>
                                        <input type="checkbox" id="weekday-fri" class="weekday Friday editDataFeed validateSaveDay" />
                                        <label for="weekday-fri">F</label>
                                        <input type="checkbox" id="weekday-sat" class="weekday Saturday editDataFeed validateSaveDay" />
                                        <label for="weekday-sat">S</label>
                                        <input type="checkbox" id="weekday-sun" class="weekday Sunday editDataFeed validateSaveDay" />
                                        <label for="weekday-sun">SU</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                    At Location
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" id="txtLocation" class="form-control editDataFeed validateSave" />
                                    <select id="slcWorkflow" class="form-control input-sm tb1" style="display: none">
                                    </select>
                                </div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-2">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                    At Time
                                </div>
                                <div class="col-lg-2" style="padding-left: 2px; padding-right: 2px">
                                    <div class="row">
                                        <div class="col-lg-3 " style="padding-left: 2px; padding-right: 2px">
                                            <select id="slcHrs" class=" input-sm tb1 form-control editDataFeed hoursDropdown validateSave">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 " style="padding-left: 2px; padding-right: 2px">
                                            <select id="slcMins" class=" input-sm tb1 form-control editDataFeed minutesDropdown">
                                            </select>
                                        </div>
                                        <div class="col-lg-4 " style="padding-left: 2px; padding-right: 2px;">
                                            <select id="slcAMPM" class="input-sm tb1 form-control editDataFeed validateSave">
                                                <option value="AM/PM">AM/PM</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3" style="padding-left: 2px; padding-right: 2px">
                                    <div class="row" style="display: none">
                                        <div class="col-lg-4 noPaddingL">
                                            <select id="slcTimezone" class=" input-sm tb1 form-control editDataFeed" style="display: none">
                                                <option value="00">EST</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row topStyle">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-2">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                    to
                                </div>
                                <div class="col-lg-2" style="padding-left: 2px; padding-right: 2px">
                                    <div class="row">
                                        <div class="col-lg-3 " style="padding-left: 2px; padding-right: 2px">
                                            <select id="slcToHrs" class=" input-sm tb1 form-control editDataFeed hoursDropdown validateSave">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 " style="padding-left: 2px; padding-right: 2px">
                                            <select id="slcToMins" class=" input-sm tb1 form-control editDataFeed minutesDropdown">
                                            </select>
                                        </div>
                                        <div class="col-lg-4 " style="padding-left: 2px; padding-right: 2px">
                                            <select id="slcToAMPM" class=" input-sm tb1 form-control editDataFeed validateSave">
                                                <option value="AM/PM">AM/PM</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3" style="padding-left: 2px; padding-right: 2px">
                                    <div class="row" style="display: none">
                                        <div class="col-lg-4 noPaddingL">
                                            <select id="slcToTimezone" class=" input-sm tb1 form-control editDataFeed validateSave">
                                                <option value="00">EST</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-1">
                                    <label>Alerts</label>
                                </div>
                                <div class="col-lg-7"></div>
                                <div class="col-lg-1">
                                    <button type="button" class="btn btn-link editDataFeed" data-toggle="modal" data-target="#myModal" id="btnAlert">Alert Messages</button>
                                </div>
                            </div>
                            <br />
                            <div class="dvAlerts">
                                <div class="alertDiv" data-xxx="x">
                                    <%--TOP--%>
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-1">
                                            <div class="col-lg-3 noPaddingR" style="padding-top: 4px">
                                                <input type="checkbox" id="cbIsActive" class="editDataFeed validateAlertCB" data-xxx="x" />
                                            </div>
                                            <div class="col-lg-9 noPaddingR" style="padding-top: 4px">
                                                If file is
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="col-lg-8" style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcReceived" class=" input-sm tb1 form-control selectRecieved editDataFeed" data-xxx="x">
                                                    <option value="Received">Received</option>
                                                    <option value="Not Received">Not Received</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 text-center" style="padding-left: 2px; padding-right: 2px; padding-top: 4px">After</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="col-lg-5" style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcHrMinSec" class=" input-sm tb1 form-control selectHrMinSec minutesDropdown" data-xxx="x">
                                                </select>
                                            </div>
                                            <div class="col-lg-7" style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcMHDWM" class=" input-sm tb1 form-control selectLabelHrMinSec" data-xxx="x">
                                                    <option value="00">Select</option>
                                                    <option value="minute(s)">minute(s)</option>
                                                    <option value="hour(s)">hour(s)</option>
                                                    <option value="day(s)">day(s)</option>
                                                    <option value="week(s)">week(s)</option>
                                                    <option value="month(s)">month(s)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="col-lg-4 text-center" style="padding-left: 2px; padding-right: 2px; padding-top: 4px">then send</div>
                                            <div class="col-lg-8" style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcSuccess" class=" input-sm tb1 form-control editDataFeed selectSend" data-xxx="x">
                                                    <option value="Success Alert">Success Alert</option>
                                                    <option value="Failure Alert">Failure Alert</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-2">
                                            <button id="btnAddAppend" class="btn btn-link editDataFeed" type="button" value="x" data-xxx="x"><i class="fas fa-plus text-green"></i></button>
                                            <button id="btnRemoveAppend" class="btn btn-link editDataFeed" type="button" style="display: none;" value="x"><i class="fas fa-trash-alt text-red"></i></button>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                    <%--BOTTOM--%>
                                    <div class="row">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-1">
                                            <div class="col-lg-7 noPaddingR"></div>
                                            <div class="col-lg-1 noPaddingR" style="padding-top: 4px">to</div>
                                            <div class="col-lg-3 noPaddingR"></div>
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:TextBox ID="txtTo" ClientIDMode="Static" class="form-control txtLabel autocom" runat="server" data-xxx="x"></asp:TextBox>
                                            <label id="lblToAppend"></label>
                                            <div class="appendLabel" style="width: 250px;" value="x"></div>
                                        </div>
                                        <div class="col-lg-1 noPaddingLR">
                                            <div class="col-lg-4 noPaddingR">
                                                <button id="btnAddTo" class="btn btn-link btnGetTo" type="button" data-xxx="x"><i class="fas fa-plus text-green"></i></button>
                                            </div>
                                            <div class="col-lg-2 noPaddingR " style="padding-top: 4px">every</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="col-lg-5 " style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcForHrMinSec" class=" input-sm tb1 form-control selectEveryHrMinSec minutesDropdown" data-xxx="x">
                                                </select>
                                            </div>
                                            <div class="col-lg-7 " style="padding-left: 2px; padding-right: 2px">
                                                <select id="slcForMHDWM" class=" input-sm tb1 form-control selectEveryLabelHrMinSec" data-xxx="x">
                                                    <option value="00">Select</option>
                                                    <option value="minute(s)">minute(s)</option>
                                                    <option value="hour(s)">hour(s)</option>
                                                    <option value="day(s)">day(s)</option>
                                                    <option value="week(s)">week(s)</option>
                                                    <option value="month(s)">month(s)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="6"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-footer">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="btnEdit" class="btn btn-primary" style="display: none;">Edit</button>
                                    <button type="button" id="btnSave" class="btn btn-primary">Save</button>
                                    <button type="button" id="btnUpdate" class="btn btn-primary" style="display: none;" disabled="disabled">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #142541; color: #fff">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Alert Message</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <label>Success Alert</label>
                        </div>
                        <div class="col-lg-2">
                            <label>Fail Alert</label>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Subject :
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control validateSaveMessage" id="txtSuccAlert" />
                        </div>
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Subject :
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control validateSaveMessage" id="txtFailAlert" />
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Message :
                        </div>
                        <div class="col-lg-4">
                            <textarea class="form-control validateSaveMessage" rows="5" id="txtSuccAreaMessage"></textarea>
                        </div>
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Message :
                        </div>
                        <div class="col-lg-4">
                            <textarea class="form-control validateSaveMessage" rows="5" id="txtFailAreaMessage"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer " style="text-align: center">
                    <button type="button" class="btn btn-primary" id="btnOkay" data-dismiss="modal">Okay</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="JS/DataFeed.js"></script>
</asp:Content>
