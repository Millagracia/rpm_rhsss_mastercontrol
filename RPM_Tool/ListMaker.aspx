﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ListMaker.aspx.cs" Inherits="RPM_Tool.ListMaker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .panel-darkblue > .panel-heading {
            color: #fff;
            background-color: #142541;
            border-color: #fff;
        }

        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Master Control</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;List Maker</span>
                </li>
            </ol>
        </section>
        <section class="content" style="padding-bottom: 0px">
            <div class="row">
                <div class="col-lg-3">
                    <%-- <div class="btn-group btn-group-justified">
                        <a href="#" class="btn btn-primary">List</a>
                        <a href="#" class="btn btn-default">My Workflow Fields</a>
                    </div>--%>
                    <div class="btn-group">
                        <button type="button" id="btnList" style="width: 118px" class="btn btn-primary ">List</button>
                        <div class="btn-group ">
                            <button type="button" id="btnWorkflow" class="btn btn-default dropdown-toggle  " data-toggle="dropdown">
                                My Workflow Fields <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#" id="btnFiled">Field Selection</a></li>
                                <li><a href="#" id="btnRepository">Repository</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="row" id="dvList">
                <div class="col-lg-4">
                    <div class="panel panel-darkblue">
                        <div class="panel-heading text-center">Workflow</div>
                        <div class=" panel-body" style="height: 528px;">
                            <div class="dvListAppend" style="display: none;">
                                <%--<div class="row dvListTitle"  style="display: none">
                                    <div class="col-lg-5 text-center"></div>
                                    <div class="col-lg-3 text-left" style="padding-left: 0px; padding-top: 0px">
                                        <button type="button"  class="btn btn-link btnListTitle"></button>
                                    </div>
                                    <div class="col-lg-2 noPaddingL text-right">
                                        <button type="button" class="btn btn-default btn-sm" style="background-color: transparent"><i class="fas fa-pencil-alt fa-lg"></i></button>
                                    </div>
                                    <div class="col-lg-2 noPaddingLR">
                                        <button type="button" class="btn btn-default btn-sm btnDeleteList" style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-lg"></i></button>
                                    </div>
                                </div>--%>
                            </div>
                            <div class="row topStyle" id="dvAddNewList" style="display: none">
                                <div class="col-lg-8 text-left">
                                    <%--<input id="txtListTitle" type="text" class="form-control" />--%>
                                    <select id="slcDropdown" class=" input-sm tb1 form-control">
                                    </select>
                                </div>
                                <div class="col-lg-4  text-right noPaddingL">
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnSaveList" class="btn btn-default btn-sm " style="background-color: transparent"><i class="far fa-save fa-lg"></i></button>
                                    </div>
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnCancelList" class="btn btn-default btn-sm " style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-lg"></i></button>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center" style="display: none">
                                    <button type="button" id="btnAddNewList" class="btn btn-link btn-sm "><i class="fas fa-plus text-green"></i>Add New List</button>
                                </div>
                            </div>

                            <div class="dvAppendWorkflowValue">
                                <div class="row dvRemoveWorkflowValue">
                                    <div class="col-lg-3 text-center ">
                                    </div>
                                    <div class="col-lg-4 text-left ">
                                        <label>Renewal</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" id="btnDeleteWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-sm"></i></button>
                                    </div>
                                </div>
                            </div>



                            <div class="row topStyle" id="dvAddNewWorkflow" style="display: none">
                                <div class="col-lg-8 text-left">
                                    <input id="txtWorkflow" type="text" class="form-control" />
                                </div>
                                <div class="col-lg-4  text-right noPaddingL">
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnSaveWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="far fa-save fa-lg"></i></button>
                                    </div>
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnCancelWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-lg"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="btnAddNewWorkflow" class="btn btn-link btn-sm "><i class="fas fa-plus text-green"></i>Add New List</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="panel panel-darkblue">
                        <div class="panel-heading text-center">Tasks</div>
                        <div class=" panel-body" style="height: 528px; overflow-y: scroll" id="dvListOption">
                            <div class="row dvappendAddRemove" style="display: none;">
                                <div class="col-lg-3 "></div>
                                <div class="col-lg-1 text-right" style="padding-top: 3px">1</div>
                                <div class="col-lg-3">
                                    <input type="text" id="txtListOption" class="form-control" />
                                </div>
                                <div class="col-lg-1 dvListOption">
                                    <button type="button" class="btn btn-default btn-sm form-control btnAdd" style="background-color: transparent"><i class="fas fa-plus text-green  fa-lg"></i></button>
                                </div>
                                <div class="col-lg-1 dvListOption noPaddingL">
                                </div>
                            </div>
                            <div class="dvAppendSubWorkflowValue">
                                <%--  <div class="row dvRemoveSubWorkflowValue">
                                    <div class="col-lg-3 text-center ">
                                    </div>
                                    <div class="col-lg-2 text-left ">
                                        <label>Renewal</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" id="btnDeleteSubWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-sm"></i></button>
                                    </div>
                                </div>--%>
                            </div>
                            <div class="row topStyle" id="dvAddNewSubWorkflow" style="display: none">
                                <div class="col-lg-2 text-left">
                                </div>
                                <div class="col-lg-4 text-left">
                                    <input id="txtSubWorkflow" type="text" class="form-control" />
                                </div>
                                <div class="col-lg-2  text-right noPaddingL">
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnSaveSubWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="far fa-save fa-lg"></i></button>
                                    </div>
                                    <div class="col-lg-5 noPaddingLR">
                                        <button type="button" id="btnCancelSubWorkflow" class="btn btn-default btn-sm " style="background-color: transparent"><i class="fas fa-trash-alt text-red fa-lg"></i></button>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-left">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="btnAddNewSubWorkflow" class="btn btn-link btn-sm "><i class="fas fa-plus text-green"></i>Add New List</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center" style="background-color: #fff; border-color: #fff; display: none">
                            <button id="btnSaveListOption" type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvFieldSelection" style="display: none">
                <div class="row" style="display: none">

                    <div class="col-lg-1 text-right">Upload File</div>
                    <div class="col-lg-3">
                        <input type="file" data-show-preview="false" id="fileUpload" data-allowed-file-extensions='["xlsx"]' />
                    </div>
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-4 text-right">
                                File Name
                            </div>
                            <div class="col-lg-6">
                                <label id="lblFileName" style="font-style: normal !important; font-size: 16px"></label>
                            </div>
                        </div>
                        <div class="row topStyle">
                            <div class="col-lg-4 text-right">
                                Sheet Name
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="usr">
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2">
                        <button type="button" class="form-control btn btn-primary" id="btnGetHeaders">Get Headers</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-1 text-right " style="padding-top: 4px; display: none">Workflow</div>
                    <div class="col-lg-2" style="display: none">
                        <select id="slcWorkflowHead" class="form-control input-sm tb1">
                        </select>
                    </div>
                </div>
                <div class="row" style="padding-top: 5px">
                    <div class="col-lg-3">
                        <div class="panel panel-darkblue">
                            <div class="panel-heading text-center">Choose Headers</div>
                            <div class=" panel-body dvHeaders" style="height: 578px; overflow: scroll">
                                <div class="row">
                                    <div class="col-lg-12 topStyle">
                                        <button type="button" class="btn btn-primary form-control" data-toggle="collapse" data-target="#HeadersBuilding">Buildings</button>

                                        <div class="dvHeadersBuilding collapse out" id="HeadersBuilding">
                                            <div class="search-container topStyle">
                                                <form>
                                                    <input type="text" id="txtSearchBuildings" name="search" class="form-control "  placeholder="Search.."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 topStyle">
                                        <button type="button" class="btn btn-primary form-control" data-toggle="collapse" data-target="#HeadersLeases">Leases</button>
                                        <div class="dvHeadersLeases collapse out" id="HeadersLeases">
                                               <div class="search-container topStyle">
                                                <form>
                                                    <input type="text" id="txtSearchLeases" name="search" class="form-control "  placeholder="Search.."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 topStyle">
                                        <button type="button" class="btn btn-primary form-control" data-toggle="collapse" data-target="#HeadersRRI">RRI</button>
                                        <div class="dvHeadersRRI collapse out" id="HeadersRRI">
                                               <div class="search-container topStyle">
                                                <form>
                                                    <input type="text" id="txtSearchRRI" name="search" class="form-control "  placeholder="Search.."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="row" style="padding-bottom: 166px">
                            <div class="col-lg-12">
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-link" id="btnRight"><i class="fas fa-arrow-alt-circle-right text-primary fa-5x"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-link" id="btnLeft"><i class="fas fa-arrow-alt-circle-left text-primary fa-5x"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-darkblue">
                            <div class="panel-heading text-center">Choose Headers</div>
                            <div class=" panel-body dvGetHeaders" style="height: 528px; overflow: scroll">

                                <div class="row">
                                    <div class="col-lg-5">
                                        <label>Buildings</label>
                                        <div class="dvGetHeadersBuildings">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Leases</label>
                                        <div class="dvGetHeadersLeases">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>RRI</label>
                                        <div class="dvGetHeadersRRI">
                                        </div>
                                    </div>
                                </div>




                            </div>
                            <div class="panel-footer" style="height: 50px">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-1 topStyle">
                                        Workflow
                                    </div>
                                    <div class="col-lg-3">
                                        <select id="slcWorkflow" class="form-control input-sm tb1">
                                        </select>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="dvSaveBtn">
                                            <button id="btnSaveHeaders" class="btn btn-primary" type="button">Save</button>
                                        </div>
                                        <div class="dvUPdateBtn" style="display: none">
                                            <button id="btnUpdateHeaders" class="btn btn-primary" type="button">Update</button>
                                            <button id="btnCancelHeaders" class="btn btn-primary" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvRepository" style="display: none">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-darkblue">
                            <div class="panel-heading text-center">Repository</div>
                            <div class=" panel-body" style="height: 528px">
                                <table id="tblRepository" class="table table-bordered table-hover" data-single-select="true">
                                    <thead style="background-color: #142541; color: #fff">
                                        <tr>
                                            <th data-sortable="true" data-field="Workflow" data-formatter="Headers">Workflow</th>
                                            <th data-sortable="true" data-field="Last_Modified">Last Modified By</th>
                                            <th data-sortable="true" data-field="Last_Modified_DateAndTime" data-formatter="dateFormat">Last Modified Date and Time</th>
                                            <th data-field="id" data-formatter="edit"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-darkblue">
                            <%--dvDisplayViewHeaders--%>
                            <div class="panel-heading text-center">Headers</div>
                            <div class=" panel-body " style="height: 528px; overflow: scroll">

                                <label>Buildings</label>
                                <div class="dvDisplayViewHeadersBuilding">
                                </div>
                                <label>Leases</label>
                                <div class="dvDisplayViewHeadersLeases">
                                </div>
                                <label>RRI</label>
                                <div class="dvDisplayViewHeadersRRI">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="JS/ListMaker.js"></script>
</asp:Content>
