﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Collections;


namespace RPM_Tool
{
    public partial class BusinessPrioritization : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "4")
                    {
                        idVal = "4";
                    }
                }

                if (idVal == "4")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }

        [WebMethod]
        public static string ddlValues()
        {
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMddlValues";
            dtSet = cls.GetDataSet(qry);
            dt = dtSet.Tables[0];
            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                }
            });
        }
        [WebMethod]
        public static string ddlValuesVal()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();

            DataSet dtSet = new DataSet();
            dtSet = cls.GetDataSet("EXEC spRPMddlValues");

            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            dt3 = dtSet.Tables[3];
            dt4 = dtSet.Tables[4];
            dt5 = dtSet.Tables[5];

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    asd1 = dt1,
                    asd2 = dt2,
                    asd3 = dt3,
                    asd4 = dt4,
                    asd5 = dt5
                }
            });
        }

        [WebMethod]
        public static string ddlValuesSubWorkflow(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();

            DataSet dtSet = new DataSet();
            dtSet = cls.GetDataSet("EXEC spRPMddlValues");
            string qry = @"Select Sublist_Option from tbl_RPM_ListTitle_SubOption where id = 
                            (Select Workflow_ID From tbl_RPM_ListTitle_Option Where List_Option = '" + workflow + "')";
            // dt = cls.GetData(qry);
            dt = dtSet.Tables[0];
            dt1 = dtSet.Tables[1];
            dt2 = dtSet.Tables[2];
            dt3 = dtSet.Tables[3];
            dt4 = cls.GetData(qry);
            dt5 = dtSet.Tables[5];

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    asd1 = dt1,
                    asd2 = dt2,
                    asd3 = dt3,
                    asd4 = dt4,
                    asd5 = dt5
                }
            });
        }
        [WebMethod]
        public static string ddlStateList()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMddlStateList";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                }
            });
        }

        [WebMethod]
        public static string SaveBusinessPrio(string Priority, string Workflow, string State, string EffectiveFor, string EffectiveFrom, string EffectiveTo)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "";

            if (EffectiveFor == "" || EffectiveFor == null)
            {
                //                qry = @"Insert Into tbl_RPM_BusinessPrioritization (Priority,Workflow,State,Effective_For,Effective_From,Effective_To,LastModified_Date,LastModified_By) 
                //                            values ('" + Priority + "','" + Workflow + "','" + State + "','','From " + EffectiveFrom + "','To " + EffectiveTo +
                //                                       "',GetDate(),'" + HttpContext.Current.Session["ntid"] + "')";
                qry = "EXEC spRPMSaveBusinessPrio '" + Priority + "','" + Workflow + "','" + State + "','','From " + EffectiveFrom + "','To " + EffectiveTo + "','" +
                    HttpContext.Current.Session["ntid"] + "'";


            }
            else
            {
                //                qry = @"Insert Into tbl_RPM_BusinessPrioritization (Priority,Workflow,State,Effective_for,Effective_From,Effective_To,LastModified_Date,LastModified_By) 
                //                            values ('" + Priority + "','" + Workflow + "','" + State + "','For " + EffectiveFor + "','','To " + EffectiveTo +
                //                                       "',GetDate(),'" + HttpContext.Current.Session["ntid"] + "')";
                qry = "EXEC spRPMSaveBusinessPrio '" + Priority + "','" + Workflow + "','" + State + "','For " + EffectiveFor + "','','To " + EffectiveTo + "','" +
                 HttpContext.Current.Session["ntid"] + "'";
            }
            cls.ExecuteQuery(qry);
            return "True";
        }
        [WebMethod]
        public static string displayBusinessPrio()
        {
          

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMdisplayBusinessPrio";

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string GetSettingsPrio()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMGetSettingsPrio";

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string editSettings(string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMeditSettings " + id;

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }



         [WebMethod]
        public static string gettime()
        {
            var timeUtc = DateTime.UtcNow;
            var easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var today = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            string strFormat = "MM/d/yyyy/hh:mm:ss";
            string pdfDate = String.Format("{0:" + strFormat + "}", today);
            var id = "";
            var efor = "";
            var efrom = "";
            var eto = "";
            var idbus = "";

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMdisplayBusinessPrio";

            dt = cls.GetData(qry);
            foreach (DataRow drow in dt.Rows)
            {

                efor = drow["Effective_For"].ToString();
                efrom = drow["Effective_From"].ToString();
                id = drow["id"].ToString();
                if (efrom =="")
                {
                    string qrylogic = "Select * from tbl_RPM_BusinessPrioritization where ";   

                }

            };

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }


        [WebMethod]
        public static string gettime2()
        {
            var timeUtc = DateTime.UtcNow;
            var easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var today = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            string strFormat = "MM/d/yyyy/hh:mm:ss";
            string pdfDate = String.Format("{0:" + strFormat + "}", today);
            var id = "";
            var efor = "";
            var efrom = "";
            var eto = "";
            var idbus = "";
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = @"EXEC spRPMdisplayBusinessPrio";
            //string qry = "Select id,Priority, LastModified_By,LastModified_Date,Effective_For,Effective_From ,Effective_To from tbl_RPM_BusinessPrioritization";
            dt = cls.GetData(qry);

            foreach (DataRow drow in dt.Rows)
            {

                efor = drow["Effective_For"].ToString();
                id = drow["id"].ToString();
              
            };



            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UpdateBusinessPrio(string Priority, string Workflow, string State, string EffectiveFor, string EffectiveFrom, string EffectiveTo, string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "";

            if (EffectiveFor == "" || EffectiveFor == null)
            {
                //qry = @"Update tbl_RPM_BusinessPrioritization Set Priority = '" + Priority + "',Workflow = '" + Workflow + "',State = '" + State +
                //                        "',Effective_For = '',Effective_From = 'From " + EffectiveFrom + "',Effective_To = 'To " + EffectiveTo +
                //                       "', LastModified_Date = GetDate(),LastModified_By = '" + HttpContext.Current.Session["ntid"] + "'  where id= " + id;
                qry = "EXEC spRPMUpdateBusinessPrio '" + Priority + "','" + Workflow + "','" + State + "','','From " +
                                                                 EffectiveFrom + "','To " + EffectiveTo + "','" + id + "','" + HttpContext.Current.Session["ntid"] + "'";
            }
            else
            {
                //qry = @"Update tbl_RPM_BusinessPrioritization Set Priority = '" + Priority + "', Workflow = '" + Workflow + "', State = '" + State +
                //                            "',Effective_for = 'For " + EffectiveFor + "',Effective_From = '',Effective_To = 'To " + EffectiveTo +
                //                       "',LastModified_Date = GetDate(),LastModified_By = '" + HttpContext.Current.Session["ntid"] + "'  where id= " + id;
                qry = "EXEC spRPMUpdateBusinessPrio '" + Priority + "','" + Workflow + "','" + State + "','For " + EffectiveFor + "','','To " + EffectiveTo + "','" + 
                    id + "','" + HttpContext.Current.Session["ntid"] + "'";
            }
            cls.ExecuteQuery(qry);
            return "True";
        }

        [WebMethod]
        public static string DeleteBusinessPrior(string id)
        {
            clsConnection cls = new clsConnection();
            string qry = @"Delete tbl_RPM_BusinessPrioritization where id =" + id;
            cls.ExecuteQuery(qry);
            return "True";
        }



    }
}