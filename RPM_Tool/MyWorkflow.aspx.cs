﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;

namespace RPM_Tool
{
    public partial class MyWorkflow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "6")
                    {
                        idVal = "6";
                    }
                }

                if (idVal == "6")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }

        [WebMethod]
        public static string GetWorkloadHeaders()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dtGetWorkflow = new DataTable();
            DataTable dtGetSub = new DataTable();
            DataTable dtTable = new DataTable();
            List<string> arrGet = new List<string> { };
            string[] arr;
            string strTable = "";
            string qryGetWorkflow = @"Select Workflow from tbl_RPM_AgentTask_Manager where ntid = '" + HttpContext.Current.Session["ntid"] + "'";
            string qryGetSubTask = @"Select SubTask from tbl_RPM_SubTask where ntid = '" + HttpContext.Current.Session["ntid"] + "'";
            string qryTable = "";
            string headersVal = "";
            string valWorkable = "";
            dtGetWorkflow = cls.GetData(qryGetWorkflow);
            dtGetSub = cls.GetData(qryGetSubTask);
            string[] arrSubTask = dtGetSub.Rows[0][0].ToString().Split(',');
            arr = dtGetWorkflow.Rows[0][0].ToString().Split(',');

            for (int iG = 0; iG < arr.Length; iG++)
            {
                qryTable = @"Declare @strtable int 
                                set @strtable = (select  count(Distinct Workflow) from tbl_RPM_Workable_Buildings where Workflow = '" + arr[iG] + @"')
                                if(@strtable Like 0)
                                begin 
                                set @strtable = (select  count(Distinct Workflow) from tbl_RPM_Workable_Leases where Workflow = '" + arr[iG] + @"')
			                                if(@strtable Like 0)
			                                begin
			                                set @strtable = (select  count(Distinct Workflow) from tbl_RPM_Workable_RRI where Workflow = '" + arr[iG] + @"')
						                                if(@strtable Like 0)
						                                begin
								                                Select 'None'
						                                end
						                                Else
						                                Begin
							                                Select 'tbl_RPM_Workable_RRI'
						                                End	
			                                end
			                                Else
			                                Begin
				                                Select 'tbl_RPM_Workable_Leases'
			                                End
                                End
                                Else
                                Begin
	                                Select 'tbl_RPM_Workable_Buildings'
                                End";
                dtTable = cls.GetData(qryTable);

                if (dtTable.Rows[0][0].ToString() != "None")
                {
                    try
                    {
                        string qry = "";
                        for (int ii = 0; ii < arr.Length; ii++)
                        {
                            for (int iii = 0; iii < arrSubTask.Length; iii++)
                            {
                                string table = "";
                                if (dtTable.Rows[0][0].ToString() == "tbl_RPM_Workable_Buildings")
                                {
                                    table = "Buildings";
                                }
                                else if (dtTable.Rows[0][0].ToString() == "tbl_RPM_Workable_Leases")
                                {
                                    table = "Leases";
                                }
                                else
                                {
                                    table = "RRI";
                                }


                                qry = @"DECLARE @strlocktoid int
                    set @strlocktoid = 
                    (Select Top 1 id from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                           @"' and not id in(Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = '" + table +
                           "' and workflow_id is not null) and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"')
                    if @strlocktoid is null
                    Begin
                    Update " + dtTable.Rows[0][0].ToString() + @" set lockto = '" + HttpContext.Current.Session["ntid"] +
                           @"' where id = (Select Top 1 id from " + dtTable.Rows[0][0].ToString() + @" where not id in (Select workflow_id from tbl_RPM_Agents_Workload 
                    where workflow_table = '" + table + @"'  and workflow_id is not null)
                      and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"')
                    Select Top 1 Workflow
                    from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                           @"' and not id in (Select workflow_id from tbl_RPM_Agents_Workload where workflow_table = '" + table +
                           @"' and workflow_id is not null) and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"'
                    END
                    ELSE
                    Select Top 1 Workflow
                    from " + dtTable.Rows[0][0].ToString() + @" where id = @strlocktoid";
                                dt = cls.GetData(qry);

                                if (dt.Rows.Count == 0)
                                {

                                }
                                else
                                {
                                    break;
                                }
                            }
                            if (dt.Rows.Count == 0)
                            {

                            }
                            else
                            {
                                break;
                            }


                        }


                        if (dt.Rows.Count == 0)
                        {
                            for (int ii = 0; ii < arr.Length; ii++)
                            {
                                for (int iii = 0; iii < arrSubTask.Length; iii++)
                                {
                                    qry = @"DECLARE @strlocktoid int
                        set @strlocktoid = 
                        (Select Top 1 id from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                            @"' and not id in(Select workflow_id from tbl_RPM_Agents_Workload Where workflow_id is not null) 
                        and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"')
                        if @strlocktoid is null
                        Begin
                        Update " + dtTable.Rows[0][0].ToString() + @" set lockto = '" + HttpContext.Current.Session["ntid"] +
                            @"' where id = (Select Top 1 id from " + dtTable.Rows[0][0].ToString() + @" where not id in (Select workflow_id from tbl_RPM_Agents_Workload)
                        and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"')
                        set @strlocktoid =  (Select Top 1 Workflow
                        from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                            @"' and not id in (Select workflow_id from tbl_RPM_Agents_Workload)
                        and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"')

                            if @strlocktoid is null
						Begin
						Select Top 1 Workflow
						from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                            @"' and not id in (Select workflow_id from tbl_RPM_Agents_Workload where not 
							disposition in ('In progress','Escalated') ) 
                            and Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"'

						End
						Else
						Begin
						 Select Top 1 Workflow
                        from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                            @"' and not id in (Select workflow_id from tbl_RPM_Agents_Workload) and 
                        Workflow ='" + arr[ii] + @"' and Task_Type='" + arrSubTask[iii] + @"'
						End
                    END
                    ELSE
                    Select Top 1 Workflow
                    from " + dtTable.Rows[0][0].ToString() + @" where id = @strlocktoid";
                                    dt = cls.GetData(qry);

                                    if (dt.Rows.Count == 0)
                                    {

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                if (dt.Rows.Count == 0)
                                {

                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                        }
                        string tblGet = "";
                        string[] arrTables = { "Buildings", "Leases", "RRI" };
                        for (int i = 0; i < arrTables.Length; i++)
                        {
                            string qry1 = @"Select Headers,Workflow from tbl_RPM_ListMaker_Headers a inner join tbl_RPM_ListMaker_Headers_Created b on a.id = b.id 
                                    Where Workflow = '" + dt.Rows[0][0].ToString() + @"' and Assign_To_Workflow = 1 and Pick_Table = '" + arrTables[i] + @"'";

                            //Select Headers,'" + dt.Rows[0][46].ToString() + @"' as 'Workflow' from tbl_RPM_ListMaker_Headers where  Assign_To_Workflow = 1
                            tblGet = arrTables[i];
                            dt1 = cls.GetData(qry1);

                            if (dt1.Rows.Count == 0)
                            {
                            }
                            else
                            {
                                break;
                            }
                        }


                        List<string> headers = new List<string>();
                        foreach (DataColumn col in dt1.Columns)
                        {
                            headers.Add(col.ColumnName);
                        }
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            headersVal += headersVal == "" ? "a.[" + dt1.Rows[i]["Headers"].ToString() + "]" : ",a.[" + dt1.Rows[i]["Headers"].ToString() + "]";
                        }
                        headersVal = headersVal.Replace(" ", "_");
                        //qry = @"Select Top 1 " + headersVal + @" from " + dtTable.Rows[0][0].ToString() + @"";

                        //                        qry = @"Select Top 1 " + headersVal + @"
                        //                        from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                        //                        @"' and not id in (Select workflow_id from tbl_RPM_Agents_Workload Where workflow_id is not null)";



                        qry = @"Select Top 1 " + headersVal + @" ,c.AHT_Target,a.id
                        from " + dtTable.Rows[0][0].ToString() + @" a 
                        left join tbl_RPM_Agents_Workload b on b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                        where a.lockto = '" + HttpContext.Current.Session["ntid"] +
                        @"' and not a.id in (Select workflow_id from tbl_RPM_Agents_Workload Where workflow_id is not null and workflow_table = '" + tblGet + @"')";



                        dt = cls.GetData(qry);

                        if (dt.Rows.Count == 0)
                        {

                        }
                        else
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                valWorkable += valWorkable == "" ? dt.Rows[0][i].ToString() : "_" + dt.Rows[0][i].ToString();
                            }
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        string[] arrTables = { "Buildings", "Leases", "RRI" };
                        string[] arrTBL = { "tbl_RPM_Workable_Buildings", "tbl_RPM_Workable_Leases", "tbl_RPM_Workable_RRI" };

                        string qry = "";
                        string strtbl = "";
                        string strWorkflow = "";
                        DataTable dtTBL = new DataTable();
                        for (int i = 0; i < arrTBL.Length; i++)
                        {
                            qry = @"Select Top 1 Workflow from " + arrTBL[i] + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                                    Where disposition  IN ('In progress','Escalated') and ntid = '" + HttpContext.Current.Session["ntid"] + "' and not isSecond = 1";



                            dtTBL = cls.GetData(qry);


                            if (dtTBL.Rows.Count != 0)
                            {
                                strtbl = arrTBL[i];
                                strWorkflow = dtTBL.Rows[0][0].ToString();
                                break;
                            }
                            else
                            {

                            }
                        }

                        //                        qry = @"Select Top 1 Workflow from " + dtTable.Rows[0][0].ToString() + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                        //                        where lockto = '" + HttpContext.Current.Session["ntid"] + @"' and
                        //                        not b.id in(Select workflow_id from tbl_RPM_Agents_Workload_Process  where disposition in ('In progress','Escalated')  and workflow_id is not null  and isSecond = 1) and  b.disposition in 
                        //                        ('In progress','Escalated')";
                        //                        dt = cls.GetData(qry);

                        for (int i = 0; i < arrTables.Length; i++)
                        {
                            string qry1 = @"Select Headers,Workflow from tbl_RPM_ListMaker_Headers a inner join tbl_RPM_ListMaker_Headers_Created b on a.id = b.id 
                                    Where Workflow = '" + strWorkflow + @"' and Assign_To_Workflow = 1 and Pick_Table = '" + arrTables[i] + @"'";

                            //Select Headers,'" + dt.Rows[0][46].ToString() + @"' as 'Workflow' from tbl_RPM_ListMaker_Headers where  Assign_To_Workflow = 1

                            dt1 = cls.GetData(qry1);

                            if (dt1.Rows.Count == 0)
                            {

                            }
                            else
                            {
                                break;
                            }
                        }

                        List<string> headers = new List<string>();
                        foreach (DataColumn col in dt1.Columns)
                        {
                            headers.Add(col.ColumnName);
                        }
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            headersVal += headersVal == "" ? "a.[" + dt1.Rows[i]["Headers"].ToString() + "]" : ",a.[" + dt1.Rows[i]["Headers"].ToString() + "]";
                        }
                        headersVal = headersVal.Replace(" ", "_");


                        //                        qry = @"Select Top 1 " + headersVal + @" from " + strtbl + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                        //                        where lockto = '" + HttpContext.Current.Session["ntid"] + @"' and
                        //                        not b.id in(Select workflow_id from tbl_RPM_Agents_Workload  where disposition in ('In progress','Escalated')  and workflow_id is not null  and not isSecond = 1) and  b.disposition in 
                        //                        ('In progress','Escalated')";


                        //                        qry = @"Select Top 1 " + headersVal + @" from " + strtbl + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                        //                                Where disposition  IN ('In progress','Escalated') AND ntid = '" + HttpContext.Current.Session["ntid"] + @"' and not isSecond = 1";

                        qry = @"Select Top 1 " + headersVal + @" ,c.AHT_Target,a.id
                        from " + strtbl + @" a 
                        inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on 
                        c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                        Where disposition  IN ('In progress','Escalated') AND ntid = '" + HttpContext.Current.Session["ntid"] + @"' and not isSecond = 1
                            and lockto = '" + HttpContext.Current.Session["ntid"] + @"'";



                        dt = cls.GetData(qry);
                        if (dt.Rows.Count == 0)
                        {
                        }
                        else
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                valWorkable += valWorkable == "" ? dt.Rows[0][i].ToString() : "+" + dt.Rows[0][i].ToString();
                            }
                        }

                        if (valWorkable == "")
                        {
                            string qryUpdate = @"Update tbl_RPM_Agents_Workload set isSecond = 0
                                                Where ntid = '" + HttpContext.Current.Session["ntid"] +
                                                @"' and disposition = 'In progress' or disposition = 'Escalated'";
                            cls.ExecuteQuery(qryUpdate);

                            //                            qry = @"Select Top 1 " + headersVal + @" from " + strtbl + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                            //                                Where disposition  IN ('In progress','Escalated') AND ntid = '" + HttpContext.Current.Session["ntid"] + @"' and not isSecond = 1";

                            qry = @"Select Top 1 " + headersVal + @" ,c.AHT_Target,a.id
                        from " + strtbl + @" a 
                        inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on 
                        c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                         Where disposition  IN ('In progress','Escalated') AND ntid = '" + HttpContext.Current.Session["ntid"] + @"' and not isSecond = 1
                            and lockto = '" + HttpContext.Current.Session["ntid"] + @"'";


                            dt = cls.GetData(qry);
                            if (dt.Rows.Count == 0)
                            {
                            }
                            else
                            {
                                for (int i = 0; i < dt.Columns.Count; i++)
                                {
                                    valWorkable += valWorkable == "" ? dt.Rows[0][i].ToString() : "+" + dt.Rows[0][i].ToString();
                                }
                            }

                            if (valWorkable == "")
                            {
                                return JsonConvert.SerializeObject(new { Success = true, Message = "Not Workflow" });
                            }
                            else
                            {
                                return JsonConvert.SerializeObject(new { Success = true, Message = "NotCompleted", data = new { asd = dt1, asd1 = valWorkable, asd2 = dtTable.Rows[0][0].ToString() } });
                            }
                        }
                        else
                        {
                            //return JsonConvert.SerializeObject(
                            //new { Success = true, Message = "Not Workflow" });
                            return JsonConvert.SerializeObject(new { Success = true, Message = "NotCompleted", data = new { asd = dt1, asd1 = valWorkable, asd2 = dtTable.Rows[0][0].ToString() } });
                        }
                    }
                }
            }

            if (valWorkable == "")
            {
                string qry = "";

                //                qry = @"Select Top 1 " + headersVal + @"
                //                        from " + dtTable.Rows[0][0].ToString() + @" where lockto = '" + HttpContext.Current.Session["ntid"] + @"' and not id in 
                //                        (Select workflow_id from tbl_RPM_Agents_Workload)";

                //                qry = @"Select Top 1 " + headersVal + @" from " + dtTable.Rows[0][0].ToString() + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                //                        where lockto = '" + HttpContext.Current.Session["ntid"] + @"' and
                //                        not b.id in(Select workflow_id from tbl_RPM_Agents_Workload  where disposition in ('In progress','Escalated')  
                //                            and workflow_id is not null  and not isSecond = 1) and  b.disposition in 
                //                        ('In progress','Escalated')";
                qry = @"Select Top 1 " + headersVal + @" ,c.AHT_Target,a.id
                        from " + dtTable.Rows[0][0].ToString() + @" a 
                        inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on 
                        c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                         Where disposition  IN ('In progress','Escalated') AND ntid = '" + HttpContext.Current.Session["ntid"] + @"' and not isSecond = 1 
                        and lockto = '" + HttpContext.Current.Session["ntid"] + @"'";

                dt = cls.GetData(qry);

                if (dt.Rows.Count == 0)
                {

                }
                else
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        valWorkable += valWorkable == "" ? dt.Rows[0][i].ToString() : "+" + dt.Rows[0][i].ToString();
                    }
                }

                if (valWorkable == "")
                {
                    return JsonConvert.SerializeObject(
                  new { Success = true, Message = "Not Workflow" });
                }
                else
                {
                    //    return JsonConvert.SerializeObject(
                    //        new { Success = true, Message = "Not Workflow" });
                    return JsonConvert.SerializeObject(new { Success = true, Message = "NotCompleted", data = new { asd = dt1, asd1 = valWorkable, asd2 = dtTable.Rows[0][0].ToString() } });
                }

            }
            else
            {
                //    return JsonConvert.SerializeObject(
                //            new { Success = true, Message = "Not Workflow" });
                return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1, asd1 = valWorkable, asd2 = dtTable.Rows[0][0].ToString() } });
            }
        }
        [WebMethod]
        public static string GetWorkable()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            string qry = @"Select Top 1 * from tbl_RPM_Workable";
            string headersVal = "";
            dt = cls.GetData(qry);
            string qry1 = @"Select Headers,'" + dt.Rows[0][46].ToString() + "' as 'Workflow' from tbl_RPM_ListMaker_Headers where  Assign_To_Workflow = 1";



            dt1 = cls.GetData(qry1);



            List<string> headers = new List<string>();
            foreach (DataColumn col in dt1.Columns)
            {
                headers.Add(col.ColumnName);
            }

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                headersVal += headersVal == "" ? "[" + dt1.Rows[i]["Headers"].ToString() + "]" : ",[" + dt1.Rows[i]["Headers"].ToString() + "]";
            }
            headersVal = headersVal.Replace(" ", "_");
            qry = @"Select Top 1 " + headersVal + @" from tbl_RPM_Workable";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }



        [WebMethod]
        public static string Save(string duration, string workStatus, string disposition, string remarks, string strTable, string strInEs, string Workflow_id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string tableType = "";

            if (strTable == "tbl_RPM_Workable_Buildings")
            {
                tableType = "Buildings";
            }
            else if (strTable == "tbl_RPM_Workable_Leases")
            {
                tableType = "Leases";
            }
            else
            {
                tableType = "RRI";
            }

            string qry = "";
            if (strInEs == "Success")
            {

                if (disposition == "In progress" || disposition == "Escalated")
                {
                    //                    qry = @"Insert Into tbl_RPM_Agents_Workload (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                    //                            values('" + HttpContext.Current.Session["ntid"] +
                    //                      @"', '" + workStatus + "','','" + duration + @"',(
                    //                            Select TOP 1 id from " + strTable + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                    //                      @"'  and not id in (Select workflow_id from tbl_RPM_Agents_Workload  Where workflow_id is not null)), '" + tableType +
                    //                      "',GetDate(),'" + disposition + "','" + remarks + "') ";
                    qry = @"Insert Into tbl_RPM_Agents_Workload (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                                                values('" + HttpContext.Current.Session["ntid"] +
                      @"', '" + workStatus + "','','" + duration + @"','" + Workflow_id + "', '" + tableType +
                      "',GetDate(),'" + disposition + "','" + remarks + "') ";

                    qry += @" Insert Into tbl_RPM_Agents_Workload_Process (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                            values('" + HttpContext.Current.Session["ntid"] +
                     @"', '" + workStatus + "','','" + duration + @"','" + Workflow_id + "', '" + tableType +
                     "',GetDate(),'" + disposition + "','" + remarks + "') ";

                    //                    qry += @" Insert Into tbl_RPM_Agents_Workload_Process (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                    //                            values('" + HttpContext.Current.Session["ntid"] +
                    //                     @"', '" + workStatus + "','','" + duration + @"',(
                    //                            Select TOP 1 id from " + strTable + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                    //                     @"'  and not id in (Select workflow_id from tbl_RPM_Agents_Workload  Where workflow_id is not null)), '" + tableType +
                    //                     "',GetDate(),'" + disposition + "','" + remarks + "') ";

                }
                else
                {
                    //                    qry = @"Insert Into tbl_RPM_Agents_Workload (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                    //                            values('" + HttpContext.Current.Session["ntid"] +
                    //                      @"', '" + workStatus + "','','" + duration + @"',(
                    //                            Select TOP 1 id from " + strTable + @" where lockto = '" + HttpContext.Current.Session["ntid"] +
                    //                      @"'  and not id in (Select workflow_id from tbl_RPM_Agents_Workload  Where workflow_id is not null)), '" + tableType +
                    //                      "',GetDate(),'" + disposition + "','" + remarks + "')";

                    qry = @"Insert Into tbl_RPM_Agents_Workload (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                            values('" + HttpContext.Current.Session["ntid"] +
                  @"', '" + workStatus + "','','" + duration + @"','" + Workflow_id + "', '" + tableType +
                  "',GetDate(),'" + disposition + "','" + remarks + "')";

                }
            }
            else if (strInEs == "NotCompleted")
            {
                if (disposition == "In progress" || disposition == "Escalated")
                {
                    //                    qry = @"Select workflow_id from tbl_RPM_Workable_Buildings a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                    //                            Where disposition  IN ('In progress','Escalated') and ntid = 'alintano' and not isSecond = 1";



                    //                    qry = @" Insert Into tbl_RPM_Agents_Workload_Process (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks,isSecond) 
                    //                            values('" + HttpContext.Current.Session["ntid"] +
                    //                   @"', '" + workStatus + "','','" + duration + @"',(Select TOP 1 workflow_id from " + strTable + @" a inner join tbl_RPM_Agents_Workload b on
                    //                            b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on 
                    //                        c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                    //                            Where disposition  IN ('In progress','Escalated') and ntid = '" + HttpContext.Current.Session["ntid"] +
                    //                      @"' and not isSecond = 1), '" + tableType +
                    //                        "',GetDate(),'" + disposition + "','" + remarks + "',1) ";
                    qry = @" Insert Into tbl_RPM_Agents_Workload_Process (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks,isSecond) 
                            values('" + HttpContext.Current.Session["ntid"] +
                   @"', '" + workStatus + "','','" + duration + @"','" + Workflow_id + "', '" + tableType +
                        "',GetDate(),'" + disposition + "','" + remarks + "',1) ";
                    cls.ExecuteQuery(qry);

                    //                    qry = " Update tbl_RPM_Agents_Workload set work_date = GetDate(), disposition = '" + disposition + "', remarks ='" + remarks +
                    //                              "',Duration = '" + duration + @"', isSecond = 1 Where workflow_id IN (Select Top 1 workflow_id from " + strTable + @" a 
                    //            inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id inner join vw_RPM_WorkflowManager_Settings c on 
                    //                        c.SubTask = a.Task_Type and  c.Workflow= a.Workflow
                    //                            Where disposition  IN ('In progress','Escalated') and ntid = '" + HttpContext.Current.Session["ntid"] +
                    //                    @"' and not isSecond = 1) ";
                    qry = " Update tbl_RPM_Agents_Workload set work_date = GetDate(), disposition = '" + disposition + "', remarks ='" + remarks +
                              "',Duration = '" + duration + @"', isSecond = 1 Where workflow_id IN ('" + Workflow_id + "') and ntid = '" + HttpContext.Current.Session["ntid"] +
                    @"' and not isSecond = 1";
                  cls.ExecuteQuery(qry);
                    qry = "";
                }
                else
                {
                    qry = "Update tbl_RPM_Agents_Workload set work_date = GetDate(),disposition = '" + disposition + "', remarks ='" + remarks +
                               "',Duration = '" + duration + @"', isSecond = 0 where workflow_id IN ('" + Workflow_id + "')";
                    //                    qry = "Update tbl_RPM_Agents_Workload set work_date = GetDate(),disposition = '" + disposition + "', remarks ='" + remarks +
                    //                               "',Duration = '" + duration + @"', isSecond = 0 where workflow_id IN (Select Top 1 workflow_id from " + strTable + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                    //                            Where disposition  IN ('In progress','Escalated') and ntid = '" + HttpContext.Current.Session["ntid"] +
                    //                     @"' and not isSecond = 1)";


                    //                    qry = @"Insert Into tbl_RPM_Agents_Workload_Process (ntid,work_status,Status,Duration,workflow_id,workflow_table,work_date,disposition,remarks) 
                    //                            values('" + HttpContext.Current.Session["ntid"] +
                    //                      @"', '" + workStatus + "','','" + duration + @"',(Select workflow_id from " + strTable + @" a inner join tbl_RPM_Agents_Workload b on b.workflow_id = a.id
                    //                            Where disposition  IN ('In progress','Escalated') and ntid = 'alintano' and not isSecond = 1), '" + tableType +
                    //                      "',GetDate(),'" + disposition + "','" + remarks + "')";
                }

            }
            else
            {

            }

            //" + strTable + @"

          cls.ExecuteQuery(qry);
            return "True";
        }

        [WebMethod]
        public static string SaveWorkStatus(string workStatus, string duration, string valpreStatus)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qrySelect = @"Select CONVERT(varchar, time , 101),* from tbl_RPM_Agent_WorkStatus 
                                where work_status = '" + workStatus + "' and ntid = '" + HttpContext.Current.Session["ntid"] +
                                @"' and isLogout = 0";
            dt = cls.GetData(qrySelect);
            if (dt.Rows.Count == 0)
            {
                string qry = @"Insert Into tbl_RPM_Agent_WorkStatus (ntid,work_status,time) values('" + HttpContext.Current.Session["ntid"] +
                           @"', '" + workStatus + "',GetDate())";
                if (workStatus != "Logout")
                {
                    cls.ExecuteQuery(qry);
                }
            }
            else
            {
                if (workStatus == "Working...")
                {
                    string qry = @"Insert Into tbl_RPM_Agent_WorkStatus (ntid,work_status,time) values('" + HttpContext.Current.Session["ntid"] +
                               @"', '" + workStatus + "',GetDate())";
                    cls.ExecuteQuery(qry);
                }
                else
                {

                }
            }
            if (valpreStatus == "Working...")
            {
                if (workStatus == "Logout")
                {
                    string qryUpdate = @"Update tbl_RPM_Agent_WorkStatus set duration = '" + duration + @"' 
                                where work_status = '" + valpreStatus + "' and ntid = '" + HttpContext.Current.Session["ntid"] + @"' and isLogout = 0 
                                and (duration = '' or duration is null)";
                    cls.ExecuteQuery(qryUpdate);

                    string qry = @"Insert Into tbl_RPM_Agent_WorkStatus (ntid,work_status,time,duration) values('" + HttpContext.Current.Session["ntid"] +
                               @"', '" + workStatus + "',GetDate(),'0:00:00')";
                    cls.ExecuteQuery(qry);

                    qryUpdate = "Update tbl_RPM_Agent_WorkStatus set isLogout = 1 where ntid = '" + HttpContext.Current.Session["ntid"] +
                                "' and isLogout = 0";
                    cls.ExecuteQuery(qryUpdate);
                }
                else
                {
                    string qryUpdate = @"Update tbl_RPM_Agent_WorkStatus set duration = '" + duration + @"' 
                                where work_status in ('Working','Working...') and ntid = '" + HttpContext.Current.Session["ntid"] + @"' and isLogout = 0 
                                and (duration = '' or duration is null)";
                    cls.ExecuteQuery(qryUpdate);
                }
            }
            else
            {
                string qryUpdate = @"Update tbl_RPM_Agent_WorkStatus set duration = '" + duration + @"' 
                                where work_status = '" + valpreStatus + "' and ntid = '" + HttpContext.Current.Session["ntid"] + @"' and isLogout = 0 
                                and (duration = '' or duration is null)";
                cls.ExecuteQuery(qryUpdate);

            }
            return "True";
        }
    }
}