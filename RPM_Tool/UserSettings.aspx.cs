﻿using Master_Control;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RPM_Tool
{

    public partial class UserSettings : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "1")
                    {
                        idVal = "1";
                    }
                }

                if (idVal == "1")
                {

                }
                else
                {
                    //  Response.Redirect("Home.aspx");
                }

            }



        }

        [WebMethod]
        public static UserList userList()
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            dt = cls.GetData("EXEC spRPMGetDataUserList");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    ud.ntid = dt.Rows[i][6].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }
        [WebMethod]
        public static UserList userListSelect(string ntid)
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            string qry = "EXEC spRPMGetuserListSelect  '" + ntid + "'";

            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    ud.ntid = dt.Rows[i][6].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }
        [WebMethod]
        public static string empLevel()
        {
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();
            dtSet = cls.GetDataSet("EXEC spRPMGetempLevel");
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dtSet.Tables[0], asd2 = dtSet.Tables[1], asd3 = dtSet.Tables[2] } });
        }
        [WebMethod]
        public static List<AccessRole> loadAccess(string type)
        {
            List<AccessRole> ls = new List<AccessRole>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            dt = cls.GetData("EXEC spRPMGetloadAccess '" + type + "'");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AccessRole ar = new AccessRole();
                ar.id = dt.Rows[i][0].ToString();
                ar.name = dt.Rows[i][1].ToString();
                ls.Add(ar);
            }

            return ls;
        }
        [WebMethod]
        public static List<AccessFunc> loadFunction(string id, string type)
        {
            List<AccessFunc> ls = new List<AccessFunc>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            DataSet dtSet = new DataSet();
            string qry = "";
            dtSet = cls.GetDataSet("EXEC spRPMGetloadFunction " + id + ",'" + type + "'");
            dt = dtSet.Tables[0];
            if (dt.Rows[0][0].ToString() == "True")
            {
                dt = dtSet.Tables[1];
                string avl = dt.Rows[0][0].ToString();

                //current function
                qry = "Select * from tbl_RPM_Access_Role_Function where tag = 'RPM' and id in (" + avl + ")";

                dt = cls.GetData(qry);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Console.WriteLine(dt.Rows[i][1].ToString());
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "True";
                    ls.Add(ar);
                }
                //Available function
                Console.WriteLine("\n");
                qry = "Select * from tbl_RPM_Access_Role_Function where tag = 'RPM' and id not in (" + avl + ") and functype='" + type + "'";
                dt = cls.GetData(qry);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }
            else
            {
                dt = dtSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }
            return ls;





            //string qry = "Select * from tbl_RPM_Access_Role_Type_Function where tag = 'RPM' and role_id =" + id;
            //dt = cls.GetData(qry);
            //if (dt.Rows.Count > 0)
            //{
            //    string avl = dt.Rows[0][2].ToString();

            //    //current function
            //    qry = "Select * from tbl_RPM_Access_Role_Function where tag = 'RPM' and id in (" + avl + ")";

            //    dt = cls.GetData(qry);

            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        Console.WriteLine(dt.Rows[i][1].ToString());
            //        AccessFunc ar = new AccessFunc();
            //        ar.id = dt.Rows[i][0].ToString();
            //        ar.name = dt.Rows[i][1].ToString();
            //        ar.isOn = "True";
            //        ls.Add(ar);
            //    }
            //    //Available function
            //    Console.WriteLine("\n");
            //    qry = "Select * from tbl_RPM_Access_Role_Function where tag = 'RPM' and id not in (" + avl + ") and functype='" + type + "'";
            //    dt = cls.GetData(qry);
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        AccessFunc ar = new AccessFunc();
            //        ar.id = dt.Rows[i][0].ToString();
            //        ar.name = dt.Rows[i][1].ToString();
            //        ar.isOn = "False";
            //        ls.Add(ar);
            //    }
            //}
            //else
            //{
            //    //if null
            //    qry = "Select * from tbl_RPM_Access_Role_Function where functype='" + type + "' and tag = 'RPM'";
            //    dt = cls.GetData(qry);
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        AccessFunc ar = new AccessFunc();
            //        ar.id = dt.Rows[i][0].ToString();
            //        ar.name = dt.Rows[i][1].ToString();
            //        ar.isOn = "False";
            //        ls.Add(ar);
            //    }
            //}
            
        }
        [WebMethod]
        public static void updateFunction(string id, string newStr)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataSet dtSet = new DataSet();
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

            DataTable dtRole = new DataTable();
            dtRole = cls.GetData("EXEC spRPMUpdateFunction " + id + ",'" + newStr + "'");

            cls.FUNC.Audit("Updated Role/Skill Function: " + dtRole.Rows[0][1].ToString(), arrPages);

        }
        [WebMethod]
        public static void addAccess(string name, string desc, string type)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("EXEC spRPMaddAccess '" + name + "','" + desc + "','" + type + "'");
            //cls.ExecuteQuery("INSERT INTO tbl_RPM_Access_Role_Type(accessname,accessdesc,type,isActive, tag) Values ('" + name + "', '" + desc + "','" + type + "',1, 'RPM')");
            //cls.ExecuteQuery("INSERT INTO tbl_RPM_Access_Role_Type_Function(role_id,function_on) Values ((select MAX(ID) from tbl_RPM_Access_Role_Type) , 0)");
            //ArrayList arrPages = new ArrayList();
            //arrPages.Add("User");
            //arrPages.Add("Access");
            //cls.FUNC.Audit("Create New Role/Skill: " + name, arrPages);
        }
        [WebMethod]
        public static string deleteLine(string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            dt =  cls.GetData("EXEC spRPMdeleteLine " + id);
          //  cls.ExecuteQuery("Update tbl_RPM_Access_Role_Type set isActive = 0 where id=" + id);
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");
          
          //  dt = cls.GetData("select * from tbl_RPM_Access_Role_Type where tag = 'RPM' and id =" + id);

            cls.FUNC.Audit("Deleted Role/Skill: " + dt.Rows[0][1].ToString(), arrPages);
            return dt.Rows[0][3].ToString();

        }

        [WebMethod]
        public static void applyAction(string role_id, string emp_id)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_RPM_Access_User set role_id =" + role_id + " where id in (" + emp_id + ")");
            //ArrayList arrPages = new ArrayList();
            //arrPages.Add("User");
            //arrPages.Add("User");
            //DataTable dt = new DataTable();
            //dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where tag = 'UC' and id =" + role_id);
            //string role = dt.Rows[0][1].ToString();
            //dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where  tag = 'UC' and id =" + skill_id);
            //string skill = dt.Rows[0][1].ToString();
            //cls.FUNC.Audit("Applied Role: " + role + " and Skill: " + skill + " to certain users", arrPages);
        }

        [WebMethod]
        public static UserList filterby(string empcat, string immeSuper)
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();

            string qry = "EXEC spRPMfilterby '" + empcat +  "','" + immeSuper + "'" ;
            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    ud.ntid = dt.Rows[i][6].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }
    }
}