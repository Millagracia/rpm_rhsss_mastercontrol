﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace RPM_Tool
{
    /// <summary>
    /// Summary description for ExcelFileUpload
    /// </summary>
    public class ExcelFileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            clsConnection cls = new clsConnection();
            HttpFileCollection files = context.Request.Files;
            string fname = files.AllKeys[0].ToString();
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string path = Path.Combine(context.Server.MapPath("~/fileExcel/"), file.FileName + "");
                file.SaveAs(path);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

       
    }

}