﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RPM_Tool
{
    public partial class WorkflowMaker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFunc = new DataTable();
            dtFunc = cls.GetData("EXEC spGetPage '" + Session["ntid"] + "',0");
            if (dtFunc.Rows.Count > 0)
            {
                string func = dtFunc.Rows[0]["function_on"].ToString();

                string[] ids = func.Split(',');

                ArrayList arr = new ArrayList();
                string idVal = "";
                foreach (string id in ids)
                {
                    if (id == "2")
                    {
                        idVal = "2";
                    }
                }

                if (idVal == "2")
                {

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }
        [WebMethod]
        public static string ddlValues()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            string qry = "Select * from tbl_RPM_ListTitle_Option where id = 24";
            //string qry1 = "Select NTID,EmpName from tbl_RPM_HRMS_EmployeeMaster where EmpLevel = 'MGR' order by EmpName";
            string qry1 = @"EXEC sp_workflow"; 
            //string qry2 = "Select NTID,EmpName from tbl_RPM_HRMS_EmployeeMaster where EmpLevel = 'TL' order by EmpName";
            string qry2 = "exec sp_tlworkflow";
            string qry3 = "Select distinct Created_By from [tbl_RPM_Workflow] order by Created_By";
            string qry4 = "Select * from tbl_RPM_ListTitle where id = 25";
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            dt2 = cls.GetData(qry2);
            dt3 = cls.GetData(qry3);
            dt4 = cls.GetData(qry4);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dt2, asd3 = dt3, asd4 = dt4 } });
        }
        [WebMethod]
        public static string ddlValuesSubTask(string workflow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt4 = new DataTable();
            string qry4 = @"Select * from tbl_RPM_ListTitle_SubOption 
                        where id = (select Workflow_ID from tbl_RPM_ListTitle_Option where List_Option = '" + workflow + "')";
            dt4 = cls.GetData(qry4);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new {asd4 = dt4 } });
        }
        [WebMethod]
        public static string SaveWorkflow(string slcNewWorkflow, string slcQueue, string MGR, string TL, ArrayList slcSubTask,
            ArrayList slcSLAnum, ArrayList slcSLAdays, ArrayList slcAHThr, ArrayList slcAHTmm, ArrayList slcAHTss)
        {
            clsConnection cls = new clsConnection();
            string qryInsertSubTask = "";
            string qryInsertWorkflow = @"Insert Into tbl_RPM_Workflow (Workflow,QueueMethod,Manager,TeamLeader,Created_By,Created_Date) 
                                            values('" + slcNewWorkflow + "','" + slcQueue + "','" + MGR + "','" + TL + "','" + HttpContext.Current.Session["ntid"] +
                                                     "',CONVERT(CHAR(10), GETDATE(), 101))";

            int countTask = slcSubTask.Count + 1;

            for (int i = 0; i < slcSubTask.Count; i++)
            {
                qryInsertSubTask += "Insert Into tbl_RPM_Workflow_SubTask (id,SubTask,SLA,AHT_Target) values((select Max(id) from tbl_RPM_Workflow),'" + slcSubTask[i] + "','" + slcSLAnum[i]
                                        + " " + slcSLAdays[i] + "','" + slcAHThr[i] + ":" + slcAHTmm[i] + ":" + slcAHTss[i] + "');";
            }

            qryInsertSubTask = qryInsertSubTask.Remove(qryInsertSubTask.Length - 1, 1);
            cls.ExecuteQuery(qryInsertWorkflow);
            cls.ExecuteQuery(qryInsertSubTask);

            return "true";
        }

        [WebMethod]
        public static string SaveWorkflowEdit(string slcNewWorkflow, string slcQueue, string MGR, string TL, ArrayList slcSubTask,
            ArrayList slcSLAnum, ArrayList slcSLAdays, ArrayList slcAHThr, ArrayList slcAHTmm, ArrayList slcAHTss, string id)
        {
            clsConnection cls = new clsConnection();
            string qryInsertSubTask = "";
//          string qryInsertWorkflow = @"Insert Into tbl_RPM_Workflow (Workflow,QueueMethod,Manager,TeamLeader,Created_By,Created_Date) 
//                                            values('" + slcNewWorkflow + "','" + slcQueue + "','" + MGR + "','" + TL + "','" + HttpContext.Current.Session["ntid"] +
//                                                     "',CONVERT(CHAR(10), GETDATE(), 101))";
            string qryUpdate = @"Update tbl_RPM_Workflow Set Workflow = '" + slcNewWorkflow + @"', QueueMethod = '" + slcQueue + @"', Manager = '" + MGR + @"', 
                                    TeamLeader = '" + TL + @"', Created_By = '" + HttpContext.Current.Session["ntid"] +
                                                     @"', Created_Date = CONVERT(CHAR(10), GETDATE(), 101) where id = '" + id + "'";
            int countTask = slcSubTask.Count + 1;
            cls.ExecuteQuery("Delete tbl_RPM_Workflow_SubTask where id = '" + id + "'");
            for (int i = 0; i < slcSubTask.Count; i++)
            {
                qryInsertSubTask += "Insert Into tbl_RPM_Workflow_SubTask (id,SubTask,SLA,AHT_Target) values('" + id + "','" + slcSubTask[i] + "','" + slcSLAnum[i]
                                        + " " + slcSLAdays[i] + "','" + slcAHThr[i] + ":" + slcAHTmm[i] + ":" + slcAHTss[i] + "');";
            }
            qryInsertSubTask = qryInsertSubTask.Remove(qryInsertSubTask.Length - 1, 1);
            cls.ExecuteQuery(qryUpdate);
            cls.ExecuteQuery(qryInsertSubTask);
            return "true";
        }


        [WebMethod]
        public static string displayWorkflow()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = @"SELECT a.id,a.Workflow,a.Created_By,a.Created_Date, SubTask =REPLACE( (SELECT REPLACE(SubTask,' ','_') AS [data()] FROM tbl_RPM_Workflow_SubTask b WHERE b.id = a.id 
			ORDER BY SubTask FOR XML PATH('')), ' ', ' ') FROM [tbl_RPM_Workflow] a ";

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string displayWorkflowSearch(string slcWorkflow, string slcCreatedBy, string dFrom, string dTo)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "";
            if (dFrom == "" || dTo == "")
            {
                qry = @"SELECT a.id,a.Workflow,a.Created_By,a.Created_Date, SubTask =REPLACE( (SELECT REPLACE(SubTask,' ','_') AS [data()] FROM tbl_RPM_Workflow_SubTask b WHERE b.id = a.id 
			ORDER BY SubTask FOR XML PATH('')), ' ', ' ') FROM [tbl_RPM_Workflow] a where a.Workflow = '" + slcWorkflow + "' and a.Created_By = '" + slcCreatedBy +
                           "' order by a.id";

                //qry = "Select * from [tbl_RPM_Workflow] where Workflow = '" + slcWorkflow + "' and Created_By = '" + slcCreatedBy +
                //           "' order by id";
            }
            else
            {
                qry = @"SELECT a.id,a.Workflow,a.Created_By,a.Created_Date, SubTask =REPLACE( (SELECT REPLACE(SubTask,' ','_') AS [data()] FROM tbl_RPM_Workflow_SubTask b WHERE b.id = a.id 
			ORDER BY SubTask FOR XML PATH('')), ' ', ' ') FROM [tbl_RPM_Workflow] a where a.Workflow = '" + slcWorkflow + "' and a.Created_By = '" + slcCreatedBy +
                          "' and a.Created_Date Between '" + dFrom + "' and '" + dTo + "' order by a.id";
                //qry = "Select * from [tbl_RPM_Workflow] where Workflow = '" + slcWorkflow + "' and Created_By = '" + slcCreatedBy +
                //          "' and Created_Date Between '" + dFrom + "' and '" + dTo + "' order by id";

            }

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }



        [WebMethod]
        public static string EditWorkflow(string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "";

            qry = @"select a.id,a.Workflow,b.SubTask,a.QueueMethod,a.Manager,a.TeamLeader,b.SubTask,b.SLA,b.AHT_Target from [dbo].[tbl_RPM_Workflow] a 
            inner join tbl_RPM_Workflow_SubTask b on b.id = a.id where a.id = '" + id + "'";

            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string deleteWorkflow(string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "Delete tbl_RPM_Workflow where id = " + id + " Delete tbl_RPM_Workflow where id = " + id;
            cls.ExecuteQuery(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success" });
        }
        
    }
}