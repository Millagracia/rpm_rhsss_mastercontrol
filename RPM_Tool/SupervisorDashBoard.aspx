﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SupervisorDashBoard.aspx.cs" Inherits="RPM_Tool.SupervisorDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .page-navigation a {
            margin: 0 2px;
            margin: 0 2px;
            background-color: #ffffff; /* Green */
            border: 1px solid #428bca;
            border-radius: 5px;
            color: #428bca;
            padding: 1px 9px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
        }
            .page-navigation a[data-selected] {
                background-color: #428bca;
                color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper dvNotReady RPMbackground">
        <section class="content-header">
             <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Supervisor Dashboard</a></li>
            </ol>
        </section>
        <section class="content" style="padding-bottom: 0px">
            <div class="row">
                <div class="col-lg-3">
                    <div class="btn-group">
                        <button type="button" id="btnWorkflow" style="width: 118px" class="btn btn-primary ">Workflow</button>
                        <button type="button" id="btnAgents" style="width: 118px" class="btn btn-default ">Agents</button>
                    </div>
                </div>
            </div>
            <div class="row topStyle">
                <div class="col-lg-12">
                    <div class="panel panel-body" style="height: 636px">
                        <div class="row dvWorkflow">
                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-3 text-right">
                                <label style="padding-top: 29px">Total Backlog from previous days</label>
                            </div>
                            <div class="col-lg-1 text-center text-red">
                                <label id="lblBacklog" style="padding-top: 29px">0</label>
                                <hr style="background-color: black; padding-top: 1px; margin-top: 10px">
                            </div>
                            <div class="col-lg-2">
                                <label>TOTAL QUEUE LEFT</label>
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-5 text-center" style="background-color: #142541; color: #fff; height: 40px">
                                        <label id="lblTotalLeft" style="padding-top: 7px; font-size: 20px">0</label>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row topStyle dvWorkflow">
                            <div class="col-lg-12 ">
                                <table id="tblWorkflow" class="table table-bordered table-striped" data-single-select="true">
                                    <thead>
                                        <tr>
                                            <th data-sortable="true" data-field="WorkflowType">Workflow Type</th>
                                            <th data-sortable="true" data-field="TotalInflow">Total Inflow</th>
                                            <th data-sortable="true" data-field="ActionRequired">Action Required</th>
                                            <th data-sortable="true" data-field="Counts">Count</th>
                                            <th style="color:#ff0000" data-field="TotalLeft">Total Left</th>
                                            <th style="color:#ff0000" data-field="TimeRequired">Time Required</th>
                                            <th style="color:#ff0000" data-field="NoOfAgents"># of Agents</th>
                                            <th style="color:#168700" data-field="Complete">Completed</th>
                                            <th style="color:#168700" data-field="Inprogress">In Progress</th>
                                            <th style="color:#168700" data-field="Escalated">Escalated</th>
                                        </tr>
                                    </thead>
                                    <tbody class="dvAppendTable">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row topStyle dvAgents" style="display: none">
                            <div class="col-lg-12">
                                <table id="tblAgents" style="width: 100%" class="table table-bordered table-striped ">
                                    <thead>
                                        <tr>
                                            <th data-field="Associate">Associate</th>
                                            <th data-field="ImmediateSupervisor">Immediate Supervisor</th>
                                            <th data-field="Status">Status</th>
                                            <th data-field="Duration">Duration</th>
                                            <th style="color: #168700" data-field="TotalProcessed">Total Processed</th>
                                        </tr>
                                    </thead>
                                    <tbody class="dvAppendTableAgents">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="JS/SupervisorDashboard.js"></script>
    <script>
</script>



</asp:Content>
