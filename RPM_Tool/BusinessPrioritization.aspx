﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="BusinessPrioritization.aspx.cs" Inherits="RPM_Tool.BusinessPrioritization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .drag {
            width: 100%;
            height: 40px;
            padding: 10px;
            border: 1px solid #aaaaaa;
        }
        .panelBox {
            width: 100%;
            height: 40px;
            padding: 10px;
            border: 1px solid #aaaaaa;
        }
        .drag:hover, .pointerCursor:hover {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Master Control</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;Business Prioritization</span>
                </li>
            </ol>
        </section>
        <section class="content" style="padding-bottom: 0px">
            <div class="row">
                <div class="col-lg-3">
                    <div class="btn-group btn-group-justified">
                        <a href="#" id="btnCurrent" class="btn btn-primary">Current</a>
                        <a href="#" id="btnFuture" class="btn btn-default">Future</a>
                        <a href="#" id="btnSettings" class="btn btn-default">Settings</a>
                    </div>
                </div>
            </div>
            <div id="dvCurrent">
                <div class="row" style="padding-top: 20px">
                    <div class="col-lg-12">
                        <div class="panel panel-body" style="height: 580px; overflow: scroll;">
                            <table class="table table-hover">
                                <thead style="background-color: #142541; color: #fff">
                                    <tr>
                                        <th>Priority</th>
                                        <th>Last Modified By</th>
                                        <th>Last Modified Date and Time</th>
                                        <th>Effective Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvFuture" style="display: none">
                <div class="row" style="padding-top: 20px">
                    <div class="col-lg-12">
                        <div class="panel panel-body" style="height: 580px; overflow: scroll;">
                            <table id="tblBusinessPrio" class="table table-bordered table-hover" data-single-select="true">
                                <thead style="background-color: #142541; color: #fff">
                                    <tr>
                                        <th data-field="Priority" data-formatter="idFormatter1">Priority</th>
                                        <th data-field="LastModified_By">LastModified By</th>
                                        <th data-formatter="dateFormat" data-field="LastModified_Date">LastModified Date</th>
                                        <th data-formatter="idFormatter3" data-field="Effective_From">Effective Date</th>
                                        <th data-formatter="EditDelete" data-field="id"></th>
                                    </tr>
                                </thead>
                            </table>
                          <button type="button" id="btnrefresh" class="btn btn-primary">Refresh</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvSetting" style="display: none">
                <div class="row">
                    <div class="col-lg-7">Prioritize By:</div>
                    <div class="col-lg-5 dvWorkflow" style="display: none">Workflow</div>
                    <div class="col-lg-5 dvStatePrio" style="display: none">State</div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="panel panel-body" style="height: 580px; margin-bottom: 0px">
                            <div class="row">
                                <div class="col-lg-6 dvSettingsPrio">
                                    <div class="row topStyle dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div class="panelBox pointerCursor">
                                                <div id="dragPrio1" draggable="true" ondragstart="drag(event)">
                                                    <div class="col-lg-9" style="padding-left: 0px">
                                                        <label class="pointerCursor lblPrioritize">SLA</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row topStyle dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div class="panelBox pointerCursor">
                                                <div id="dragPrio2" draggable="true" ondragstart="drag(event)">
                                                    <div class="col-lg-9" style="padding-left: 0px">
                                                        <label class="pointerCursor lblPrioritize">Workflow</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row topStyle dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div class="panelBox pointerCursor">
                                                <div id="dragPrio3" draggable="true" ondragstart="drag(event)">
                                                    <div class="col-lg-9" style="padding-left: 0px">
                                                        <label class="pointerCursor lblPrioritize">State</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 text-right noPaddingR">
                                    <div class="row topStyle">
                                        <div class="col-lg-12">
                                            <h3 style="margin-top: 8px; margin-bottom: 0px">1</h3>
                                        </div>
                                    </div>
                                    <div class="row topStyle">
                                        <div class="col-lg-12">
                                            <h3 style="margin-top: 16px; margin-bottom: 0px">2</h3>
                                        </div>
                                    </div>
                                    <div class="row topStyle">
                                        <div class="col-lg-12">
                                            <h3 style="margin-top: 13px; margin-bottom: 0px">3</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 dvSettingsPrioDrop">
                                    <div class="row topStyle dvPrioritize1 dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>
                                        </div>
                                    </div>
                                    <div class="row topStyle dvPrioritize2 dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>
                                        </div>
                                    </div>
                                    <div class="row topStyle dvPrioritize3 dvDestroySettingsPrio">
                                        <div class="col-lg-12">
                                            <div ondrop="drop(event)" class="drag" ondragover="allowDrop(event)"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="panel panel-body" style="height: 270px; overflow: scroll;">
                                <div class="row dvWorkflow" style="display: none">
                                    <div class="col-lg-5 dvPrio ">
                                    </div>
                                    <div class="col-lg-1 dvPrioNum ">
                                    </div>
                                    <div class="col-lg-6 dvPrioDropZone">
                                    </div>
                                </div>
                                <div class="dvAppendPrio topStyle dvStatePrio" style="display: none">
                                    <div class="row dvState">
                                        <div class="col-lg-2" style="padding-right: 0px; padding-top: 4px">1st Priority</div>
                                        <div class="col-lg-4">
                                            <select id="slcState" class="form-control input-sm tb1">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row dvStatePrio" style="display: none">
                                    <div class="col-lg-4">
                                        <button id="btnAddPrio" type="button" class="btn btn-link"><i class="fas fa-plus text-green"></i>Add Priority</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            Save Settings
                        </div>
                        <div class="row">
                            <div class="panel panel-body" style="height: 270px">
                                <form action="a">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <input type="radio" name="for" value="For" id="chkFor"/><b>For</b>
                                        </div>
                                        <div class="col-lg-3" style="padding-right: 3px">
                                            <select id="slcDays" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-5" style="padding-left: 3px;">
                                            <select id="slcMHDWM" class="form-control input-sm tb1">
                                                <option value="">Select</option>
                                                <option value="minute">minute(s)</option>
                                                <option value="hour">hour(s)</option>
                                                <option value="day">day(s)</option>
                                                <option value="week">week(s)</option>
                                                <option value="month">month(s)</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row" style="padding-top: 5px">
                                        <div class="col-lg-2">
                                        </div>
                                        <div class="col-lg-3" style="padding-right: 3px">
                                            <select id="slcForHr" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2" style="padding-right: 3px; padding-left: 3px">
                                            <select id="slcForMins" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                                <option value="49">49</option>
                                                <option value="50">50</option>
                                                <option value="51">51</option>
                                                <option value="52">52</option>
                                                <option value="53">53</option>
                                                <option value="54">54</option>
                                                <option value="55">55</option>
                                                <option value="56">56</option>
                                                <option value="57">57</option>
                                                <option value="58">58</option>
                                                <option value="59">59</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3" style="padding-left: 3px">
                                            <select id="slcForAMPM" class="form-control input-sm tb1">
                                                <option value="AM/PM">AM/PM</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row" style="padding-top: 5px">
                                        <div class="col-lg-2">
                                            <input type="radio" name="From" value="From" id="chkFrom"><b>From</b>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="txtFromDate" class="form-control input-sm">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar text-green"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 5px">
                                        <div class="col-lg-2">
                                        </div>
                                        <div class="col-lg-3" style="padding-right: 3px">
                                            <select id="slcFromHr" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2" style="padding-right: 3px; padding-left: 3px">
                                            <select id="slcFromMins" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                                <option value="49">49</option>
                                                <option value="50">50</option>
                                                <option value="51">51</option>
                                                <option value="52">52</option>
                                                <option value="53">53</option>
                                                <option value="54">54</option>
                                                <option value="55">55</option>
                                                <option value="56">56</option>
                                                <option value="57">57</option>
                                                <option value="58">58</option>
                                                <option value="59">59</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3" style="padding-left: 3px">
                                            <select id="slcFromAMPM" class="form-control input-sm tb1">
                                                <option value="AM/PM">AM/PM</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>


                                    </div>
                                    <div class="row" style="padding-top: 5px">
                                        <div class="col-lg-2" style="padding-left: 30px">
                                            <b>To</b>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="txtToDate" class="form-control input-sm">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar text-green"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 5px">
                                        <div class="col-lg-2">
                                        </div>
                                        <div class="col-lg-3" style="padding-right: 3px">
                                            <select id="slcToHr" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2" style="padding-right: 3px; padding-left: 3px">
                                            <select id="slcToMins" class="form-control input-sm tb1">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                                <option value="49">49</option>
                                                <option value="50">50</option>
                                                <option value="51">51</option>
                                                <option value="52">52</option>
                                                <option value="53">53</option>
                                                <option value="54">54</option>
                                                <option value="55">55</option>
                                                <option value="56">56</option>
                                                <option value="57">57</option>
                                                <option value="58">58</option>
                                                <option value="59">59</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3" style="padding-left: 3px">
                                            <select id="slcAMPM" class="form-control input-sm tb1">
                                                <option value="AM/PM">AM/PM</option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </div>


                                    </div>
                                    <div class="row topStyle">
                                        <div class="col-lg-12 text-center">
                                            <div class="dvUpdatebtn" style="display:none">
                                                <button type="button" id="btnUpdate" class="btn btn-primary">Update</button>
                                                <button type="button" id="btnCancel" class="btn btn-primary">Cancel</button>
                                            </div>
                                            <div class="dvSavebtn" >
                                                <button type="button" id="btnSave" class="btn btn-primary">Save</button>
                                            </div>


                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>



        <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #142541; color: #fff">
                    <button type="button" class="close text-red" style="color: #ff0000" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">
                        <label id="lblSubTask" style="font-style: normal !important; font-size: 16px"></label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5 dvSubPrio">
                        </div>
                        <div class="col-lg-1 dvSubPrioNum">
                        </div>
                        <div class="col-lg-6 dvSubPrioDropZone">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" id="btnSaveSubTaskPrio" class="btn btn-primary">Save</button>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>
    <script src="JS/BusinessPrioritization.js"></script>
</asp:Content>
